<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Fortrabbit debug log
|--------------------------------------------------------------------------
|
| Per default Laravel writes all logs to storage/log/...
| Since you don't have direct file access, you need to configure Laravel
| to write to the PHP error_log method instead.
|
*/

// Tail log SSH
if ( env('USE_SSH_TAIL', false) )
{
    $app->configureMonologUsing(function ($monolog)
    {
        // chose the error_log handler
        $handler = new \Monolog\Handler\ErrorLogHandler();
        // time will already be logged -> change default format
        $handler->setFormatter(new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %context% %extra%'));
        $monolog->pushHandler($handler);
    });
}

// Loggly
if ( env('USE_LOGGLY', false) )
{
    $app->configureMonologUsing(function($monolog) {
        // chose the error_log handler
        $handler = new \Monolog\Handler\LogglyHandler('48b7d65b-41f2-419a-98ad-47fc69f414f2/tag/monolog', \Monolog\Logger::INFO);
        // time will already be logged -> change default format
        $handler->setFormatter(new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %context% %extra%'));
        $monolog->pushHandler($handler);
    });
}

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
