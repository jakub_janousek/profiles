var gulp       = require('gulp'),
    elixir     = require('laravel-elixir'),
    notify     = require('gulp-notify'),
    exec       = require('child_process').exec,
    gutil      = require('gulp-util'),
    livereload = require('gulp-livereload');

var config = {
  livereload: {
    port: 8000,
    host: 'localhost'
  }
}

/*
 |--------------------------------------------------------------------------
 | Auto-publish
 |--------------------------------------------------------------------------
 |
 | Automatically publish Cartalyst Platform extension theme files
 | on save in the respective folder.
 |
 */

gulp.task('themes', function() {

  gulp.watch([
    'extensions/**/*.{php,js,css,less,scss}',
    'workbench/**/*.{php,js,css,less,scss}'
  ])
      .on('change', function(file){

        var path = file.path;

        if ( path.indexOf('extensions/') != -1 ) {
          var split_string = 'extensions/';
        } else if ( path.indexOf('workbench/') != -1 ) {
          var split_string = 'workbench/';
        }

        var extensions = path.split(split_string);

        if ( path.indexOf('themes/') == -1 )
            return null;

        var parts = extensions[1].split('/');

        var vendor = parts[0],
            extension = parts[1],
            fullname = vendor + '/' + extension;

        exec('php artisan theme:publish --extension="'+fullname+'"', function(err, stdout, stderr){
          gutil.log(stdout);
          // livereload(config.livereload); -- livereload does not work yet
        });
      });
});
