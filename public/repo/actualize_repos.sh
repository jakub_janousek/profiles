#!/bin/bash

# Actualize repos
# - run this file manually from command line for the first time
#   it will ask for authentication using username/password for
#   all listed services and then store them in auth.json for
#   future use.

DIRECTORY="/var/www/clients/client17/web193/web/repo"
LOG="log"
DATE_NOW=`date`
START_TIME=$SECONDS

cd "$DIRECTORY"
export COMPOSER_HOME=".composer"

echo -e -n "[${DATE_NOW}]: " >> $LOG

# php bin/satis build <configuration file> <build dir>
php satis/bin/satis build ${DIRECTORY}/satis/satis.json ${DIRECTORY}
# php ${DIRECTORY}/satis/bin/satis build ${DIRECTORY}/satis/satis.json ${DIRECTORY}
echo -e -n "php ${DIRECTORY}/satis/bin/satis build ${DIRECTORY}/satis/satis.json ${DIRECTORY} -n" >> $LOG

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo " ($(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) sec) " >> $LOG

./public/repo/satis/bin/satis build ./public/repo/satis/satis.json ./public/repo/