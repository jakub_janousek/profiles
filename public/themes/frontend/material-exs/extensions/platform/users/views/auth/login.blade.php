@extends('layouts/blank')

{{-- Page title --}}
@section('title')
{{{ trans('platform/users::auth/form.login.legend') }}} ::
@parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('login-style', 'platform/scss/login.scss') }}

{{-- Inline Scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')

<div class="row login-form">
        <div class="col-md-6 col-md-offset-3">
            <img src="{{ Asset::getUrl('platform/img/logo.svg') }}" class="form-logo" />
        </div>
	<div class="col-md-6 col-md-offset-3">

		{{-- Form --}}
		<form id="login-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" action="{{ route('user.login') }}" data-parsley-validate>

			{{-- Form: CSRF Token --}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="panel panel-brand">

				<!-- div class="panel-heading">{{{ trans('platform/users::auth/form.login.legend') }}}</div -->

				<div class="panel-body">

					{{-- Email Address --}}
					<div class="form-group{{ Alert::onForm('email', ' has-error') }} form-group-default">

						<label class="control-label lato14400" for="email">{{{ trans('platform/users::auth/form.email') }}}</label>

						<input class="form-control" type="email" name="email" id="email" value="{{{ Input::old('email') }}}" placeholder="{{{ trans('platform/users::auth/form.email_placeholder') }}}"
						required
						autofocus
						data-parsley-trigger="change"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.email_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('email') ?: trans('platform/users::auth/form.email_help') }}}
						</span>

					</div>

					{{-- Password --}}
					<div class="form-group{{ Alert::onForm('password', ' has-error') }} form-group-default">

						<label class="control-label lato14400" for="password">{{{ trans('platform/users::auth/form.password') }}}</label>

						<input class="form-control" type="password" name="password" id="password" placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
						required
						data-parsley-trigger="change"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.password_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password') ?: trans('platform/users::auth/form.password_help') }}}
						</span>

					</div>

					{{-- Remember me --}}
					<div class="form-group">

                                            <label class="control-label lato14400" for="remember">

							<input type="checkbox" class="js-switch" name="remember" id="remember" value="1"{{ Input::old('remember') ? ' checked="checked"' : null }} />

							{{{ trans('platform/users::auth/form.login.remember-me') }}}

						</label>

					</div>

					{{-- Form actions --}}
					<div class="form-group text-center">

						<button class="btn btn-default" type="submit">{{{ trans('platform/users::auth/form.login.submit') }}}</button>

						<!-- p class="help-block text-center">{{{ trans('platform/users::auth/form.login.no_account') }}} <a href="{{ URL::route('user.register') }}">{{{ trans('platform/users::auth/form.login.register') }}}</a> {{{ trans('platform/users::auth/form.login.or_recover') }}}  <a href="{{ URL::route('user.password_reminder') }}">{{{ trans('platform/users::auth/form.login.forgot-password') }}}</a></p -->
                                                <p class="text-center password lato14400"><a href="{{ URL::route('user.password_reminder') }}">{{{ trans('platform/users::auth/form.login.forgot-password') }}}</a></p>
					</div>

					{{-- Fix when not loaded from controller --}}
					@if (!isset($connections))
						<?php
						// Load oauth connections
						// that would normally do method in UsersController
						$users = app('platform.users');
						$connections = $users->auth()->getSocialConnections();
						?>
					@endif

					{{-- Social Logins --}}
					@if (isset($connections))
						@if (count($connections) > 0)
						<div class="btn-group btn-group-justified btn-group-social" role="group" aria-label="...">

							@foreach ($connections as $slug => $connection)
							<div class="btn-group" role="group">

								<a href="{{ url()->route('user.oauth.auth', $slug) }}" class="btn btn-sm btn-default btn-{{ $slug }}">
									<i class="fa fa-{{ $slug }}"></i> {{ $connection['driver'] }}
								</a>

							</div>
							@endforeach

						</div>
						@endif
					@endif

				</div>

			</div>

		</form>


	</div>
    
    <div class="login-watermark" style="background-image: url('{{ Asset::getUrl('platform/img/expert.svg') }}')"></div>

</div>

@stop
