$(function(){

  // Add remove focused class
  $('input').bind('focus', function(){
    $(this).parents('form:first').addClass('focused');
  }).bind('focusout', function(){
    $(this).parents('form:first').removeClass('focused');
  });

});