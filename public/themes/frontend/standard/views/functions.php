<?php

if ( !function_exists('is_front_page') ) {
	function is_front_page() {
		if ( $_SERVER['REQUEST_URI'] == '/' ) return true;
		return false;
	}
}

if ( !function_exists('is_search') ) {
	function is_search() {
		return Input::has(trans('sanatorium/shop::general.search.input'));
	}
}