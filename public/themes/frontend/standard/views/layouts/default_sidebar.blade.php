@extends('layouts/master')

{{-- Page title --}}
@section('title')
    @parent
@stop

{{-- Meta description --}}
@section('meta-description')
    @parent
@stop

{{-- Page content --}}
@section('main')

    <div class="sidebar">

        @yield('sidebar')

    </div>

    <div class="content">

        @yield('page')

    </div>

@stop

@section('body_classes')
@parent
with-sidebar
@stop