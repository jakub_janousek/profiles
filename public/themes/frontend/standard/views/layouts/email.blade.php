<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ config('platform.app.title') }}</title>

    <style type="text/css">
        /* -------------------------------------
                GLOBAL
        ------------------------------------- */
        * {
            margin:0;
            padding:0;
        }
        * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

        img {
            max-width: 100%;
        }
        .collapse {
            margin:0;
            padding:0;
        }
        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }


        /* -------------------------------------
                ELEMENTS
        ------------------------------------- */
        a { color: #00a1fe;}

        .btn {
            text-decoration:none;
            color: #FFF;
            background-color: #666;
            padding:10px 16px;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
        }

        p.callout {
            padding:15px;
            background-color: #222222;
            margin-bottom: 15px;
            color: #eee;
        }

        .callout a {
            font-weight:bold;
            color: #666;
        }

        table.social {
            /*  padding:15px; */
            background-color: #ebebeb;

        }

        a.fb { background-color: #3B5998!important; }
        a.tw { background-color: #1daced!important; }
        a.gp { background-color: #DB4A39!important; }
        a.ms { background-color: #000!important; }

        .sidebar .soc-btn {
            display:block;
            width:100%;
        }

        /* -------------------------------------
                HEADER
        ------------------------------------- */
        table.head-wrap { width: 100%;}

        .header.container table td.logo { padding: 15px; }
        .header.container table td.label { padding: 15px; padding-left:0px;}


        /* -------------------------------------
                BODY
        ------------------------------------- */
        table.body-wrap { width: 100%;}


        /* -------------------------------------
                FOOTER
        ------------------------------------- */
        table.footer-wrap { width: 100%;    clear:both!important;
        }
        .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
        .footer-wrap .container td.content p {
            font-size:10px;
            font-weight: bold;

        }


        /* -------------------------------------
                TYPOGRAPHY
        ------------------------------------- */
        h1,h2,h3,h4,h5,h6 {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

        h1 { font-weight:200; font-size: 42px;}
        h2 { font-weight:200; font-size: 32px;}
        h3 { font-weight:500; font-size: 24px;}
        h4 { font-weight:500; font-size: 22px;}
        h5 { font-weight:900; font-size: 16px;}
        h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#eee;}

        .collapse { margin:0!important;}

        p, ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size:14px;
            line-height:1.6;
        }
        p.lead { font-size:17px; }
        p.last { margin-bottom:0px;}

        ul li {
            margin-left:5px;
            list-style-position: inside;
        }

        /* -------------------------------------
                SIDEBAR
        ------------------------------------- */
        ul.sidebar {
            background:#ebebeb;
            display:block;
            list-style-type: none;
        }
        ul.sidebar li { display: block; margin:0;}
        ul.sidebar li a {
            text-decoration:none;
            color: #666;
            padding:10px 16px;
            /*  font-weight:bold; */
            margin-right:10px;
            /*  text-align:center; */
            cursor:pointer;
            border-bottom: 1px solid #777777;
            border-top: 1px solid #FFFFFF;
            display:block;
            margin:0;
        }
        ul.sidebar li a.last { border-bottom-width:0px;}
        ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



        /* ---------------------------------------------------
                RESPONSIVENESS
                Nuke it from orbit. It's the only way to be sure.
        ------------------------------------------------------ */

        .container {
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important; /* makes it centered */
            clear:both!important;
        }

        .content {
            padding:15px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }

        .content table { width: 100%; }


        .column {
            width: 300px;
            float:left;
        }
        .column tr td { padding: 15px; }
        .column-wrap {
            padding:0!important;
            margin:0 auto;
            max-width:600px!important;
        }
        .column table { width:100%;}
        .social .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }

        .clear { display: block; clear: both; }


    </style>

</head>

<body bgcolor="#FFFFFF">

<div style="background:#ffffff;">

    <table cellspacing="0" cellpadding="0" border="0" style="margin:0px auto 0px;text-align:left;width:660px;background:#ffffff;color:#333333;font-weight:normal;font-size:12px;font-family:Verdana,Arial,Tahoma;min-width:660px">

        <tbody>

        <!-- Empty row -->
        <tr>

            <td colspan="4" width="660" height="30"></td>

        </tr>

        <!-- Email header -->
        <tr>

            <td width="30"></td>

            <td width="450" align="left" style="text-align:left">

                <strong style="font-weight:bold;font-size:22px;color:#333333">{{ config('platform.app.title') }}</strong>

            </td>

            <td width="150" align="right">

                <a href="{{ URL::to('/') }}" target="_blank">

                    <img src="@hook('logo')" alt="{{ config('platform.app.title') }}" style="max-width:100%;height:auto;">

                </a>

            </td>

            <td width="30"></td>

        </tr>
        <!-- /Email header -->

        <!-- Empty row -->
        <tr>

            <td colspan="4" width="660" height="30"></td>

        </tr>

        <!-- Email body -->
        <tr>

            <td colspan="4">

                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px auto 0px;text-align:left;width:660px;background:#ffffff;color:#333333;font-weight:normal;font-size:12px;font-family:Verdana,Arial,Tahoma">

                    <tbody>

                    <tr>

                        <td colspan="3" width="660" height="30"></td>

                    </tr>

                    <tr>

                        <td width="30"></td>

                        <td style="line-height:24px;font-size:12px;color:#333333">


                            <!-- Content -->
                            @yield('content')

                        </td>

                        <td width="30"></td>
                    </tr>

                    <tr>

                        <td width="30"> </td>

                        <td>

                            <table cellspacing="0" cellpadding="0" border="0" width="660" style="margin:10px auto 0px;text-align:left;width:600px;background:#ffffff;color:#333333;font-weight:normal;font-size:12px;font-family:Verdana,Arial,Tahoma">

                                <tbody>

                                <tr>

                                    <td style="padding:20px 0 0 0;line-height:24px;font-size:12px;color:#333333" width="60%">

                                        {{ trans('sanatorium/orders::emails.general.team') }}
                                        <strong>{{ config('platform.app.title') }}</strong>

                                    </td>

                                    <td align="right" style="padding:20px 0 0 0;text-align:right">

                                        <img src="@hook('logo')" alt="{{ config('platform.app.title') }}" style="max-width:50%;height:auto;">

                                    </td>

                                </tr>

                                </tbody>

                            </table>

                        </td>

                        <td width="30"> </td>

                    </tr>

                    <tr>

                        <td colspan="3" width="660" height="30"> </td>

                    </tr>

                    <tr>

                        <td colspan="3" width="660" height="10" style="line-height:0"> </td>

                    </tr>

                    </tbody>

                </table>

            </td>

        </tr>

        <!-- Footer -->
        <tr>

            <td width="20" style="background-color:#f9f9f9"></td>

            <td colspan="2" width="660" height="20" style="background-color:#f9f9f9"></td>

            <td width="20" style="background-color:#f9f9f9"></td>

        </tr>

        <tr>

            <td width="20" style="background-color:#f9f9f9"></td>

            <td colspan="2" align="center" style="text-align:center;color:#666666;background-color:#f9f9f9;border-top:1px solid #DDD;border-bottom:1px solid #DDD;padding-top:15px;padding-bottom:15px;">

                <p style="text-align:center;color:#666666;font-size:11px">

                    {!! trans('sanatorium/orders::emails.general.assistance', ['email' => config('mail.from.address') ]) !!}

                </p>

                @hook('mail.footer')

            </td>

            <td width="20" style="background-color:#f9f9f9"></td>

        </tr>

        <tr>

            <td width="20" style="background-color:#f9f9f9"></td>

            <td colspan="2" width="660" height="20" style="background-color:#f9f9f9"></td>

            <td width="20" style="background-color:#f9f9f9"></td>

        </tr>

        </tbody>

    </table>

</div>

</body>
</html>
