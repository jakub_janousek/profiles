@extends('layouts/master')

{{-- Page title --}}
@section('title')
    @parent
@stop

{{-- Meta description --}}
@section('meta-description')
    @parent
@stop

{{-- Page content --}}
@section('main')

    <div class="content">
        @yield('page')
    </div>

@stop

@section('body_classes')
    @parent
@stop