@extends('layouts/master')

{{-- Page title --}}
@section('title')
	@parent
@stop

{{-- Meta description --}}
@section('meta-description')
	@parent
@stop

{{-- Page content --}}
@section('main')


	<div class="container">

		<div class="content">

				@yield('page')

		</div>

	</div>

@stop

@section('body_classes')
	@parent
@stop