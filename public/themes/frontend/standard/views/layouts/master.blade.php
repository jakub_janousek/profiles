<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>
        @section('title')
            {{ Config::get('platform.app.title') }}
        @show
    </title>
    <meta name="description" content="@yield('meta-description')">
    <meta name="viewport" content="width=device-width">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    {{-- Queue assets --}}
    {{ Asset::queue('standard', 'standard/sass/shop.scss') }}
    {{ Asset::queue('standard', 'standard/js/standard.js', 'jquery') }}

    {{ Asset::queue('modernizr', 'modernizr/js/modernizr.min.js') }}
    {{ Asset::queue('jquery', 'jquery/js/jquery.min.js') }}
    {{ Asset::queue('bootstrap', 'bootstrap/js/bootstrap.min.js', 'jquery') }}
    {{ Asset::queue('svgjs', 'svgjs/svgjs.js', 'jquery' )}}
    {{ Asset::queue('bxslider', 'bxslider/jquery.bxslider.js', 'jquery' )}}

    {{ Asset::queue('trade-gothic', 'standard/fonts/trade/MyFontsKit.css') }}

	{{-- HTML5 shim, for IE6-8 support of HTML5 elements --}}
	<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    {{-- OG tags etc --}}
    @section('meta-general')
    @show

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach

</head>

@include('functions')

<body class="@yield('body_classes') has-navbar-fixed has-navbar-high">

<!-- Hook: head -->
@hook('shop.head')

<!-- Partials: header -->
@include('partials/header')

<!-- Main -->
<main class="main" role="document">

    <div class="container-fluid">

        @yield('main')

    </div>

</main>

<!-- Partials: footer -->
@include('partials/footer')

<!-- Hook: Scripts footer -->
@hook('scripts.footer')

		<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->

{{-- @widget('platform/menus::nav.show', array('main', 0, 'navbar-fixed-bottom-right')) --}}

{{-- Compiled scripts --}}
@foreach (Asset::getCompiledScripts() as $script)
    <script src="{{ $script }}"></script>
@endforeach

{{-- Call custom inline scripts --}}
@section('scripts')
    <script type="text/javascript">
        $(function(){

        });
    </script>
@show

{{-- Call custom inline styles --}}
@section('styles')
@show

</body>
</html>