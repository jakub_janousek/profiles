<!-- Header navbar -->
<nav class="navbar navbar-default navbar-fixed-top navbar-wrapper" role="navigation">

	<div class="container-fluid navbar-low hidden-xs">

		<!-- Widget: Menu - account -->
		@widget('platform/menus::nav.show', ['special', 0, 'nav navbar-nav navbar-left nav-special text-small'])

		<!-- HookL cart.show -->
		@hook('cart.show', 'navbar-form navbar-right')

		<!-- Hook: shop.header -->
		@hook('shop.header', 'nav navbar-nav navbar-right')

		<!-- Widget: Menu - account -->
		@widget('platform/menus::nav.show', ['account', 0, 'nav navbar-nav navbar-right text-small'])

	</div>

	<div class="container-fluid navbar-pad nav-center">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-primary" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a href="/" class="navbar-brand">

				<img class="standard-title" src="@hook('logo')" width="150" height="50">
			</a>

		</div>

		<div class="collapse navbar-collapse" id="navbar-collapse-primary">

			<!-- Hook: shop.search -->
			@hook('shop.search', 'navbar-form navbar-right search-form inverse-form')

			<!-- Widget: Menu - general -->
			@widget('platform/menus::nav.show', ['main', 0, 'nav navbar-nav nav-codensed nav-mega', null, 'partials/menus/mega'])

		</div>

	</div>

</nav>


