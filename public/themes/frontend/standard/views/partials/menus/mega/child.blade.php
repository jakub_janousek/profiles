<li class="{{ $child->isActive ? 'active' : null }} col-sm-3">
    <a target="{{ $child->target }}" href="{{ $child->uri }}">
        <i class="{{ $child->class }}"></i>
        <span>{{ $child->name }}</span>
        @if ($child->children and ! $child->hasSubItems)
            <b class="caret"></b>
        @endif
    </a>

    @if ($child->children)
        <ul class="" role="menu">
            @each('partials/menus/mega/sub', $child->children, 'child')
        </ul>
    @endif
</li>
