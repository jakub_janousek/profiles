<li class="{{ $child->isActive ? 'active' : null }}">
    <a target="{{ $child->target }}" href="{{ $child->uri }}">
        <i class="{{ $child->class }}"></i>
        <span>{{ $child->name }}</span>
        @if ($child->children and ! $child->hasSubItems)
            <b class="caret"></b>
        @endif
    </a>
</li>
