<?php
	if ( !function_exists('is_category_active') ) :
		function is_category_active($url) {
			// Build full current url
			$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

			if ( strpos($actual_link, $url) === 0 ) return true;
			return false;
		}
	endif;

	if ( !function_exists('sideCategoryMenu') ) :
		function sideCategoryMenu($parent = 0, $level = 0, $depth = 0) {
			$listGroupItemsClasses = [
				"list-group-item-top",
				"list-group-item-sub",
				"list-group-item-level",
				"list-group-item-level",
				""
			];

			$categories = Category::where('parent', $parent)->get();
			if ( count($categories) > 0 ) : ?>

				@if ( $parent == 0 )
					<div id="accordion">
						<div class="list-group panel sidemenu">
				@endif

				@foreach($categories as $category)
					<?php
						$subcategories = Category::where('parent', $category->id)->get();
						$hasChildren = count($subcategories) > 0 && $level < $depth;
					?>
					@if ($hasChildren)
						@if ( $level == 0 )
						<div class="list-group-group">
						@endif
							<a href="#{{ $category->slug }}" class="list-group-item {{ $listGroupItemsClasses[$level % count($listGroupItemsClasses)] }} {{ is_category_active(URL::to( $category->url)) ? 'opened' : '' }}" data-toggle="collapse">
								@if ( $level == 0 )
								<i class="menu-icon-{{ $category->slug }}"></i>
								@endif
								{{ $category->category_title }}
								<i class="ion-plus pull-right"></i>
							</a>
							<div class="collapse {{ is_category_active(URL::to( $category->url)) ? 'in' : '' }}" id="{{ $category->slug }}">
								{{ sideCategoryMenu($category->id, $level + 1, $depth) }}
							</div>
						@if ( $level == 0 )
						</div>
						@endif
					@else
						@if ( $level == 0 )
						<div class="list-group-group">
						@endif
							<a href="{{ $category->url }}" class="list-group-item {{ $listGroupItemsClasses[$level % count($listGroupItemsClasses)] }}">
								@if ( $level == 0 )
								<i class="menu-icon-{{ $category->slug }}"></i>
								@endif
								{{ $category->category_title }}
							</a>
						@if ( $level == 0 )
						</div>
						@endif
					@endif
				@endforeach

				@if ( $parent == 0 )
						</div>
					</div>
				@endif
				<?php
			endif;
		}
	endif;
?>

{{ sideCategoryMenu(0, 0, 3) }}
