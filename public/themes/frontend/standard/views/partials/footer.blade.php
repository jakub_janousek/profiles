<footer class="footer bg-inverse">
	<div class="container">
		<div class="row">

			<!-- Footer block -->
			<div class="col-sm-2 highlights">
				@widget('platform/menus::nav.show', ['highlights', 0, 'nav nav-list'])
			</div>

			<!-- Footer block: How to -->
			<div class="col-sm-2">
				<h4>{{ trans('extra.how_to') }}</h4>
				@widget('platform/menus::nav.show', ['how-to', 0, 'nav nav-list'])
			</div>

			<!-- Footer block: About -->
			<div class="col-sm-2">
				<h4>{{ trans('extra.about') }}</h4>
				@widget('platform/menus::nav.show', ['about', 0, 'nav nav-list'])
			</div>

			<!-- Footer block: Social -->
			<div class="col-sm-3 col-sm-offset-3 text-right">
				<h4>{{ trans('extra.social') }}</h4>

				<?php $linkable = config('sanatorium-social.links'); ?>
				@if ( is_array($linkable) )
					<ul class="nav nav-social">
					@foreach( $linkable as $site => $link )

						@if ( $link )
							<li>
								<a href="{{ $link }}" target="_blank" rel="external">
									<i class="fa fa-{{ $site }}"></i>
									<span class="sr-only">{{ config('platform.app.title') }} {{ $site }}</span>
								</a>
							</li>
						@endif

					@endforeach
					</ul>
				@endif
			</div>

		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-bottom">
					<div class="row">
						<div class="col-sm-6">
							<p>
								{{ config('platform.app.tagline') }}
							</p>
						</div>
						<div class="col-sm-6 text-right">
							@widget('platform/menus::nav.show', ['bottom', 0, 'nav'])
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</footer>

