@extends('layouts/default_fullscreen')

{{-- Page title --}}
@section('title')
@parent
{{{ $page->meta_title or $page->name }}}
@stop

{{-- Meta description --}}
@section('meta-description')
{{{ $page->meta_description }}}
@stop

{{-- Page content --}}
@section('page')

	<div class="row bg-gray-lightest welcome pad-vertical">

		<div class="container">

			<div class="row">

				<div class="col-sm-6 col-sm-offset-3 text-center">

					<h1>Říkal někdo rychlé?</h1>

				</div>

			</div>

			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="panel">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-8">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eius expedita
										fugiat modi soluta veritatis!</p>
								</div>
							</div>
							<div class="row">
								<hr>
							</div>
							<div class="row">
								<div class="col-sm-4">

								</div>
								<div class="col-sm-8">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eius expedita
										fugiat modi soluta veritatis!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

@stop