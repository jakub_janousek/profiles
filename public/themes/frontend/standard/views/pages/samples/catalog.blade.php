@extends('layouts/default_sidebar')

{{-- Page title --}}
@section('title')
    {{{ $page->meta_title or $page->name }}}
    @parent
@stop

{{-- Meta description --}}
@section('meta-description')
    {{{ $page->meta_description }}}
@stop

{{-- Page content --}}
@section('page')

    <!-- Samples: Catalog page -->
    <div class="container container-aligned">

        @widget('sanatorium/shop::sort.compact', ['btn-group pull-right', 'btn btn-default'])

        <h1 class="title-unified-size">
            {{{ $page->name }}}
        </h1>

        <div class="row">

        @if ( class_exists('Product') )

            <?php

            if ( !isset($per_page) ) {
                $per_page = config('sanatorium-shop.per_page');
            }

            $products = Product::ordered()
                    ->search()
                    ->take($per_page)
                    ->orderBy('id', 'DESC')
                    ->get();
            ?>

            @foreach($products as $product)
                @include('sanatorium/shop::catalog/product')
            @endforeach

        @endif

        </div>

    </div>

@stop

@section('sidebar')

    <h1 class="title-unified-size">
        {{{ $page->name }}}
    </h1>


    @if ( $categories = app('sanatorium.categories.category')->where('parent', 0)->get() )
        <ul class="nav category-filter" data-category-filter>
        @foreach( $categories as $category )

            <li>
                <a href="{{ $category->url }}" data-category-filter-id="{{ $category->id }}">
                    {{ $category->{$category->map['title']} }}
                </a>

                @if ( $subcategories = app('sanatorium.categories.category')->where('parent', $category->id)->get() )
                    <ul>
                        @foreach( $subcategories as $category )
                            <li>
                                <a href="{{ $category->url }}" data-category-filter-id="{{ $category->id }}">
                                    {{ $category->{$category->map['title']} }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif

            </li>

        @endforeach
        </ul>
    @endif


@stop

@section('body_classes')
    @parent
@stop