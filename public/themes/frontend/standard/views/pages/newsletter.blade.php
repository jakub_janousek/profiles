@extends('layouts/default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ $page->meta_title or $page->name }}}
@stop

{{-- Meta description --}}
@section('meta-description')
    {{{ $page->meta_description }}}
@stop

{{-- Page content --}}
@section('page')


    <div class="row">
        <div class="col-sm-12">
            @if ( Route::has('sanatorium.newsletter.subscribe') )
                @if ( Cookie::get('has_newsletter') )
                    <p class="alert alert-success">
                        {{ trans('sanatorium/newsletter::messages.subscribed') }}
                    </p>
                @else
                    <form action="{{ route('sanatorium.newsletter.subscribe') }}" method="post" id="subscribe_form">
                        <div class="input-group">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}">
                            <input class="form-control" type="email" name="email" id="subscribe_email" placeholder="Subscribe">
                            <div class="input-group-btn">
                                <button type="submit" id="subscribe_submit" class="btn btn-default">
                                    {{ trans('action.submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                @endif
            @else

                {{-- Newsletter extension is disabled --}}
                <p>Newsletters are currently disabled</p>

            @endif
        </div>
    </div>


@stop