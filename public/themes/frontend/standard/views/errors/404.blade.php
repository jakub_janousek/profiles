@extends('layouts/default')

{{-- Page title --}}
@section('title')
{{{ trans('errors.404.title') }}} ::
@parent
@stop

{{-- Meta description --}}
@section('meta-description')
{{{ trans('errors.404.description') }}}
@stop

{{-- Styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('content')

	<section>

		<div class="error">
			<div class="error__message">

				<h1>{{{ trans('errors.404.page.title') }}}</h1>
				<h2>{{{ trans('errors.404.page.subtitle') }}}</h2>
				<p>{{{ trans('errors.404.page.description') }}}</p>

				<hr>

				<h4>{{{ trans('errors.options') }}}</h4>
				<ul>
					<li><a href="{{{ URL::to('/') }}}" title="{{ @config('platfom.app.title') }}"><i class="fa fa-home"></i> {{config('platform.app.title') }}</a></li>
				</ul>

			</div>
		</div>

	</section>

@stop
