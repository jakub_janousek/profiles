<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>
			@section('title')
				@setting('platform.app.title')
			@show
		</title>
		<meta name="description" content="@yield('meta-description')">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="base_url" content="{{ URL::to('/') }}">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		{{-- Queue assets --}}
		{{ Asset::queue('bootstrap', 'bootstrap/css/bootstrap.min.css') }}
		{{ Asset::queue('font-awesome', 'font-awesome/css/font-awesome.min.css') }}
		{{ Asset::queue('style', 'platform/scss/style.scss') }}

		{{ Asset::queue('modernizr', 'modernizr/js/modernizr.min.js') }}
		{{ Asset::queue('jquery', 'jquery/js/jquery.min.js') }}
		{{ Asset::queue('bootstrap', 'bootstrap/js/bootstrap.min.js', 'jquery') }}
		{{ Asset::queue('platform', 'platform/js/platform.js', 'jquery') }}

		{{ Asset::queue('switchery', 'switchery/switchery.js') }}
		{{ Asset::queue('switchery', 'switchery/switchery.css') }}

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="{{ Asset::getUrl('platform/img/favicon.png') }}">

		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

		{{-- Compiled styles --}}
		@foreach (Asset::getCompiledStyles() as $style)
			<link href="{{ $style }}" rel="stylesheet">
		@endforeach

		{{-- Call custom inline styles --}}
		@section('styles')
		@show
	</head>

	<body class="layout-default">

		<!--[if lt IE 7]>
		<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
		<![endif]-->
		
		<!-- Navigation -->
		@include('partials/navigation')

		<!-- alerts -->
		<div class="alerts">
			@include('partials/alerts')
		</div>

		<div class="base">

			<!-- Page -->
			<div class="page">

				<!-- Page Header-->
				<div class="page__header hidden">
					@yield('header', '')
				</div>

				<!-- Page Content-->
				<div class="page__content container">
					
					@yield('page')
				</div>

			</div>

		</div>

		<div class="p-t-40 p-b-30">

			<!-- Footer -->
			@include('partials/footer')

		</div>

		{{-- Compiled scripts --}}
		@foreach (Asset::getCompiledScripts() as $script)
			<script src="{{ $script }}"></script>
		@endforeach

		{{-- Call custom inline scripts --}}
		@section('scripts')
		@show

		<script type="text/javascript">
			var switcheryOptions = {
				color               : '#28deee'
				, secondaryColor    : '#eeeeee'
				, jackColor         : '#fff'
				, jackSecondaryColor: null
				, className         : 'switchery'
				, disabled          : false
				, disabledOpacity   : 0.5
				, speed             : '0.4s'
				, size              : 'default'
			};
			$(function(){
				$('.js-switch').each(function(){
					var init = new Switchery($(this)[0], switcheryOptions);
				});

				setTimeout(function(){
					$('.alerts .alert').each(function(){
					
						$(this).fadeOut();

					});
				}, 2000);
			});
		
		</script>

		@if ( isset($user) )

			@if ( strpos($user->email, '@yori') !== -1 )

				<!-- Modal -->
				<div class="modal fade" id="claim-contact-modal" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document" style="z-index:1500">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Claim contact</h4>
							</div>
							<div class="modal-body">
								<input type="email" name="claim_email" class="form-control" id="claim_email" placeholder="Your E-mail">
							</div>
							<div class="modal-footer" style="text-align:center!important">
								<button type="button" class="btn btn-primary" id="claim_btn">Claim</button>
							</div>
						</div>
					</div>
				</div>


			@endif

		@endif

	</body>
</html>
