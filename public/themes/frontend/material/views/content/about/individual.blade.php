	
	<div class="row">
		<div class="col-sm-6">
			<h3>Landing page</h3>
			<p class="lead">
				Use following login information to sign in:
			</p>
			<p class="lead">
				E-mail: <code>individual@sld.gs</code>
				<br>
				Password: <code>individual</code>
			</p>
			<p>
				The sample user is "individual contact".
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/1.png" class="lightbox">
				<img src="/about/1.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Individual profile page</h3>
			<p class="lead">
				As signed in corprorate user, you are able to manage your data.
			</p>
			<p class="lead">
				You can also see your linked corporates in part <strong>Linked corporates</strong> with their assignment started and ended at.
			</p>
			<p class="lead">
				In part <strong>Manage newsletters</strong> you are able to opt-out from newsletter lists you are signed to.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/4.png" class="lightbox">
				<img src="/about/2/4.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Manage your company</h3>
			<p class="lead">
				As a user capable to change the company you can also manage your assigned company (or companies). To see them, click on your avatar in right top corner and switch by clicking.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/11.png" class="lightbox">
				<img src="/about/2/11.png" class="fullwidth">
			</a>
		</div>
	</div>