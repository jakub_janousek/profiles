	
	<div class="row">
		<div class="col-sm-6">
			<h3>Landing page</h3>
			<p class="lead">
				Use following login information to sign in:
			</p>
			<p class="lead">
				E-mail: <code>agnostic@sld.gs</code>
				<br>
				Password: <code>agnostic</code>
			</p>
			<p>
				The sample user has "superuser" rights.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/1.png" class="lightbox">
				<img src="/about/1.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Authenticated user</h3>
			<p class="lead">
				You can access your profile by using dropdown menu <strong>Account <i class="ion-ios-arrow-thin-right"></i> Profile</strong> in the top right corner.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2.png" class="lightbox">
				<img src="/about/2.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Your profile</h3>
			<p class="lead">
				Your profile lists all the different user fields and their current values, in the top right hand side, there is completeness of profile according to fields scoring (0-5). 
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/3.png" class="lightbox">
				<img src="/about/3.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Manage profile</h3>
			<p class="lead">
				Hover them to <code><i class="fa fa-pencil"></i> Edit</code>, <code><i class="fa fa-trash"></i> Clear</code> or <code><i class="fa fa-cog"></i> Manage</code>.
			</p>
			<p>
				Notes:<br>
				- Only users with specific permissions can <code><i class="fa fa-cog"></i> Manage</code> the fields (as a superuser agnostic@sld.gs, you got the permissions to do so).
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/4.png" class="lightbox">
				<img src="/about/4.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Corporate profiles</h3>
			<p class="lead">
				Your profile is personal profile, now if you want to see corporate profile, try <a href="{{ route('sleighdogs.profile.view', [11]) }}" target="_blank">Sleighdogs on @setting('platform.app.title')</a>.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/6.png" class="lightbox">
				<img src="/about/6.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Linked contacts</h3>
			<p class="lead">
				Scroll down the corporate profile page to see <strong>Linked contacts</strong> (if any), their position and since when they are linked with the company.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/7.png" class="lightbox">
				<img src="/about/7.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<h2 class="hidden">Backend</h2>

	<div class="row">
		<div class="col-sm-6">
			<h3>Administrator</h3>
			<p class="lead">
				Now if you hit <strong>Account <i class="ion-ios-arrow-thin-right"></i> Administrator</strong> instead, you will end up on <strong>Dashboard</strong>. Dashboard features following widgets:
			</p>
			<p>
				<i class="ion-ios-arrow-thin-right"></i> Stats (registered users)<br>
				<i class="ion-ios-arrow-thin-right"></i> Embed contact samples (iframe as requested in briefing)<br>
				<i class="ion-ios-arrow-thin-right"></i> Few widgets depicting newest activity
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/8.png" class="lightbox">
				<img src="/about/8.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Admin menu</h3>
			<p class="lead">
				Admin screen contains two menus:
			</p>
			<p>
				<strong>Admin menu</strong> on left is used for general content management. Some are straightforward YORI specific stuff, like <code><i class="fa fa-building"></i> Corporate contacts</code> or <code><i class="fa fa-user"></i> Individual contacts</code>. Some, f.e. <code><i class="fa fa-file"></i> Pages</code> are meant only for superuser to compose pages like this one etc.
			</p>
			<p>
				<strong>System menu</strong> on top right contains <code><i class="fa fa-sliders"></i> Settings</code> and is used specifically for high-level configuration (mailing services, social login keys, app title etc.), the rest is general user-type menu (Preview, Sign out etc.).
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/9.png" class="lightbox">
				<img src="/about/9.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Corporate contacts</h3>
			<p class="lead">
				Companies view (grid) type of admin screen. With filters, search, export, bulk actions and mailing list creation features. 
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/10.png" class="lightbox">
				<img src="/about/10.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Individual contacts</h3>
			<p class="lead">
				Employees view (grid) type of admin screen. With filters, search, export, bulk actions and mailing list creation features. 
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/11.png" class="lightbox">
				<img src="/about/11.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Mail transactions</h3>
			<p class="lead">
				Automatic mail template view (grid) type of admin screen. These are currently under development and will be different.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/13.png" class="lightbox">
				<img src="/about/13.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Mail template form</h3>
			<p class="lead">
				Automatic mail template detail screen. Composing actual mail templates is possible here. More information coming when this feature is fully operational.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/14.png" class="lightbox">
				<img src="/about/14.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Mail lists</h3>
			<p class="lead">
				Mail lists view (grid) providing list of saved mail lists. Clicking on this brings you to detail of mail list with all the users listed in it. This is still under development.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/15.png" class="lightbox">
				<img src="/about/15.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Attributes</h3>
			<p class="lead">
				Attributes view (grid) contains all different fields listed for all entities throughout the system.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/16.png" class="lightbox">
				<img src="/about/16.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Manage field</h3>
			<p class="lead">
				Attribute detail (or Manage field) is place where user with sufficient permissions can edit Name, Description, Type and Score of specific field.
			</p>
			<p>
				Notes:<br>
				- This is the screen where you are taken from profile by hitting <code><i class="fa fa-cog"></i> Manage</code>.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/17.png" class="lightbox">
				<img src="/about/17.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Menus</h3>
			<p class="lead">
				As a super admin, you are able to manage <code><i class="fa fa-th-list"></i> Menus</code>, in the default grid view, you can see all the menus registered in website. There is:<br>
			</p>
			<p>
				<strong>Main menu</strong> - Frontend in the top right area beside your Account<br>
				<strong>Account menu</strong> - Frontend in the very top right corner, newly features avatar<br>
				<strong>System menu</strong> - Backend in the top right area<br>
				<strong>Admin menu</strong> - Backend left menu<br>
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/19.png" class="lightbox">
				<img src="/about/2/19.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Menus <i class="ion-ios-arrow-thin-right"></i> Menu</h3>
			<p class="lead">
				Managing menu is straightforward. You can add items by using <strong>Create New Link</strong>. There is multiple options, which will be important for you:
			</p>
			<p>
				<strong>Link name</strong> - label of the item<br>
				<strong>Slug</strong> - this will be automatically derived from Link name<br>
				<strong>Type</strong> - you may choose between static link and all different registered post types (these have to be registered to menu manager, by default, there is only Page)<br>
				<strong>Uri</strong> - relative path of the link from the root of the menu (f.e. /profile)<br>
			</p>
			<p>
				There is also <code><i class="fa fa-wrench"></i> Advanced settings</code> which you might find useful in more detailed work:<br>
				<strong>Status, Target, HTTPS, Parent</strong> are prety self explanatory.<br>
				<strong>CSS class</strong> - by specifiying this you might add class to element "i" before the link, so that the link will have an icon.<br>
				<strong>Regular expressions</strong> - serves for highlighting active menus<br>
				<strong>Visibility</strong> - defines which types of users can see the link, possible values are: Show always, Logged In, Logged Out, Admin only. Besides these - Logged In option also offers you to select which specific groups of user may see the menu item.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/20.png" class="lightbox">
				<img src="/about/2/20.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Settings</h3>
			<p class="lead">
				You can also manage settings <code><i class="fa fa-sliders"></i> Settings</code> there are multiple tabs, first of all, you will see <strong>General settings</strong>.
			</p>
			<p class="lead">
				Here you can manage for example the app <strong>Title</strong> and <strong>Tagline</strong> which are shown on countless occurrences through the system. You can also manage and change the mail driver and details as well as <strong>E-mail sender</strong> (from name and from address). 
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/22.png" class="lightbox">
				<img src="/about/2/22.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Settings <i class="ion-ios-arrow-thin-right"></i> Social</h3>
			<p class="lead">
				Social settings are good if you wish to change the way users authenticate to your site. You may enable and disable different services, change their API credentials.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/23.png" class="lightbox">
				<img src="/about/2/23.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Settings <i class="ion-ios-arrow-thin-right"></i> Maintenance</h3>
			<p class="lead">
				Extension Maintenance will allow you to close the site for either all users, or for users who do not have admin rights or for those that are not in allowed IPs. You might aswell use combination of exceptions named before.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/24.png" class="lightbox">
				<img src="/about/2/24.png" class="fullwidth">
			</a>
		</div>
	</div>