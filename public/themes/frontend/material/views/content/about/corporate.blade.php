	
	<div class="row">
		<div class="col-sm-6">
			<h3>Landing page</h3>
			<p class="lead">
				Use following login information to sign in:
			</p>
			<p class="lead">
				E-mail: <code>official@sld.gs</code>
				<br>
				Password: <code>official</code>
			</p>
			<p>
				The sample user is "corporate".
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/1.png" class="lightbox">
				<img src="/about/1.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Company profile page</h3>
			<p class="lead">
				As signed in corprorate user, you are able to manage your data.
			</p>
			<p class="lead">
				You can also see your linked contacts in part <strong>Linked contacts</strong> with their positions and when their assignment started and ended at.
			</p>
			<p class="lead">
				In part <strong>Manage newsletters</strong> you are able to opt-out from newsletter lists you are signed to.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/5.png" class="lightbox">
				<img src="/about/2/5.png" class="fullwidth">
			</a>
		</div>
	</div>