	
	<div class="row">
		<div class="col-sm-6">
			<h3>Viewing profiles</h3>
			<p class="lead">
				As a guest you can go and view others profile, like <a href="{{ route('sleighdogs.profile.view', [11]) }}" target="_blank">Sleighdogs on @setting('platform.app.title')</a>.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/6.png" class="lightbox">
				<img src="/about/6.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Sharing corporate embeds</h3>
			<p class="lead">You can also share the embeds, for example use following code to share corporate embed profile on 3rd party site in <strong>Skyscraper</strong> size (300x600).</p>
			<pre style="max-width:75%">{{ '<iframe src="http://sup.sld.gs/public/embed/11" style="width:300px;height:600px;border:0;overflow:hidden;" frameborder="0"></iframe>' }}</pre>
		</div>
		<div class="col-sm-6">
			<iframe src="{{ route('sleighdogs.profile.embed', 11) }}" style="width:300px;height:600px;border:0;overflow:hidden;" frameborder="0"></iframe>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Sharing individual embeds</h3>
			<p class="lead">To share individual embed in <strong>Box</strong> size (300x300) use following code.</p>
			<pre style="max-width:75%">{{ '<iframe src="http://sup.sld.gs/public/embed/2" style="width:300px;height:600px;border:0;overflow:hidden;" frameborder="0"></iframe>' }}</pre>
		</div>
		<div class="col-sm-6">
			<iframe src="{{ route('sleighdogs.profile.embed', 2) }}" style="width:300px;height:300px;border:0;overflow:hidden;" frameborder="0"></iframe>
		</div>
	</div>