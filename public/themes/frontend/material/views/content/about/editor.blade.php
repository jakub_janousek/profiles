	
	<div class="row">
		<div class="col-sm-6">
			<h3>Landing page</h3>
			<p class="lead">
				Use following login information to sign in:
			</p>
			<p class="lead">
				E-mail: <code>editor@sld.gs</code>
				<br>
				Password: <code>editor</code>
			</p>
			<p>
				The sample user is "Editor".
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/1.png" class="lightbox">
				<img src="/about/1.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Dashboard</h3>
			<p class="lead">
				As type of user editor, your dashboard offers the same options <strong>Admin</strong> or <strong>Super admin</strong>, but your limitation is most visible on the menus.
			</p>
			<p>
				<strong>Admin menu</strong> on left is used for general content management. You will see there:<br>
				<code><i class="fa fa-building"></i> Corporate contacts</code> - to manage companies<br>
				<code><i class="fa fa-user"></i> Individual contacts</code> - to manage people<br>
				<code><i class="fa fa-asterisk"></i> Attributes</code> - to manage fields<br>
				<code><i class="fa fa-tag"></i> Tags</code> - to manage tags<br>
				<code><i class="fa fa-circle-o"></i> Transaction mails</code> - to manage automatic e-mails<br>
				<code><i class="fa fa-circle-o"></i> Maillist</code> - to manage mail lists<br>
			</p>
			<p>
				<strong>System menu</strong> on top right contains offers <code><i class="fa fa-laptop"></i> Preview</code> which takes you back to YORI frontend.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/3.png" class="lightbox">
				<img src="/about/2/3.png" class="fullwidth">
			</a>
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Corporate contacts</h3>
			<p class="lead">
				Company view (grid) type of admin screen. With filters, search, export, bulk actions and mailing list creation features. 
			</p>
			<p>
				<strong>Toolbar</strong> on top offers:<br>
				<code><i class="fa fa-envelope-o"></i> Static mail list</code> - to create static mail list of checked rows<br>
				<code><i class="fa fa-filter"></i> Dynamic mail list</code> - to create mail list from defined filters<br>
				<code><i class="fa fa-bell"></i> Profile reminder</code> - remind profile completeness - most common automatic mail action can be triggered manually<br>
				<code><i class="fa fa-trash-o"></i> Delete</code> - delete selected rows<br>
				<code><i class="fa fa-download"></i> Download</code> - export selected rows to one of offered formats (JSON, CSV, PDF)<br>
				<code><i class="fa fa-plus"></i> Create</code> - create new entry<br>
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/1.png" class="lightbox">
				<img src="/about/2/1.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Corporate contact <i class="ion-ios-arrow-thin-right"></i> History</h3>
			<p class="lead">
				<strong>History</strong> is a tab for displaying history of mails sent to the user (when, which and what was the response status).
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/17.png" class="lightbox">
				<img src="/about/2/17.png" class="fullwidth">
			</a>
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Corporate contact <i class="ion-ios-arrow-thin-right"></i> Linked contacts</h3>
			<p class="lead">
				<strong>Linked contacts</strong> serves for updating and adding relations between Individuals and Corporates.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/16.png" class="lightbox">
				<img src="/about/2/16.png" class="fullwidth">
			</a>
		</div>
	</div>

	
	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Individual contacts</h3>
			<p class="lead">
				People view (grid) type of admin screen. With filters, search, export, bulk actions and mailing list creation features.
			</p>
			<p>
				<strong>Toolbar</strong> on top offers:<br>
				<code><i class="fa fa-envelope-o"></i> Static mail list</code> - to create static mail list of checked rows<br>
				<code><i class="fa fa-filter"></i> Dynamic mail list</code> - to create mail list from defined filters<br>
				<code><i class="fa fa-bell"></i> Profile reminder</code> - remind profile completeness - most common automatic mail action can be triggered manually<br>
				<code><i class="fa fa-trash-o"></i> Delete</code> - delete selected rows<br>
				<code><i class="fa fa-download"></i> Download</code> - export selected rows to one of offered formats (JSON, CSV, PDF)<br>
				<code><i class="fa fa-plus"></i> Create</code> - create new entry<br>
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/6.png" class="lightbox">
				<img src="/about/2/6.png" class="fullwidth">
			</a>
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Creating mail list</h3>
			<p class="lead">
				When you create <code><i class="fa fa-envelope-o"></i> static</code> or <code><i class="fa fa-filter"></i> dynamic</code> mail list, you are then prompted to give it label. <strong>Watch out, this label shows to the user in Manage newsletters</strong>
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/8.png" class="lightbox">
				<img src="/about/2/8.png" class="fullwidth">
			</a>
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Maillists</h3>
			<p class="lead">
				From here you can manage all your maillists. When you feel like sending e-mail to given list, click on the list, that will bring you first to maillist detail view and from there you may compose new message to the user.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/9.png" class="lightbox">
				<img src="/about/2/9.png" class="fullwidth">
			</a>
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Maillist detail</h3>
			<p class="lead">
				From here you can manage the mail list - it's <strong>Label</strong>, see it's <strong>Recipients</strong> in the lower part (no matter if statically or dynamically selected).
			</p>
			<p class="lead">
				In the top right corner, you can initiate <strong>Sending e-mail</strong> (the button is green, you won't miss it).
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/10.png" class="lightbox">
				<img src="/about/2/10.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Compose e-mail</h3>
			<p class="lead">
				In the screen that follows, you are able to compose message you want to send to selected mail recipients. Feel free to use variables, as the example shows you, they look like this:<br>
				<code>{{ '$object->user->first_name' }}</code>
			</p>		
			<p>
				List of usable variables is basically identical with list of attributes, but watch out, different attributes behave differently (based on type of attribute).<br>
				<br>
				Here is a short list of the most important:<br>
				<code>{{ '$object->user->first_name' }}</code> - for First name<br>
				<code>{{ '$object->user->last_name' }}</code> - for Last name<br>
				<code>{{ '$object->user->email' }}</code> - for E-mail<br>
				<code>{{ '$object->user->gender' }}</code> - for Gender (returns male|female)<br>
				<br>
				Moreover, you can also touch the application configuration values:<br>
				<code>{{ 'config("platform.app.title")' }}</code> - returns "{{ config("platform.app.title") }}" as the name of application<br>
				<code>{{ 'config("platform.app.tagline")' }}</code> - returns "{{ config("platform.app.tagline") }}" as the tagline of application<br>
			</p>
			<p>
				And if none of that is enough, feel free to use the <a href="https://laravel.com/docs/5.0/templates" rel="nofollow" target="_blank">Blade syntax</a> with all the conditions, loops and extra flavour.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/18.png" class="lightbox">
				<img src="/about/2/18.png" class="fullwidth">
			</a>
		</div>
	</div>
