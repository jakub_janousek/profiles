
	<div class="row">
		<div class="col-sm-6">
			<h3>Landing page</h3>
			<p class="lead">
				Use following login information to sign in:
			</p>
			<p class="lead">
				E-mail: <code>admin@sld.gs</code>
				<br>
				Password: <code>admin11</code>
			</p>
			<p>
				The sample user is "Admin".
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/1.png" class="lightbox">
				<img src="/about/1.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Dashboard</h3>
			<p class="lead">
				As type of user admin, your dashboard has following menus:
			</p>
			<p>
				<strong>Admin menu</strong> on left is used for general content management. You will see there:<br>
				<code><i class="fa fa-building"></i> Corporate contacts</code> - to manage companies<br>
				<code><i class="fa fa-user"></i> Individual contacts</code> - to manage people<br>
				<code><i class="fa fa-user"></i> Acess</code> - to manage users and roles<br>
				<code><i class="fa fa-asterisk"></i> Attributes</code> - to manage fields<br>
				<code><i class="fa fa-tag"></i> Tags</code> - to manage tags<br>
				<code><i class="fa fa-image"></i> Media</code> - for comprehensive asset manager<br>
				<code><i class="fa fa-circle-o"></i> Transaction mails</code> - to manage automatic e-mails<br>
				<code><i class="fa fa-circle-o"></i> Maillist</code> - to manage mail lists<br>
			</p>
			<p>
				<strong>System menu</strong> on top right contains:<br>
				<code><i class="fa fa-laptop"></i> Preview</code> - which takes you back to YORI frontend<br>
				<code><i class="fa fa-sliders"></i> Settings</code> - where you can manage the app settings
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/12.png" class="lightbox">
				<img src="/about/2/12.png" class="fullwidth">
			</a>
		</div>
	</div>
	
	<hr>

	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<p class="lead text-center" style="max-width:100%">
		
				To find out more about common views like <code><i class="fa fa-building"></i> Corporate contacts</code>, <code><i class="fa fa-user"></i> Individual contacts</code>, <code><i class="fa fa-asterisk"></i> Attributes</code>, <code><i class="fa fa-tag"></i> Tags</code>, <code><i class="fa fa-circle-o"></i> Transaction mails</code> or <code><i class="fa fa-circle-o"></i> Maillist</code>, please see the <strong>Editor</strong> tab.

			</p>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Access <i class="ion-ios-arrow-thin-right"></i> Roles</h3>
			<p class="lead">
				Roles grid view shows all the different roles found in system. You can perform the standard operations in toolbar, or to view them or change, click a role.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/13.png" class="lightbox">
				<img src="/about/2/13.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Role <i class="ion-ios-arrow-thin-right"></i> Permissions</h3>
			<p class="lead">
				In role detail, you can give the role different name or slug, but what's probably more interesting is <strong>Permissions</strong>. This tab gives you opportunity to grant different capabilities to given role.
			</p>
			<p>
				Role knows following statuses of permissions:<br>
				<strong class="text-success">Allow</strong> - to allow such operation<br>
				<strong class="text-danger">Deny</strong> - to deny given operation<br>
				If user has multiple roles, the ACL system selects the Allowed to define specific user's rights. To make an <strong>exception</strong> for specific user, which is in given role, you can define the user's specific permission (described bellow).
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/14.png" class="lightbox">
				<img src="/about/2/14.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>User <i class="ion-ios-arrow-thin-right"></i> Permissions</h3>
			<p class="lead">
				User <strong>Permissions</strong> tab is similar to Role <i class="ion-ios-arrow-thin-right"></i> Permissions, but has superior priority to permissions given from Roles, and therefore is ideal to make exceptions.
			</p>
			<p>
				User knows following statuses of permissions:<br>
				<strong class="text-success">Allow</strong> - to allow such operation (overrides role settings)<br>
				<strong class="text-danger">Deny</strong> - to deny given operation (overroides role settings)<br>
				<strong>Inherit</strong> - to inherit rights from role (default)
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/15.png" class="lightbox">
				<img src="/about/2/15.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Settings</h3>
			<p class="lead">
				You can also manage settings <code><i class="fa fa-sliders"></i> Settings</code> there are multiple tabs, first of all, you will see <strong>General settings</strong>.
			</p>
			<p class="lead">
				Here you can manage for example the app <strong>Title</strong> and <strong>Tagline</strong> which are shown on countless occurrences through the system. You can also manage and change the mail driver and details as well as <strong>E-mail sender</strong> (from name and from address). 
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/22.png" class="lightbox">
				<img src="/about/2/22.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Settings <i class="ion-ios-arrow-thin-right"></i> Social</h3>
			<p class="lead">
				Social settings are good if you wish to change the way users authenticate to your site. You may enable and disable different services, change their API credentials.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/23.png" class="lightbox">
				<img src="/about/2/23.png" class="fullwidth">
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<h3>Settings <i class="ion-ios-arrow-thin-right"></i> Maintenance</h3>
			<p class="lead">
				Extension Maintenance will allow you to close the site for either all users, or for users who do not have admin rights or for those that are not in allowed IPs. You might aswell use combination of exceptions named before.
			</p>
		</div>
		<div class="col-sm-6">
			<a href="/about/2/24.png" class="lightbox">
				<img src="/about/2/24.png" class="fullwidth">
			</a>
		</div>
	</div>
	