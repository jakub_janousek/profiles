<div class="footer container">
	<div class="row">
		<div class="col-xs-12 col-sm-3">
			<p class="footer__company montserrat">@setting('platform.app.copyright')</p>
			<p class="footer__tagline">@setting('platform.app.tagline')</p>
		</div>
		<div class="col-xs-12 col-sm-3">
			@nav('footer', 0, 'nav-footer montserrat')
		</div>
		<div class="hidden-xs col-sm-3">
			<p class="montserrat">
				<a href="{{ route('user.register') }}">
					Register
				</a>
			</p>
			<p>Get a new account</p>
		</div>
		<div class="hidden-xs col-sm-3">
			<p class="montserrat">
				<a href="{{ route('user.password_reminder') }}">
					Lost password
				</a>
			</p>
			<p>Get a new password to your account</p>
		</div>
	</div>
</div>

<style type="text/css">

.nav-footer {
	list-style-type: none;
}

.nav-footer li {
	padding-top: 5px;
	padding-bottom: 5px;
}

</style>