<ul class="{{ $cssClass }}" role="navigation">
	@each('partials/nav/child', $children, 'child')
</ul>
