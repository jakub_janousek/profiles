@extends('layouts/landing')

{{-- Page title --}}
@section('title')
@parent
{{{ $page->meta_title or $page->name }}}
@stop

{{-- Meta description --}}
@section('meta-description')
{{{ $page->meta_description }}}
@stop

{{-- Page Header --}}
@section('header')



@stop

{{-- Page content --}}
@section('page')

	@if ( Sentinel::check() )
		<?php $user = Sentinel::getUser(); ?>

		<p class="text-center">
		
			You are logged in as 
			@if ( $user->first_name || $user->last_name )
				{{ $user->first_name }} {{ $user->last_name }}
			@else
				Anonymous user
			@endif

			<br>
			
			{{-- Check if user can access dashboard or is Superuser --}}
			@if (Sentinel::hasAnyAccess(['superuser', 'admin.dashboard']))
				<a href="{{ url('admin') }}">Go to admin</a>
			@endif

		</p>
	@else
		@include('platform/users::auth/login')
	@endif

@stop
