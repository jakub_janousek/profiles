@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ $page->meta_title or $page->name }}}
@stop

{{-- Meta description --}}
@section('meta-description')
{{{ $page->meta_description }}}
@stop

{{ Asset::queue('magnific-popup', 'sleighdogs/profile::magnific-popup/jquery.magnific-popup.js', 'jquery') }}
{{ Asset::queue('magnific-popup', 'sleighdogs/profile::magnific-popup/magnific-popup.css') }}

{{-- Inline Scripts --}}
@section('scripts')
@parent
<script type="text/javascript">
$(document).ready(function() {
	$('.lightbox').magnificPopup({type:'image'});
});
</script>
@stop

{{-- Inline Styles --}}
@section('styles')
@parent
<style type="text/css">
.fullwidth {
	width: 100%;
	height: auto;
}
p {
	max-width: 75%;
}
</style>
@stop

{{-- Page content --}}
@section('page')

<div class="base">

	<div class="page">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs nav-justified" role="tablist">
			<li role="presentation" class="active">
				<a href="#home" aria-controls="home" role="tab" data-toggle="tab" style="text-align:left">
					<strong>
						Super admin
					</strong>
					<br>
					<small>
						Getting started
					</small>
				</a>
			</li>
			<li role="presentation">
				<a href="#individual" aria-controls="individual" role="tab" data-toggle="tab" style="text-align:left">
					<strong>
						Individual
					</strong>
					<br>
					<small>
						Employee & his rights
					</small>
				</a>
			</li>
			<li role="presentation">
				<a href="#corporate" aria-controls="corporate" role="tab" data-toggle="tab" style="text-align:left">
					<strong>
						Corporate
					</strong>
					<br>
					<small>
						Company profile
					</small>
				</a>
			</li>
			<li role="presentation">
				<a href="#admin" aria-controls="admin" role="tab" data-toggle="tab" style="text-align:left">
					<strong>
						Admin
					</strong>
					<br>
					<small>
						System admin
					</small>
				</a>
			</li>
			<li role="presentation">
				<a href="#editor" aria-controls="editor" role="tab" data-toggle="tab" style="text-align:left">
					<strong>
						Editor
					</strong>
					<br>
					<small>
						Managing data
					</small>
				</a>
			</li>
			<li role="presentation">
				<a href="#guest" aria-controls="guest" role="tab" data-toggle="tab" style="text-align:left">
					<strong>
						Guest
					</strong>
					<br>
					<small>
						Viewer & embeds
					</small>
				</a>
			</li>
		</ul>
		
		<!-- Tab panes -->
		<div class="tab-content" style="padding-top:20px;">
		    <div role="tabpanel" class="tab-pane active" id="home">
		    	@include('content/about/general')
		    </div>
		    <div role="tabpanel" class="tab-pane" id="corporate">
		    	@include('content/about/corporate')
		    </div>
		    <div role="tabpanel" class="tab-pane" id="individual">
		    	@include('content/about/individual')
		    </div>
		    <div role="tabpanel" class="tab-pane" id="admin">
		    	@include('content/about/admin')
		    </div>
		    <div role="tabpanel" class="tab-pane" id="editor">
		    	@include('content/about/editor')
		    </div>
		    <div role="tabpanel" class="tab-pane" id="guest">
		    	@include('content/about/guest')
		    </div>
		</div>
		

	</div>

</div>

@stop
