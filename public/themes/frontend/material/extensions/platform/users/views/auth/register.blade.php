@extends('layouts/landing')

{{-- Page title --}}
@section('title')
{{{ trans('platform/users::auth/form.register.legend') }}} ::
@parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/css/selectize.bootstrap3.css') }}

{{-- Inline Scripts --}}
@section('scripts')
@parent
<script type="text/javascript">
$(function(){
	$('[name="type"]').change(function(event){
		event.preventDefault();
		var value = $(this).val();

		if ( value == 'corporate' ) {
			$('.form-group.individual').addClass('hidden');
			$('.form-group.corporate').removeClass('hidden');

			$('.form-group.individual input, .form-group.individual select').removeAttr('required');
			//$('.form-group.corporate input, .form-group.corporate select').attr('required', 'required');
		} else {
			$('.form-group.individual').removeClass('hidden');
			$('.form-group.corporate').addClass('hidden');

			$('.form-group.corporate input, .form-group.corporate select').removeAttr('required');
			$('.form-group.individual input, .form-group.individual select').attr('required', 'required');
		}
	});

	$('.selectize').each(function(){
		var placeholder = $(this).data('placeholder');

		$('.selectize').selectize({
		    persist: false,
		    create: true,
		    placeholder: placeholder,
		    onChange: function(value) {
            	$('[name="root"]').val(value);
          	}
		});
	});
});
</script>
@stop

@section('styles')
@parent
<style type="text/css">
.parsley-custom-error-message, .parsley-required {
	color: red;
	font-size: 11px;
	padding-left: 12px;
	padding-bottom: 5px;
}
.selectize-input {
	border: 0;
	outline: none;
}
.has-error .help-block {
	display: block!important;
	color: red;
}
</style>
@stop

{{-- Page content --}}
@section('page')

<div class="row">

	<div class="col-md-6 col-md-offset-3">

		{{-- Form --}}
		<form id="register-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate novalidate>

			{{-- Form: CSRF Token --}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="panel panel-brand">

				<div class="panel-heading">{{{ trans('platform/users::auth/form.register.legend') }}}</div>

				<div class="panel-body">

					{{-- Email Address --}}
					<div class="form-group{{ Alert::onForm('email', ' has-error') }} form-group-default">

						<label class="control-label" for="email">{{{ trans('platform/users::auth/form.email') }}}</label>

						<input class="form-control" type="email" name="email" id="email" value="{{{ Input::old('email') }}}" placeholder="{{{ trans('platform/users::auth/form.email_placeholder') }}}"
						required
						autofocus
						data-parsley-trigger="change"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.email_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('email') ?: trans('platform/users::auth/form.email_help') }}}
						</span>

					</div>

					{{-- Password --}}
					<div class="form-group{{ Alert::onForm('password', ' has-error') }} form-group-default">

						<label class="control-label" for="password">{{{ trans('platform/users::auth/form.password') }}}</label>

						<input class="form-control" type="password" name="password" id="password" value="{{{ Input::old('password') }}}" placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
						required
						data-parsley-trigger="change"
						data-parsley-minlength="6"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.password_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password') ?: trans('platform/users::auth/form.password_help') }}}
						</span>

					</div>

					{{-- Confirm Password --}}
					<div class="form-group{{ Alert::onForm('password_confirmation', ' has-error') }} form-group-default">

						<label class="control-label" for="password_confirmation">{{{ trans('platform/users::auth/form.password_confirmation') }}}</label>

						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="{{{ Input::old('password_confirmation') }}}" placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
						required
						data-parsley-trigger="change"
						data-parsley-equalto="#password"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.password_confirmation_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password_confirmation') ?: trans('platform/users::auth/form.password_confirmation_help') }}}
						</span>

					</div>

					<div class="form-group form-group-default">

						<label for="type" class="control-label">Type</label>

						<select name="type" id="type" class="form-control" data-parsley-trigger="change" required>
							<option value="individual">Individual</option>
							<option value="corporate">Corporate</option>
						</select>

						<span class="help-block">
							{{{ Alert::onForm('type') ?: '' }}}
						</span>

					</div>

					<div class="form-group corporate hidden form-group-default">

						<label for="brand_name" class="control-label">Company name</label>

						<input name="brand_name" id="brand_name" class="form-control" data-parsley-trigger="change" placeholder="Company name">

						<span class="help-block">
							{{{ Alert::onForm('brand_name') ?: '' }}}
						</span>

					</div>

					<div class="form-group corporate hidden form-group-default">

						<label for="number_of_employees" class="control-label">Number of employees</label>

						<select name="number_of_employees" id="number_of_employees" class="form-control" data-parsley-trigger="change" required>
							<?php $attribute = \Platform\Attributes\Models\Attribute::whereSlug('number_of_employees')->first(); ?>
			                @foreach( $attribute->options as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>

						<span class="help-block">
							{{{ Alert::onForm('number_of_employees') ?: '' }}}
						</span>

					</div>
	
					<div class="form-group corporate hidden form-group-default">

						<label for="street" class="control-label">Street</label>

						<input name="street" id="street" class="form-control" data-parsley-trigger="change" placeholder="Street">

						<span class="help-block">
							{{{ Alert::onForm('street') ?: '' }}}
						</span>

					</div>

					<div class="form-group corporate hidden form-group-default">

						<label for="postcode" class="control-label">Postcode</label>

						<input name="postcode" id="postcode" class="form-control" data-parsley-trigger="change" placeholder="Postcode">

						<span class="help-block">
							{{{ Alert::onForm('postcode') ?: '' }}}
						</span>

					</div>

					<div class="form-group corporate hidden form-group-default">

						<label for="city" class="control-label">City</label>

						<input name="city" id="city" class="form-control" data-parsley-trigger="change" placeholder="City">

						<span class="help-block">
							{{{ Alert::onForm('city') ?: '' }}}
						</span>

					</div>

					<div class="form-group individual form-group-default">

						<label for="first_name" class="control-label">First name</label>

						<input name="first_name" id="first_name" class="form-control" data-parsley-trigger="change" required placeholder="First name">

						<span class="help-block">
							{{{ Alert::onForm('first_name') ?: '' }}}
						</span>

					</div>

					<div class="form-group individual form-group-default">

						<label for="last_name" class="control-label">Last name</label>

						<input name="last_name" id="last_name" class="form-control" data-parsley-trigger="change" required placeholder="last name">

						<span class="help-block">
							{{{ Alert::onForm('last_name') ?: '' }}}
						</span>

					</div>

					<div class="form-group individual form-group-default">

						<label for="root" class="control-label">Company</label>

						<select name="root" id="root" class="selectize" data-parsley-trigger="change" data-placeholder="Select company">
							<option></option>
							<?php $companies = \Sleighdogs\Profile\Models\Corporate::all(); ?>
			                @foreach( $companies as $company )
							<option value="{{ $company->id }}">{{ $company->brand_name }}</option>
							@endforeach
						</select>

						<span class="help-block">
							{{{ Alert::onForm('root') ?: '' }}}
						</span>

					</div>

					<div class="form-group individual form-group-default">

						<label for="position" class="control-label">Position</label>

						<select name="position" id="position" class="form-control" data-parsley-trigger="change" required>
							<?php $attribute = \Platform\Attributes\Models\Attribute::whereSlug('position')->first(); ?>
			                @foreach( $attribute->options as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>

						<span class="help-block">
							{{{ Alert::onForm('position') ?: '' }}}
						</span>

					</div>

					<div class="form-group individual form-group-default">

						<label for="gender" class="control-label">Gender</label>

						<select name="gender" id="gender" class="form-control" data-parsley-trigger="change" required>
							<?php $attribute = \Platform\Attributes\Models\Attribute::whereSlug('gender')->first(); ?>
			                @foreach( $attribute->options as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>

						<span class="help-block">
							{{{ Alert::onForm('gender') ?: '' }}}
						</span>

					</div>

					<hr>

					{{-- Form actions --}}
					<div class="form-group text-center">

						<button class="btn btn-default" type="submit">{{{ trans('platform/users::auth/form.register.submit') }}}</button>

						<p class="help-block text-center">Already have an Account? <a href="{{ URL::route('user.login') }}">{{{ trans('platform/users::auth/form.login.legend') }}}</a></p>

					</div>

				</div>

			</div>

		</form>

	</div>

</div>

@stop
