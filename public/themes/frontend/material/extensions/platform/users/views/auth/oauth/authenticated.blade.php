@extends('templates/default')

@section('page')
<div class="container">
	<h3>Congratulations! <small>You're authenticated and logged in.</small></h3>
</div>
@endsection
