@extends('layouts/landing')

{{-- Page title --}}
@section('title')
{{{ trans('platform/users::auth/form.reset-password.legend') }}} ::
@parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline Scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')

<div class="row">

	<div class="col-md-6 col-md-offset-3">

		{{-- Form --}}
		<form id="reset-password-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

			{{-- Form: CSRF Token --}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="panel panel-brand">

				<div class="panel-heading" style="font-size:12px;">{{{ trans('platform/users::auth/form.reset-password.legend') }}}</div>

				<div class="panel-body">

					{{-- Password --}}
					<div class="form-group{{ Alert::onForm('password', ' has-error') }}">

						<label class="control-label" for="password">{{{ trans('platform/users::auth/form.reset-password.password') }}}</label>

						<input class="form-control" type="password" name="password" id="password" value="{{{ Input::old('password') }}}" placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
						required
						data-parsley-trigger="change"
						data-parsley-minlength="6"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.password_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password') ?: trans('platform/users::auth/form.reset-password.password_help') }}}
						</span>

					</div>

					{{-- Confirm Password --}}
					<div class="form-group{{ Alert::onForm('password_confirmation', ' has-error') }}">

						<label class="control-label" for="password_confirmation">{{{ trans('platform/users::auth/form.reset-password.password_confirmation') }}}</label>

						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="{{{ Input::old('password_confirmation') }}}" placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
						required
						data-parsley-trigger="change"
						data-parsley-equalto="#password"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.password_confirmation_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password_confirmation') ?: trans('platform/users::auth/form.reset-password.password_confirmation_help') }}}
						</span>

					</div>

					{{-- Form actions --}}
					<div class="form-group">

						<button class="btn btn-default btn-block" type="submit">{{{ trans('platform/users::auth/form.reset-password.submit') }}}</button>

					</div>

				</div>

			</div>

		</form>

	</div>

</div>

@stop
