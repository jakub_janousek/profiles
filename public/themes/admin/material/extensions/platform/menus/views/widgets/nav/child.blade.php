<li class="{{ $child->isActive ? 'active' : null }} menu-{{ $child->slug }}">
	<a target="{{ $child->target }}" @if ($child->children and ! $child->hasSubItems) href="javascript:;"  @else href="{{ $child->uri }}" @endif>
		<span class="icon-thumbnail"><i class="{{ $child->class }}"></i></span>
		@if ($child->children)<span class="title">@endif
			{{ $child->name }}
		@if ($child->children)</span>@endif
		@if ($child->children and ! $child->hasSubItems)
		<span class=" arrow"></span>
		@endif
	</a>

	@if ($child->children)
		<ul class="sub-menu" role="menu" aria-labelledby="drop-{{ $child->slug }}">
		@each('platform/menus::widgets/nav/child', $child->children, 'child')
		</ul>
	@endif
</li>
