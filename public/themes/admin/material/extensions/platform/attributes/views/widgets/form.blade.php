@if (!  $form)

	<fieldset>
		<legend>No attributes available</legend>

		<br />

		<p><a class="btn btn-primary btn-lg" href="{{ route('admin.attribute.create') }}?namespace={{ $namespace }}&amp;previous_url={{ request()->path() }}" role="button">Add fields</a></p>
	</fieldset>

@else

	<fieldset>
		<legend>Attributes</legend>

		{!! $form !!}
	</fieldset>

	<hr>

	<p><a class="btn btn-primary btn-lg" href="{{ route('admin.attribute.create') }}?namespace={{ $namespace }}&amp;previous_url={{ request()->path() }}" role="button">Add fields</a></p>

@endif
