@extends('layouts/default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans('platform/dashboard::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('index', 'platform/dashboard::css/index.scss', 'style') }}

{{-- @todo make conditional --}}

{{-- Inline scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript">
        $(function () {
            // Init select2
            $.fn.select2 && $('[data-init-plugin="select2"]').each(function () {
                $(this).select2({
                    minimumResultsForSearch: ($(this).attr('data-disable-search') == 'true' ? -1 : 1),
                    formatResult           : function (input) {
                        var css = $(input.element).data('css');
                        return '<span class="' + css + '">' + input.text + '</span>';
                    },
                    formatSelection        : function (input) {
                        var css = $(input.element).data('css');
                        return '<span class="' + css + '">' + input.text + '</span>';
                    }
                }).on('select2-opening', function () {
                    $.fn.scrollbar && $('.select2-results').scrollbar({
                        ignoreMobile: false
                    })
                });
            });

            $('[data-status-change]').change(function (event) {
                var id      = $(this).data('id'),
                    val     = $(this).val(),
                    route   = $(this).data('status-route'),
                    success = $(this).data('msg-success');

                $.ajax({
                    method: "POST",
                    url   : route,
                    data  : {order_id: id, status: val},
                    cache : false
                }).done(function (data) {
                    // order status changed
                    $('body').pgNotification({
                        'style'    : 'bar',
                        'position' : 'top',
                        'message'  : success,
                        'type'     : 'alert alert-success',
                        'showClose': false
                    }).show();
                });
            });

            $('[name="tracking_number"]').change(function (event) {
                var id      = $(this).data('id'),
                    val     = $(this).val(),
                    route   = $(this).data('tracking-route'),
                    success = $(this).data('msg-success');

                $.ajax({
                    method: "POST",
                    url   : route,
                    data  : {order_id: id, tracking_number: val},
                    cache : false
                }).done(function (data) {
                    // order status changed
                    $('body').pgNotification({
                        'style'    : 'bar',
                        'position' : 'top',
                        'message'  : success,
                        'type'     : 'alert alert-success',
                        'showClose': false
                    }).show();
                });
            });
        });
    </script>
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page content --}}
@section('page')

    <?php $extensions = Extensions::allInstalled(); ?>

    @if ( isset($extensions['sanatorium/analytics']) )
        <div class="panel">
            <div class="panel-body">
                @widget('sanatorium/analytics::visitors.getVisitorsAndPageViews', [30])
            </div>
        </div>
    @endif

    @if ( isset($extensions['sanatorium/orders']) && isset($extensions['sanatorium/status']) && class_exists('Status') )

        <a href="{{ route('admin.sanatorium.orders.orders.all') }}" class="btn btn-link">

            {{ trans('sanatorium/orders::general.orders.all') }}

        </a>

        <div class="table-responsive" style="height:400px">

            <?php $statuses = Status::where('status_entity', 'Sanatorium\orders\Models\Order')->get(); ?>

            <table class="table table-hover">

                <thead>
                <th>{{ trans('sanatorium/orders::general.placed_at') }}</th>
                <th></th>
                <th>{{ trans('sanatorium/orders::cart.delivery.title') }}</th>
                <th></th>
                <th>{{ trans('sanatorium/orders::general.payment') }}</th>
                <th>{{ trans('sanatorium/orders::general.tracking_no') }}</th>
                <th></th>
                </thead>

                <tbody>

                @foreach(\Sanatorium\orders\Models\Order::orderBy('created_at', 'DESC')->take(10)->get() as $order)

                    <tr>
                        <td>
                            {{ $order->created_at->format('j.n. H:i') }}
                        </td>
                        <td>
                            <select name="status" data-id="{{ $order->id }}" class="select2"
                                    data-init-plugin="select2" style="width:135px;" data-status-change
                                    data-status-route="{{ route('admin.sanatorium.orders.orders.status') }}"
                                    data-msg-success="{{ trans('sanatorium/orders::orders/common.actions.status_change.success') }}">
                                <option value="0"
                                        data-css="badge badge-default">{{ trans('sanatorium/status::statuses/common.not_specified') }}</option>
                                @foreach($statuses as $status)
                                    <option value="{{ $status->id }}"
                                            data-css="{{ $status->css_class }}" {{ ($order->status->id == $status->id ? 'selected' : null) }}>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <?php $address = Sanatorium\Addresses\Models\Address::find($order->address_delivery_id) ?>
                            <p>{{ $address->name }}, {{ $address->street }}</p>
                            <p>{{ $address->postcode }}, {{ $address->city }}</p>
                            <p>{{ $address->country }}</p>
                        </td>
                        <td>
                            <?php
                            $cart = unserialize($order->cart);
                            $item_collections = array_values($cart['items']);

                            // Check each item if it was reviewed
                            foreach( $item_collections as $item ) {
                            if ( $product = Product::find($item->id) ) {
                            ?>
                            {{ $item->quantity }}&times;
                            <a href="{{ route('admin.sanatorium.shop.products.edit', $product->id) }}">
                                {{ $product->product_title }} (ID:{{ $product->id }})<br>
                            </a>
                            <?php
                            }
                            }
                            ?>
                        </td>
                        <td>
                            {{ $order->payment_provider_status_human_readable }}
                        </td>
                        <td>
                            <input type="text" name="tracking_number" class="form-control"
                                   data-id="{{ $order->id }}" placeholder="Tracking number"
                                   data-msg-success="Tracking number succesfully changed"
                                   data-tracking-route="{{ route('admin.sanatorium.orders.orders.tracking') }}"
                                   value="{{ $order->tracking_number }}">
                        </td>
                        <td class="text-right">
                            <a href="{{ route('admin.sanatorium.orders.orders.edit', [$order->id]) }}">
                                {{ trans('action.show') }}
                            </a>
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>

        @endif


                @if ( isset($extensions['platform/users']) )
                    <div class="col-sm-12 m-b-10">

                        <div class="panel panel-grid">

                            <div class="widget-16-header padding-20">
                                <span class="icon-thumbnail bg-master-light pull-left text-master">us</span>
                                <div class="pull-left">
                                    <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">
                                        Newest</p>
                                    <h5 class="no-margin overflow-ellipsis ">Registered users</h5>
                                </div>
                                <h3 class="pull-right semi-bold">
                                    <a href="{{ route('admin.users.all') }}" class="btn btn-link">

                                        All users

                                    </a>
                                </h3>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-body">

                            </div>

                            <div class="table-responsive" style="height:400px">

                                <table class="table table-hover">

                                    <thead>
                                    <th>Name</th>
                                    <th class="text-right">Created at</th>
                                    </thead>

                                    <tbody>

                                    @foreach(\Platform\Users\Models\User::orderBy('created_at', 'DESC')->take(10)->get() as $user)

                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.user.edit', [$user->id]) }}">
                                                    @if ( !$user->first_name && !$user->last_name )
                                                        {{ $user->email }}
                                                    @endif
                                                    {{ $user->first_name }}
                                                    {{ $user->last_name }}
                                                </a>
                                            </td>
                                            <td class="text-right">{{ $user->created_at->format('j.n.Y') }}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>
            </div>
            @endif

            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/search']) )
                        @widget('sanatorium/search::queries.top', [10])
                    @endif
                </div>

            </div>
            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/analytics']) )
                        @widget('sanatorium/analytics::visitors.getRegional', [365])
                    @endif
                </div>

            </div>

            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/analytics']) )
                        @widget('sanatorium/analytics::visitors.registrations')
                    @endif
                </div>

            </div>

            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/pricing']) )
                        @widget('sanatorium/pricing::currencymonth.show')
                    @endif
                </div>

            </div>
            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/pricing']) )
                        @widget('sanatorium/pricing::currencycontrols.show')
                    @endif
                </div>

            </div>


            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/pricing']) )
                        @widget('sanatorium/pricing::currencytoprimary.show', ['eur'])
                    @endif
                </div>

            </div>
            <div class="row">

                <div class="col-md-12 m-b-10">
                    @if ( isset($extensions['sanatorium/pricing']) )
                        @widget('sanatorium/pricing::currencytoprimary.show', ['usd'])
                    @endif
                </div>

            </div>

            @if ( config('platform.app.title') == 'Candidbeaute' )

                <div class="row">

                    <div class="col-sm-6 m-b-10">

                        <div class="panel panel-grid">

                            <div class="widget-16-header padding-20">
                                <span class="icon-thumbnail bg-master-light pull-left text-master">pro</span>
                                <div class="pull-left">
                                    <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">
                                        Newest</p>
                                    <h5 class="no-margin overflow-ellipsis ">Makeup Pro</h5>
                                </div>
                                <h3 class="pull-right semi-bold">

                                </h3>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-body">

                            </div>

                            <div class="table-responsive" style="height:400px">

                                <?php $statuses = Status::where('status_entity', 'Sanatorium\orders\Models\Order')->get(); ?>

                                <table class="table table-hover">

                                    <thead>
                                    <th>Makeup Pro</th>
                                    <th></th>
                                    <th></th>
                                    </thead>

                                    <tbody>

                                    @foreach(\MakeupPro::orderBy('id', 'DESC')->take(10)->get() as $pro)

                                        <tr>
                                            <td>{{ $pro->email }}</td>
                                            <td>{{ $pro->created_at->format('j.n.Y H:i') }}</td>
                                            <td class="text-right">
                                                <a href="{{ route('send.makeup.pro', $pro->id) }}"
                                                   class="btn btn-primary">
                                                    Send E-mail
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            @endif

        </div>

@stop
