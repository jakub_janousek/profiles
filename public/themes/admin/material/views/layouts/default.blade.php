<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>
		@section('title')
		@show
		| @setting('platform.app.title')
	</title>
	<meta name="description" content="@yield('meta-description')">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="base_url" content="{{ url('/') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	{{-- Queue assets --}}
	{{ Asset::queue('pace', 'pace/pace-theme-flash.css') }}
	{{ Asset::queue('jquery-scrollbar', 'jquery-scrollbar/jquery.scrollbar.css') }}
	{{ Asset::queue('switchery', 'switchery/css/switchery.min.css') }}
	{{ Asset::queue('bootstrap-select2', 'bootstrap-select2/select2.css') }}
	{{ Asset::queue('font-awesome', 'font-awesome/css/font-awesome.min.css', 'bootstrap') }}

	{{ Asset::queue('pace', 'pace/js/pace.min.js') }}
	{{ Asset::queue('modernizr', 'modernizr/js/modernizr.js', 'pace') }}
	{{ Asset::queue('jquery', 'jquery/js/jquery.js', 'modernizr') }}
	{{ Asset::queue('bootstrap', 'bootstrap/js/bootstrap.min.js', 'jquery') }}
	{{ Asset::queue('perfect-scrollbar', 'perfect-scrollbar/js/perfect-scrollbar.min.js', 'bootstrap') }}
	{{ Asset::queue('metis-menu', 'metis-menu/js/metisMenu.min.js', 'perfect-scrollbar') }}
	{{ Asset::queue('platform', 'platform/js/platform.js', 'metis-menu') }}
	
	{{ Asset::queue('switchery', 'switchery/js/switchery.min.js', 'jquery') }}
	{{ Asset::queue('jquery-easy', 'jquery/jquery-easy.js', 'jquery') }}
	{{ Asset::queue('jquery-unveil', 'jquery-unveil/jquery.unveil.min.js', 'jquery') }}
	{{ Asset::queue('jquery-bez', 'jquery-bez/jquery.bez.min.js', 'jquery') }}
	{{ Asset::queue('imagesloaded', 'imagesloaded/imagesloaded.pkgd.min.js', 'jquery') }}
	{{ Asset::queue('jquery-actual', 'jquery-actual/jquery.actual.min.js', 'jquery') }}
	{{ Asset::queue('jquery-scrollbar', 'jquery-scrollbar/jquery.scrollbar.min.js', 'jquery') }}

	{{ Asset::queue('pages', 'pages/scss/pages.scss') }}
	{{ Asset::queue('pages-icons', 'pages/css/pages-icons.css') }}
	{{ Asset::queue('pages', 'pages/js/pages.js', 'jquery') }}

	@if ( isset($ie8_compatibility) )
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	@endif

	<link rel="shortcut icon" href="{{ Asset::getUrl('platform/img/favicon.png') }}">

	{{-- Compiled styles --}}
	@foreach (Asset::getCompiledStyles() as $style)
	<link href="{{ $style }}" rel="stylesheet">
	@endforeach

	{{-- Call custom inline styles --}}
	@section('styles')
	@show

	{{-- Global js configuration --}}
	<script type="text/javascript">
		window.configuration = {
			'api' : {}
		};
	</script>

</head>

<body class="menu-pin screen-{{ implode('-', explode('/', $_SERVER['REQUEST_URI'])) }}">

	<!--[if lt IE 7]>
	<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
	<![endif]-->

	{{-- Sidebar --}}
	@include('partials/sidebar')
 
	{{-- Page --}}
	<main class="page page-container">
		
		{{-- Header --}}
		@include('partials/header')

		{{-- Alerts --}}
		@include('partials/alerts')

		<div class="page-content-wrapper">

	        <!-- START PAGE CONTENT -->
	        <div class="content">

			<!-- START CONTAINER FLUID -->
			{{-- Page --}}
			<div class="page__content container-fluid container-fixed-lg">
				@yield('page')
			</div>
			<!-- END CONTAINER FLUID -->
			</div>
			
			{{-- Footer --}}
			@include('partials/footer')
		</div>

	</main>

	{{-- Quickview --}}
	<div id="quickview" class="quickview-wrapper" data-pages="quickview" style="z-index:5000">
		<a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview">
			<i class="pg-close"></i>
		</a>
		<div class="tab-content">
			<div class="tab-pane fade active in no-padding" id="quickview-help">
				<div class="view-port clearfix quickview-help" id="help-quick">
					@help(Route::getCurrentRoute())
				</div>
			</div>
		</div>
	</div>

	{{-- Modals --}}
	@include('partials/modals')

	{{-- Footer hooks --}}
	@hook('admin.scripts.footer')

	{{-- Compiled scripts --}}
	@foreach (Asset::getCompiledScripts() as $script)
	<script src="{{ $script }}"></script>
	@endforeach

	{{-- Call custom inline scripts --}}
	@section('scripts')
	<script type="text/javascript">
	$(function(){
	});
	</script>
	@show
	
	{{-- Very end of inline scripts --}}
	<script type="text/javascript">
		if ( typeof moment !== 'undefined' ) { 
			moment.locale('{{ config('app.locale') }}');
		}
	</script>

</body>

</html>
