<div class="modal fade" id="modal-notice" tabindex="-1" role="dialog" aria-labelledby="model-notice-label" aria-hidden="true">

	<div class="modal-dialog">

		<div class="modal-content-wrapper">

			<div class="modal-content">

				<div class="modal-header clearfix text-left">

					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">

						<i class="pg-close fs-14"></i>

					</button>

					<h5>{{{ trans('common.warning') }}}</h5>

				</div>

				<div class="modal-body text-center">

					<i class="fa fa-exclamation fa-5x"></i>

					<h4>{{{ trans('common.warning') }}}</h4>

					<p class="lead"></p>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-block" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
				</div>

			</div>

		</div>

	</div>

</div>
