<!-- START FOOTER -->
<div class="container-fluid container-fixed-lg footer">
  <div class="copyright sm-text-center">
      <p class="small no-margin pull-right sm-pull-reset">
        <span class="hint-text">@setting('platform.app.copyright')</span>
      </p>
    <div class="clearfix"></div>
  </div>
</div>
<!-- END FOOTER -->