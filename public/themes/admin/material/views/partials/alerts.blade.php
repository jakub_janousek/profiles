@if (Alert::whereArea('form')->get())

@section('scripts')
@parent
<script type="text/javascript">
$(function(){

	$('body').pgNotification({
		'style' : 'bar',
		'position' : 'top',
		'message' : '{{ trans('message.check_form') }}',
		'type' : 'danger',
		'showClose' : false
	}).show();

});
</script>
@stop

@endif

@foreach (Alert::whereNotArea('form')->get() as $alert)

@section('scripts')
@parent
<script type="text/javascript">
$(function(){

	$('body').pgNotification({
		'style' : 'bar',
		'position' : 'top',
		'message' : '{{ $alert->message }}',
		'type' : '{{ $alert->class }}',
		'showClose' : false
	}).show();

});
</script>
@stop

@endforeach
