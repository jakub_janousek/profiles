{{-- START HEADER --}}
<div class="header ">

  {{-- START MOBILE CONTROLS --}}

  {{-- LEFT SIDE --}}
  <div class="pull-left full-height visible-sm visible-xs">
    
    {{-- START ACTION BAR --}}
    <div class="sm-action-bar">
      <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
        <span class="icon-set menu-hambuger"></span>
      </a>
    </div>
    {{-- END ACTION BAR --}}

  </div>


  {{-- RIGHT SIDE --}}
  <div class="pull-right full-height visible-sm visible-xs">
    
    {{-- START ACTION BAR --}}
    <div class="sm-action-bar">
      <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
        <span class="icon-set menu-hambuger-plus"></span>
      </a>
    </div>
    {{-- END ACTION BAR --}}

  </div>
  {{-- END MOBILE CONTROLS --}}

  <div class=" pull-left">
    <div class="header-inner">
      <div class="brand inline">
        <?php

        // Try to find parent route
        $current_route = Route::getCurrentRoute();
        $current_route_name = $current_route->getName();

        // Build index route
        // @note array lists possible routes that are contained within extension
        //      admin area, by replacing those with .all, we get index route
        $index_route_name = str_replace([
          'edit',
          'create',
          'manage',
          'delete',
          ], 'all', $current_route_name);

        // Check if vendor is included
        if ( strpos($index_route_name, 'platform') === false && 
          strpos($index_route_name, 'sanatorium') === false &&
          strpos($index_route_name, 'sleighdogs') === false ) {
          $trans_slug = 'platform.' . $index_route_name;
        } else {
          $trans_slug = $index_route_name;
        }

        // Build title
        $trans_slug = str_replace(['admin.', '.all'], '', $trans_slug);

        if ( substr_count($trans_slug, '.') == 1 ) {
          $trans_slug = preg_replace('/\./', '/', $trans_slug, 1);
          $trans_slug = str_replace('.', '::', $trans_slug) . '::common.title';
        } else {
          $trans_slug = preg_replace('/\./', '/', $trans_slug, 1);
          $trans_slug = str_replace('.', '::', $trans_slug) . '/common.title';
        }

        // Replace common route misspells and changes
        $trans_slug = str_replace('setting', 'settings', $trans_slug);

        ?>
        @if ( Route::has($index_route_name) )
          <a href="{{ route($index_route_name) }}">
            {{ trans($trans_slug) }}  
          </a>
        @endif
      </div>
    </div>
  </div>
  
  <div class=" pull-left hidden-sm hidden-xs">
    @include('partials/breadcrumb')
  </div>

  <div class="pull-right visible-lg visible-md">
    <div class="header-inner">
      @nav('system', 0, 'nav nav-pills', '', 'partials/navigation/system')
    </div>
  </div>
  
  <div class=" pull-right">

    {{-- START User Info --}}
    <div class="visible-lg visible-md dropdown navbar-button">
      <div class="pull-right dropdown-toggle" type="button" data-toggle="dropdown">
        {{ $currentUser->email }}
        <i class="pg-arrow_minimize"></i>
      </div>
      <ul class="dropdown-menu profile-dropdown" role="menu">
        <li><a href="{{ url()->toAdmin("users/{$currentUser->id}") }}">Profile</a></li>
        <li class="divider"></li>
        <li><a href="{{ url()->to('/logout') }}">Sign Out</a></li>
      </ul>
    </div>
    {{-- END User Info --}}

  </div>
</div>
{{-- END HEADER --}}

<div class="hidden-lg hidden-md" style="padding-top:50px">
  @include('partials/breadcrumb')
</div>

