<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	
	<?php
	/**
	 * Breadcrumb 0: Preview
	 */
	?>
	<li>
		<a href="{{ url('/') }}" target="_blank">
			{{ config('platform.app.title') }}
		</a>
	</li>

	<?php
	/**
	 * Breadcrumb 1: Home
	 */
	?>
	<li>
		<a href="/{{ admin_uri() }}">
			{{ trans('general.home') }}
		</a>
	</li>

	<?php
	/**
	 * Breadcrumb 2: Extension
	 */

	// Try to find parent route
	$current_route = Route::getCurrentRoute();
	$current_route_name = $current_route->getName();

	// Build index route
	// @note array lists possible routes that are contained within extension
	// 			admin area, by replacing those with .all, we get index route
	$index_route_name = str_replace([
		'edit',
		'create',
		'manage',
		'delete',
		'compose',
		], 'all', $current_route_name);

	// Check if vendor is included
	if ( strpos($index_route_name, 'platform') === false && 
		strpos($index_route_name, 'sanatorium') === false &&
		strpos($index_route_name, 'sleighdogs') === false ) {
		$trans_slug = 'platform.' . $index_route_name;
	} else {
		$trans_slug = $index_route_name;
	}

	// Build title
	$trans_slug = str_replace(['admin.', '.all'], '', $trans_slug);

	if ( substr_count($trans_slug, '.') == 1 ) {
		$trans_slug = preg_replace('/\./', '/', $trans_slug, 1);
		$trans_slug = str_replace('.', '::', $trans_slug) . '::common.title';
	} else {
		$trans_slug = preg_replace('/\./', '/', $trans_slug, 1);
		$trans_slug = str_replace('.', '::', $trans_slug) . '/common.title';
	}

	// Replace common misspells
	$trans_slug = str_replace('platform/setting', 'platform/settings', $trans_slug);

	?>
	@if ( Route::has($index_route_name) )
	<li>
		<a href="{{ route($index_route_name) }}" class="{{ $current_route_name == $index_route_name ? 'active' : null }}">
			{{ trans($trans_slug) }}	
		</a>
	</li>
	@endif

	<?php
	/**
	 * Breadcrumb 3: Current endpoint
	 */
	?>
	@if ( $current_route_name != $index_route_name )
	<li>
		
		<?php
		/**
		 * Get page title without "platform.app.title",
		 * loads the full title to output buffer,
		 * than replace the config value.
		 */
		ob_start(); 
		?>
		
		@yield('title')

		<?php 
		$title_contents = ob_get_contents();
		ob_end_clean(); 
		?>

		<a href="{{ $_SERVER['REQUEST_URI'] }}" class="active">
			{{ trim(str_replace(config('platform.app.title'), '', $title_contents)) }}	
		</a>

	</li>
	@endif

</ul>
<!-- END BREADCRUMB -->