{{-- BEGIN SIDEBAR --}}
<div class="page-sidebar" data-pages="sidebar">
	
	{{-- BEGIN SIDEBAR MENU TOP TRAY CONTENT--}}
	<div class="sidebar-overlay-slide from-top" id="appMenu">
		<div class="row">
			
		</div>
	</div>
	{{-- END SIDEBAR MENU TOP TRAY CONTENT--}}

	{{-- BEGIN SIDEBAR HEADER --}}
	<div class="sidebar-header">

		{{-- @todo: display conditionally - only if extension is installed --}}
		<a href="#" class="search-link action-link" data-toggle="search">
			<i class="pg-search"></i>
			{{ trans('sanatorium/search::general.search_cta') }}
		</a>

	</div>
	{{-- END SIDEBAR HEADER --}}

	{{-- BEGIN SIDEBAR MENU --}}
	<div class="sidebar-menu">

		@nav('admin', 0, 'menu-items', admin_uri())
		
		<div class="clearfix"></div>

	</div>
	{{-- END SIDEBAR MENU --}}
</div>
{{-- END SIDEBAR --}}