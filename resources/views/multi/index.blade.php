<!DOCTYPE html>
    <head>

    <meta charset="UTF-8">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    </head>
    <body>

        <form method="POST">
            <input type="hidden" value="{{ csrf_token() }}" name="_token">

            <div class="container">

                <div class="col-sm-10 col-sm-offset-1">

                    @if ( isset($installations) && is_array($installations) )

                        <table class="table">

                            <thead>
                                <th>Name</th>
                                <th>Prefix</th>
                                <th>Host</th>
                                <th>Filesystem prefix</th>
                                <th>Url</th>
                                <th>Locale</th>
                                <th>Delete</th>
                            </thead>

                        @foreach( $installations as $installation )

                            <?php extract($installation) ?>

                            <tr>
                                <td>{{ $name }}</td>
                                <td>{{ $prefix }}</td>
                                <td>{{ $host }}</td>
                                <td>{{ $filesystem_prefix }}</td>
                                <td>
                                    @if ( is_array($url) )
                                        @foreach( $url as $uri )
                                            {{ $uri }}<br>
                                        @endforeach
                                    @else
                                        {{ $url }}
                                    @endif
                                </td>
                                <td>{{ $locale }}</td>
                                <td>
                                    <a href="{{ route('platform.multi.delete', ['prefix' => $prefix]) }}" class="btn btn-danger btn-sm">
                                        &times;
                                    </a>
                                </td>
                            </tr>

                        @endforeach

                            <tr>
                                <td><input type="text" class="form-control" name="name" placeholder="App name"></td>
                                <td><input type="text" maxlength="6" class="form-control" name="prefix" placeholder="Database prefix without trailing underscore"></td>
                                <td><input type="text" class="form-control" name="host" placeholder="Server host"></td>
                                <td><input type="text" class="form-control" name="filesystem_prefix" placeholder="Filesystem prefix"></td>
                                <td><input type="text" class="form-control" name="url" placeholder="URL with http"></td>
                                <td><input type="text" class="form-control" name="locale" placeholder="cs"></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td colspan="7" class="text-center">
                                    <div class="well">
                                        <button type="submit" class="btn btn-primary">
                                            Add website
                                        </button>
                                    </div>
                                </td>
                            </tr>

                        </table>

                    @endif

                </div>

            </div>

        </form>

    </body>
</html>