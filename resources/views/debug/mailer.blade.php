<form method="POST">
    <label for="name">
        Napište Vaše jméno a email a pošleme Vám emailem pranostiku
    </label>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <input type="text" name="name" placeholder="Jméno" required>

    <input type="email" name="email" placeholder="E-mail" required>

    <button type="submit">
        Odeslat
    </button>
</form>