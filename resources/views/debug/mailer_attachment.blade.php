<form method="POST" enctype="multipart/form-data">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    {{-- Name --}}
    <div>
        <label for="name">
            Napište Vaše jméno
        </label>

        <input type="text" name="name" placeholder="Jméno" required>
    </div>

    {{-- Photo --}}
    <div>
        <label for="photo">
            Přiložte svou fotku
        </label>

        <input type="file" id="photo" name="photo" required>
    </div>

    {{-- Invoice --}}
    <div>
        <label for="photo">
            Přiložte fakturu
        </label>

        <input type="file" id="invoice" name="invoice" required>
    </div>

    <button type="submit">
        Odeslat
    </button>
</form>