<?php

return [

    'Apilogs' => [

        'title' => 'Apilogs',

        'tabs'      => [

            'general'    => 'Apilog',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Apilog [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Apilog was successfully created.',
            'update' => 'Apilog was successfully updated.',
            'delete' => 'Apilog was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the apilog. Please try again.',
            'update' => 'There was an issue updating the apilog. Please try again.',
            'delete' => 'There was an issue deleting the apilog. Please try again.',
        ],

        'general' => [

            'id'          => 'Id',
            'call'        => 'Call',
            'data'        => 'Data',
            'source'      => 'Source',
            'method'      => 'Method',
            'status'      => 'Status',
            'result'      => 'Result',
            'created_at'  => 'Created At',
            'call_help'   => 'Enter the Call here',
            'data_help'   => 'Enter the Data here',
            'source_help' => 'Enter the Source here',

        ],

        'index'  => 'List Apilogs',
        'create' => 'Create new Apilog',
        'edit'   => 'Edit Apilog',
        'delete' => 'Delete Apilog',

    ],

    'Diseases' => [

        'title' => 'Diseases',

        'tabs'      => [

            'general'    => 'Diseases',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Diseases [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Diseases was successfully created.',
            'update' => 'Diseases was successfully updated.',
            'delete' => 'Diseases was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the diseases. Please try again.',
            'update' => 'There was an issue updating the diseases. Please try again.',
            'delete' => 'There was an issue deleting the diseases. Please try again.',
        ],

        'general' => [

            'id'         => 'Id',
            'name'       => 'Name',
            'created_at' => 'Created At',
            'name_help'  => 'Enter the Name here',

        ],


        'index'  => 'List Diseases',
        'create' => 'Create new Diseases',
        'edit'   => 'Edit Diseases',
        'delete' => 'Delete Diseases',

    ],

    'Examinations' => [

        'title' => 'Examinations',

        'tabs'      => [

            'general'    => 'Examination',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Examination [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Examination was successfully created.',
            'update' => 'Examination was successfully updated.',
            'delete' => 'Examination was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the examination. Please try again.',
            'update' => 'There was an issue updating the examination. Please try again.',
            'delete' => 'There was an issue deleting the examination. Please try again.',
        ],

        'general' => [

            'id'           => 'Id',
            'user_id'      => 'User_Id',
            'created_at'   => 'Created At',
            'user_id_help' => 'Enter the User_Id here',

        ],

        'index'  => 'List Examinations',
        'create' => 'Create new Examination',
        'edit'   => 'Edit Examination',
        'delete' => 'Delete Examination',

    ],

    'Findings' => [

        'title' => 'Findings',

        'tabs'      => [

            'general'    => 'Finding',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Finding [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Finding was successfully created.',
            'update' => 'Finding was successfully updated.',
            'delete' => 'Finding was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the finding. Please try again.',
            'update' => 'There was an issue updating the finding. Please try again.',
            'delete' => 'There was an issue deleting the finding. Please try again.',
        ],

        'general' => [

            'id'                  => 'Id',
            'disease_id'          => 'Disease_Id',
            'part_id'             => 'Part_Id',
            'subpart_id'          => 'Subpart_Id',
            'examination_id'      => 'Examination_Id',
            'type'                => 'Type',
            'created_at'          => 'Created At',
            'disease_id_help'     => 'Enter the Disease_Id here',
            'part_id_help'        => 'Enter the Part_Id here',
            'subpart_id_help'     => 'Enter the Subpart_Id here',
            'examination_id_help' => 'Enter the Examination_Id here',
            'type_help'           => 'Enter type here',

        ],

        'index'  => 'List Findings',
        'create' => 'Create new Finding',
        'edit'   => 'Edit Finding',
        'delete' => 'Delete Finding',

    ],

    'Houses' => [

        'title' => 'Houses',

        'tabs'      => [

            'general'    => 'Houses',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Houses [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Houses was successfully created.',
            'update' => 'Houses was successfully updated.',
            'delete' => 'Houses was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the houses. Please try again.',
            'update' => 'There was an issue updating the houses. Please try again.',
            'delete' => 'There was an issue deleting the houses. Please try again.',
        ],

        'general' => [

            'id'                 => 'Id',
            'cattle_number'      => 'Cattle_Number',
            'created_at'         => 'Created At',
            'cattle_number_help' => 'Enter the Cattle_Number here',
            'company_name'       => 'Název',
        ],

        'index'  => 'List Houses',
        'create' => 'Create new Houses',
        'edit'   => 'Edit Houses',
        'delete' => 'Delete Houses',

    ],

    'Items' => [

        'title' => 'Items',

        'tabs'      => [

            'general'    => 'Items',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Items [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Items was successfully created.',
            'update' => 'Items was successfully updated.',
            'delete' => 'Items was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the items. Please try again.',
            'update' => 'There was an issue updating the items. Please try again.',
            'delete' => 'There was an issue deleting the items. Please try again.',
        ],


        'general' => [

            'id'               => 'Id',
            'item_number'      => 'Item_Number',
            'created_at'       => 'Created At',
            'item_number_help' => 'Enter the Item_Number here',

        ],

        'index'  => 'List Items',
        'create' => 'Create new Items',
        'edit'   => 'Edit Items',
        'delete' => 'Delete Items',

    ],

    'Parts' => [

        'title' => 'Parts',

        'tabs'      => [

            'general'    => 'Parts',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Parts [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Parts was successfully created.',
            'update' => 'Parts was successfully updated.',
            'delete' => 'Parts was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the parts. Please try again.',
            'update' => 'There was an issue updating the parts. Please try again.',
            'delete' => 'There was an issue deleting the parts. Please try again.',
        ],

        'general' => [

            'id'         => 'Id',
            'name'       => 'Name',
            'created_at' => 'Created At',
            'name_help'  => 'Enter the Name here',

        ],

        'index'  => 'List Parts',
        'create' => 'Create new Parts',
        'edit'   => 'Edit Parts',
        'delete' => 'Delete Parts',

    ],

    'Subparts' => [

        'title' => 'Subparts',

        'tabs'      => [

            'general'    => 'Subpart',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Subpart [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Subpart was successfully created.',
            'update' => 'Subpart was successfully updated.',
            'delete' => 'Subpart was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the subpart. Please try again.',
            'update' => 'There was an issue updating the subpart. Please try again.',
            'delete' => 'There was an issue deleting the subpart. Please try again.',
        ],

        'general' => [

            'id'         => 'Id',
            'label'      => 'Label',
            'created_at' => 'Created At',
            'label_help' => 'Enter the Label here',

        ],

        'index'  => 'List Subparts',
        'create' => 'Create new Subpart',
        'edit'   => 'Edit Subpart',
        'delete' => 'Delete Subpart',

    ],

    'Treatments' => [

        'title' => 'Treatments',

        'tabs'      => [

            'general'    => 'Treatment',
            'attributes' => 'Attributes',

        ],

        // General messages
        'not_found' => 'Treatment [:id] does not exist.',

        // Success messages
        'success'   => [
            'create' => 'Treatment was successfully created.',
            'update' => 'Treatment was successfully updated.',
            'delete' => 'Treatment was successfully deleted.',
        ],

        // Error messages
        'error'     => [
            'create' => 'There was an issue creating the treatment. Please try again.',
            'update' => 'There was an issue updating the treatment. Please try again.',
            'delete' => 'There was an issue deleting the treatment. Please try again.',
        ],

        'general' => [

            'id'         => 'Id',
            'created_at' => 'Created At',

        ],

        'index'  => 'List Treatments',
        'create' => 'Create new Treatment',
        'edit'   => 'Edit Treatment',
        'delete' => 'Delete Treatment',


    ],

    'Vet' => [

        'title' => 'My entries',

        'tabs' => [

            'general'    => 'My entries',
            'attributes' => 'Attributes',

        ],

        'general' => [
            'id'         => '#',
            'created_at' => 'Created at',
        ],

    ],


];