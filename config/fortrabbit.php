<?php

/**
 * Fortrabbit
 */

// Cache
$mc = [

    'COUNT' => '',
    'HOST1' => '',
    'PORT1' => '',
    'HOST2' => '',
    'PORT2' => '',

];

if ( isset($_SERVER['APP_SECRETS']) )
{
    $secrets = json_decode(file_get_contents($_SERVER['APP_SECRETS']), true);
    $mc = $secrets['MEMCACHE'];
} else {
    // Head.sanatorium.ninja
    $mc = [
        'COUNT' => 1,
        'HOST1' => 'localhost',
        'PORT1' => 11211
    ];
}

// Database
$creds = [
    'MYSQL' => [
        'HOST'     => '',
        'USER'     => '',
        'DATABASE' => '',
        'PASSWORD' => '',
    ],
];

// read credentials from secrets.json
if ( isset($_SERVER['APP_SECRETS']) )
{
    $creds = json_decode(file_get_contents($_SERVER['APP_SECRETS']), true);
}