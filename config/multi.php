<?php

$defaults = [
    'prefix' => '_',
    'host' => 'localhost',
    'filesystem_prefix' => 'localhost',
    'url' => 'http://localhost:8000',
];

$installations = json_decode(file_get_contents(__DIR__ . '/domains.json'), true);

if ( isset($_SERVER['HTTP_HOST']) ) {

    if ( is_array($installations) )
    {
        foreach ( $installations as $installation )
        {

            if ( $installation['host'] == $_SERVER['HTTP_HOST'] )
            {

                extract($installation);

            }

        }
    }

}

if ( !isset($prefix) || !isset($host) || !isset($filesystem_prefix) ) {

    extract($defaults);

}

if ( file_exists(__DIR__ . '/multi_simulate.php') )
{
    $prefix = include(__DIR__ . '/multi_simulate.php');
    putenv('DB_PREFIX='. $prefix);

    foreach ( $installations as $installation ) {

        if ( $installation['prefix'] == $prefix )
        {
            $filesystem_prefix = $installation['filesystem_prefix'];
            $host = $installation['host'];
            $locale = $installation['locale'];
        }

    }
}

