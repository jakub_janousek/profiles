<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Paths
    |--------------------------------------------------------------------------
    |
    | Following paths will be scanned by this extension to find
    | versioned extensions. These will be available in admin
    | area for automated operations.
    |
    */

    'paths' => [
        '/Users/Mac/Sites/Works/Platform/Development/extensions/sanatorium',
        '/Users/Mac/Sites/Works/Platform/langs',
        'C:\work\shopdevopment\extensions\sanatorium',
    ]
    
];