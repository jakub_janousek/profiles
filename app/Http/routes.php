<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
    'prefix' => 'debug',
], function ()
{

    // Test if /storage is sustained folder
    Route::group([
        'prefix' => 'storage'
    ], function(){

        Route::get('put', function(){
            //
            file_put_contents(storage_path('sample.txt'), 'Test ' . date('Y-m-d H:i:s'));

            return response('OK');
        });

        Route::get('get', function(){

            $content = file_get_contents(storage_path('sample.txt'));

            return $content;

        });

    });

    // Test which language files are loaded
    Route::get('langs', function() {

        $output = [];

        foreach ( [
                        'action',
                        'button',
                        'common',
                        'date',
                        'errors',
                        'extra',
                        'general',
                        'message',
                        'model',
                        'pagination',
                        'passwords',
                        'validation'
                  ] as $lang )
        {
            $output[$lang] = Lang::get($lang);
        }

        return $output;

    });

    Route::get('extensions', function ()
    {
        return Extensions::allInstalled();
    });

    // Mail testing
    Route::get('mail', function ()
    {

        if ( !config('mail.from.address') )
            throw new Exception('V administraci nemáš uvedenýho odesílatele debile');

        Mail::send('emails/test', ['salutation' => 'Hello', 'copyright' => 'Jsem kurak!'], function ($m)
        {

            $m->to(config('mail.from.address'));

        });

    });

    // Sanatorium mailer testing
    Route::get('mailer', function ()
    {

        return view('debug/mailer');

    });

    Route::post('mailer', function ()
    {

        $data = request()->all();

        $object['data'] = $data;

        Event::fire('pranostika', $object);

    });

    // Sanatorium mailer attachments test
    Route::get('mailer/attachment', function ()
    {

        return view('debug/mailer_attachment');

    });

    Route::post('mailer/attachment', function ()
    {

        $data = [
            request()->get('name'),
        ];

        $attachments = [];

        if ( request()->hasFile('photo') )
        {

            $medium = app('platform.media')->upload(request()->file('photo'), []);

            $attachments[] = [
                'type' => 'filesystem',
                'path' => $medium->path,
            ];

        }

        if ( request()->hasFile('invoice') )
        {

            $result = request()->file('invoice')->move(storage_path('attachments'), request()->file('invoice')->getClientOriginalName());

            $attachments[] = $result->getRealPath();

        }

        $object['data'] = $data;

        Event::fire('invoice', [$object, $attachments]);

    });

    Route::get('config', function ()
    {
        $debug = [
            'environment' => app()->environment(),
            'app'         => [
                'debug' => config('app.debug'),
            ],
            'cache'       => [
                config('cache.default'),
            ],
        ];

        return $debug;

    });

    Route::get('s3/{id}', function($id = null)
    {

        if ( app()->runningInConsole() )
            return null;

        if ( !$id )
            return null;

        $media = app('platform.media')->find($id);

        if ( !$media )
            return [];

        return [
            'media' => $media,
            'url' => StorageUrl::url($media->path)
        ];

    });

    Route::get('product/{id}', function($id){

        if ( app()->runningInConsole() )
            return null;

        if ( !class_exists('Product') )
            return null;

        $product = Product::find($id);

        $product->regenerateThumbnails();

        return [
            'coverThumbFull' => $product->coverThumb(),
            'coverThumb150' => $product->coverThumb(150),
            'coverThumb300' => $product->coverThumb(300),
            'coverThumb600' => $product->coverThumb(600),
            'gallery' => $product->getGalleryImages(),
            'product' => $product

        ];

    });

    Route::get('cache/clear', function(){

        $cache = app('cache');

        return $cache->flush();

    });

    Route::get('categories/tree', function(){

        if ( app()->runningInConsole() )
            return [];

        $repository = app('sanatorium.categories.category');
        
        return [
            'urls' => $repository->getAllUrls(),
            'names' => $repository->getAllUrlsAndNames(),
        ];

    });

    Route::get('env', function(){

        return [
            //
            'cache' => config('cache.prefix')

        ];

    });

});

/*
|--------------------------------------------------------------------------
| Multi installation
|--------------------------------------------------------------------------
|
| Manager for multiple instances of system.
|
*/

Route::group([
    'prefix' => admin_uri() . '/multi'
], function(){

    Route::get('/', function ()
    {

        $domainsJsonPath = __DIR__ . '/../../config/domains.json';

        $installations_raw = json_decode(file_get_contents($domainsJsonPath), true);

        $installations = [];

        // Merge installations by real instances (makes url array of possible urls)
        foreach( $installations_raw as $key => $value )
        {
            if ( !isset( $installations[$value['prefix']] ) )
            {
                $installations[$value['prefix']] = $value;
                $installations[$value['prefix']]['url'] = [ $value['url'] ];
            } else
            {
                $installations[$value['prefix']]['url'][] = $value['url'];
            }
        }

        return view('multi/index', compact('installations'));

    });

    Route::get('/simple', function ()
    {

        $domainsJsonPath = __DIR__ . '/../../config/domains.json';

        $installations = json_decode(file_get_contents($domainsJsonPath), true);

        return view('multi/index', compact('installations'));

    });

    Route::post('/', function ()
    {

        $data = request()->all();

        $domainsJsonPath = __DIR__ . '/../../config/domains.json';

        $installations = json_decode(file_get_contents($domainsJsonPath), true);

        $data['prefix'] = str_replace('-', '_', str_slug($data['prefix'])) . '_';

        $installations[] = [
            'name'              => $data['name'],
            'prefix'            => $data['prefix'],
            'host'              => $data['host'],
            'filesystem_prefix' => $data['filesystem_prefix'],
            'url'               => $data['url'],
            'locale'            => $data['locale']
        ];

        file_put_contents($domainsJsonPath, json_encode($installations));

        if ( !isset($data['prefix']) )
            return redirect()->back();

        if ( strlen($data['prefix']) > 8 )
            return redirect()->back();

        App\Console\Commands\Installed::createBasicTablesStatic($data['prefix']);

        return redirect()->back();

    });

    Route::any('delete/{prefix}', ['as' => 'platform.multi.delete', 'uses' => function($prefix){

        $domainsJsonPath = __DIR__ . '/../../config/domains.json';

        $installations = json_decode(file_get_contents($domainsJsonPath), true);

        foreach ( $installations as $key => $installation )
        {
            if ( $installation['prefix'] == $prefix )
            {
                unset($installations[$key]);
            }
        }

        file_put_contents($domainsJsonPath, json_encode($installations));

        return redirect()->back();

    }]);

});




/*
|--------------------------------------------------------------------------
| Style
|--------------------------------------------------------------------------
|
| Stylesheet/styleguide advisor page
|
*/

Route::get('style', function ()
{

    if ( app()->runningInConsole() )
        return null;

    return view('style/index');

});

/*
|--------------------------------------------------------------------------
| PHPinfo
|--------------------------------------------------------------------------
|
| Print phpinfo() debug
|
*/

Route::get('phpinfo', function ()
{

    if ( app()->runningInConsole() )
        return null;

    phpinfo();

});

/*
|--------------------------------------------------------------------------
| Seeds
|--------------------------------------------------------------------------
|
| Basic seeds
|
*/

class MenuSeeder extends Platform\Menus\Observer {

    public function __construct()
    {
        $this->menus = app('platform.menus');

        $this->roles = app('sentinel.roles');
    }

    public function createMenus($menus = [])
    {

        foreach ($menus as $slug => $children) {
            // Now, we'll prepare the children, which will fill in
            // the blanks
            $this->prepareChildrenNoExt($children);

            // Now, we'll loop through our new array and we'll compare that
            // to the children in the database. Any children in the database
            // not matching our array which are assigned to this extension
            // will be removed.
            $slugs = [];
            array_walk_recursive($children, function ($value, $key) use (&$slugs) {
                if ($key === 'slug') {
                    $slugs[] = $value;
                }
            });

            $slug = str_replace('_', '-', $slug);

            // Load up the associated menu
            if (! $menu = $this->menus->findBySlug($slug)) {
                $menu = last($this->menus->create([
                    'menu-name' => ucwords(str_replace('-', ' ', $slug)),
                    'menu-slug' => $slug,
                ]));
            }

            $menu->findChildren();

            $query = $this->menus
                ->createModel()
                ->whereMenu($menu->getKey())
                ->where('slug', 'like', "%{$slug}-%");

            if (count($slugs)) {
                $query->whereNotIn('slug', $slugs);
            }

            foreach ($query->get() as $child) {
                $child->refresh();

                $child->delete();
            }

            // Firstly, we'll purge the existing children (and any descendents)
            // from the children array and put them in the existing array.
            $tree     = $menu->toArray();
            $existing = $tree['children'];
            $this->recursivelyPurgeExisting($children, $existing);

            $tree = array_merge_recursive($existing, $children);
            $this->prepareChildrenAttributes($tree);

            // Because we have just taken our existing hierarchy
            // and added to it, we can save on the overhead of
            // orphaning or deleting children as there'll never
            // be any here. So, we'll just call this method as
            // a speed improvement.
            $menu->mapTreeAndKeep($tree);
        }

        $children = $this->menus->createModel()->whereExtension('')->get();

        foreach ($children as $child) {
            $child->enabled = false;

            $child->save();
        }
    }

    protected function prepareChildrenNoExt(&$children)
    {
        // Get the extension slug
        $slug = 'menu-seeded';

        // Prepare some sensible defaults
        $defaults = [
            'type'       => 'static',
            'visibility' => 'always',
            'children'   => [],
            'extension'  =>  $slug,
        ];

        foreach ($children as &$child) {
            $child = array_merge($defaults, $child);

            if (! isset($child['slug'])) {
                throw new \InvalidArgumentException("All menu children require a slug to be mapped from extension.php, Extension [{$slug}] has one or more slugs missing from it's menu children.");
            }

            if (! is_array($child['children'])) {
                throw new \InvalidArgumentException("Menu child [{$child['slug']}] for Extension [{$slug}] has a children property that is not an array.");
            }

            $this->prepareChildrenNoExt($child['children']);
        }
    }
}

Route::group([
    'prefix' => 'seeds',
], function ()
{

    // Basic pages for shop
    Route::get('shop', function ()
    {

        if ( app()->runningInConsole() )
            return null;

        // Forget pages
        Cache::forget('platform.pages.all');

        // Forget menu cache to not mess with nested sets
        Cache::forget('platform.menus.all');
        Cache::forget('platform.menus.all.root');

        foreach( ['highlights', 'how-to', 'about', 'bottom'] as $slug )
        {
            Cache::forget('platform.menu.slug.' . $slug);
            Cache::forget('platform.menu.root.slug.' . $slug);
        }

        $default = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis, elit eu consequat tempus, nisi augue finibus sapien, ac efficitur dui urna vel risus. Donec faucibus convallis nulla sit amet hendrerit. Praesent bibendum orci eget mauris consectetur tristique. Fusce auctor nulla ut maximus pretium. Sed vehicula iaculis rutrum. Sed a accumsan dolor, eu euismod arcu. Curabitur nisl eros, interdum ac lorem eget, finibus fermentum ligula. Phasellus consequat tortor id ex dapibus convallis quis ac lectus. Sed quis ante sed elit lacinia efficitur sodales at quam. Mauris ornare urna ut diam sagittis, eget porta metus aliquet. Vivamus non dui at nibh ultrices dignissim in sed justo. Fusce neque leo, blandit a nibh ac, tempor volutpat sapien. Suspendisse nec pharetra lacus, sed bibendum justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p>Suspendisse consequat sodales commodo. Aenean eget nisi tempus, finibus dolor placerat, accumsan eros. Fusce blandit, ante ac molestie varius, felis dolor volutpat massa, at imperdiet lacus eros ut turpis. Vivamus dui sem, fermentum eget libero non, cursus consequat est. Aenean eu nunc sit amet est accumsan tempor id eget neque. Nullam justo velit, aliquam at blandit in, mollis a dui. Maecenas tristique dictum nunc id laoreet. Phasellus semper nulla quis rutrum maximus. Etiam dictum ligula diam, ac blandit nunc iaculis eget. Suspendisse volutpat ante sapien, id ultricies elit pharetra id.</p>

<p>Praesent arcu diam, blandit id leo eget, euismod pharetra enim. Sed imperdiet pretium ultricies. Aenean ac gravida est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque aliquam quis odio ac aliquam. Donec eros tortor, semper sed porta et, finibus vitae purus. Mauris arcu lorem, tempor sed auctor quis, mattis sit amet tellus. In dapibus finibus suscipit. Cras vel urna mattis dui pharetra rhoncus vitae eu ante. Mauris sit amet molestie nisl. Nam commodo odio non nulla pulvinar dictum. Morbi vitae convallis odio. Fusce vel aliquet sem, in ultrices dui. Vivamus at laoreet tortor. Donec nisl massa, ultrices eu lectus quis, sollicitudin pharetra nulla.</p>

<p>Quisque et magna tincidunt, placerat purus pretium, malesuada turpis. Cras porttitor rutrum urna at ornare. Donec ultrices vel tellus at consectetur. Sed tortor augue, lobortis vel ante eu, tristique condimentum erat. Aliquam erat volutpat. Nam bibendum purus nibh, at egestas nunc egestas a. Vestibulum mollis massa non imperdiet vestibulum. Fusce rutrum sem ut sapien hendrerit rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

<p>Curabitur aliquet volutpat velit et hendrerit. Phasellus erat felis, dapibus eget feugiat vel, tincidunt at sapien. Ut tristique, velit ut rutrum porta, mauris nunc aliquet leo, dignissim tristique ex magna ac turpis. Suspendisse gravida orci in nibh tristique ultrices. Integer sed ligula a ante efficitur tempus id at est. In efficitur sapien aliquet, tincidunt odio ut, sodales ante. Donec eu aliquam nisi, id euismod dui. Quisque id dignissim elit. Integer quis vulputate turpis. Suspendisse potenti. Vivamus posuere leo a felis pulvinar, sed lobortis neque molestie. Curabitur eu massa tincidunt, sagittis orci iaculis, sollicitudin dui.</p>';

        // How to:

        app('Platform\Pages\Models\Page')->whereIn('slug', [
            'catalog',
            'shipping-and-delivery',
            'payment',
            'contact',

            'about',
            'terms',
            'career',

            'newsletter',

            'privacy',
            'cookies',
        ])->delete();

        // Sample catalog
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Catalog',
            'slug'             => 'catalog',
            'uri'              => '/catalog',
            'visibility'       => 'always',
            'meta_title'       => 'Sample catalog',
            'meta_description' => 'Sample catalog',
            'type'             => 'filesystem',
            'file'             => 'samples/catalog',
            'enabled'          => true,
        ]);

        // Shipping & Delivery
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Shipping & Delivery',
            'slug'             => 'shipping-and-delivery',
            'uri'              => '/shipping-and-delivery',
            'visibility'       => 'always',
            'meta_title'       => 'Shipping & Delivery',
            'meta_description' => 'Shipping & Delivery',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Payment Options
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Payment Options',
            'slug'             => 'payment',
            'uri'              => '/payment',
            'visibility'       => 'always',
            'meta_title'       => 'Payment Options',
            'meta_description' => 'Payment Options',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Contact us
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Contact us',
            'slug'             => 'contact',
            'uri'              => '/contact',
            'visibility'       => 'always',
            'meta_title'       => 'Contact us',
            'meta_description' => 'Contact us',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);


        // About:

        // About us
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'About us',
            'slug'             => 'about',
            'uri'              => '/about',
            'visibility'       => 'always',
            'meta_title'       => 'About us',
            'meta_description' => 'About us',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Terms and conditions
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Terms and conditions',
            'slug'             => 'terms',
            'uri'              => '/terms',
            'visibility'       => 'always',
            'meta_title'       => 'Terms and conditions',
            'meta_description' => 'Terms and conditions',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Careers
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Careers',
            'slug'             => 'career',
            'uri'              => '/career',
            'visibility'       => 'always',
            'meta_title'       => 'Careers',
            'meta_description' => 'Careers',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Highlights

        // Sign up for email
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Sign up for email',
            'slug'             => 'newsletter',
            'uri'              => '/newsletter',
            'visibility'       => 'always',
            'meta_title'       => 'Sign up for email',
            'meta_description' => 'Sign up for email',
            'type'             => 'filesystem',
            'file'             => 'newsletter',
            'enabled'          => true,
        ]);

        // Become a member - sign up
        // Sign in - sign in

        // Bottom

        // Privacy policy
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Privacy policy',
            'slug'             => 'privacy',
            'uri'              => '/privacy',
            'visibility'       => 'always',
            'meta_title'       => 'Privacy policy',
            'meta_description' => 'Privacy policy',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Cookies
        $page = app('Platform\Pages\Models\Page')->create([
            'name'             => 'Cookies',
            'slug'             => 'cookies',
            'uri'              => '/cookies',
            'visibility'       => 'always',
            'meta_title'       => 'Cookies',
            'meta_description' => 'Cookies',
            'type'             => 'database',
            'section'          => 'page',
            'template'         => 'layouts/default',
            'value'            => $default,
            'enabled'          => true,
        ]);

        // Create menus accordingly
        $menu_seeder = new MenuSeeder;
        $menu_seeder->createMenus([
            'highlights' => [
                [
                    'slug'     => 'highlights-catalog',
                    'name'     => 'Catalog',
                    'regex'    => '/catalog/i',
                    'uri'      => '/catalog',
                ],
                [
                    'slug'     => 'highlights-newsletter',
                    'name'     => 'Sign up for email',
                    'regex'    => '/newsletter/i',
                    'uri'      => '/newsletter',
                ],
                [
                    'slug'     => 'highlights-register',
                    'name'     => 'Become a member',
                    'regex'    => '/register/i',
                    'uri'      => '/register',
                ],
                [
                    'slug'     => 'highlights-login',
                    'name'     => 'Sign in',
                    'regex'    => '/login/i',
                    'uri'      => '/login',
                ]
            ],
            'how-to' => [
                [
                    'slug'     => 'how-to-shipping',
                    'name'     => 'Shipping & Delivery',
                    'regex'    => '/shipping-and-delivery/i',
                    'uri'      => '/shipping-and-delivery',
                ],
                [
                    'slug'     => 'how-to-payment',
                    'name'     => 'Payment Options',
                    'regex'    => '/payment/i',
                    'uri'      => '/payment',
                ],
                [
                    'slug'     => 'how-to-contact',
                    'name'     => 'Contact us',
                    'regex'    => '/contact/i',
                    'uri'      => '/contact',
                ],
            ],
            'about' => [
                [
                    'slug'     => 'about-terms',
                    'name'     => 'Terms and conditions',
                    'regex'    => '/terms/i',
                    'uri'      => '/terms',
                ],
                [
                    'slug'     => 'about-about',
                    'name'     => 'About us',
                    'regex'    => '/about/i',
                    'uri'      => '/about',
                ],
                [
                    'slug'     => 'about-career',
                    'name'     => 'Careers',
                    'regex'    => '/career/i',
                    'uri'      => '/career',
                ],
            ],
            'bottom' => [
                [
                    'slug'     => 'bottom-privacy',
                    'name'     => 'Privacy policy',
                    'regex'    => '/privacy/i',
                    'uri'      => '/privacy',
                ],
                [
                    'slug'     => 'bottom-terms',
                    'name'     => 'Terms and conditions',
                    'regex'    => '/terms/i',
                    'uri'      => '/terms',
                ],
                [
                    'slug'     => 'bottom-cookies',
                    'name'     => 'Cookies',
                    'regex'    => '/cookies/i',
                    'uri'      => '/cookies',
                ],
            ],
        ]);

        return [];

    });

});


/*
|--------------------------------------------------------------------------
| Client specific
|--------------------------------------------------------------------------
|
| Client specific chunk of codes
|
*/

include base_path('clients/autoload.php');