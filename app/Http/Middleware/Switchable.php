<?php namespace App\Http\Middleware;



/**
 * Switchable
 *
 * Enables you to switch installations based on
 * config/domains.json file.
 *
 * Following criteria must be met to switch
 *  - environment variable SWITCHABLE=1
 *  - app is not running in console
 *  - request is not ajax
 *
 * Requires
 *  barryvdh/laravel-debugbar
 *
 * @author  sanatorium <info@sanatorium.ninja>
 * @package Platform
 * @version 1.0.0
 */



use Closure;

class Switchable
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( $this->isAvailable($request)
        )
        {
            if ( $request->has('active_site') )
            {
                $prefix = $request->get('active_site');

                $path = base_path('config/multi_simulate.php');

                file_put_contents($path, "<?php return '{$prefix}';");

                // Semms like file put contents is too slow,
                // therefore we need to wait some before we redirect
                // for the new setup to notice, there is multi_simulate.php
                sleep(3);

                return redirect()->back();
            }

            $instances = $this->getInstances();

            $response = $next($request);

            $content = $response->getContent();

            $content = str_replace('</body>', $this->getSelectHtml($instances, env('DB_PREFIX')).'</body>', $content);

            $response->setContent($content);

            return $response;
        }

        return $next($request);
    }

    /**
     * Checks if your current environment matchs the criteria to show the site-switch option
     *  - app is not running in console
     *  - request is not ajax
     *  - environment variable SWITCHABLE=1
     *
     * @param $request
     * @return bool
     */
    public function isAvailable($request)
    {
        return !app()->runningInConsole()
        && !$request->ajax()
        && env('SWITCHABLE', 0);
    }

    public function getInstances()
    {
        $domainsJsonPath = base_path('config/domains.json');

        $installations = json_decode(file_get_contents( $domainsJsonPath ), true);

        $output = [];

        foreach( $installations as $installation )
        {
            $output[$installation['prefix']] = $installation;
        }

        return $output;
    }

    public function getSelectHtml($instances = [], $active = '', $html = '')
    {
        // Debugbar widget
        $html .= '<script type="text/javascript">
                    phpdebugbar.createIndicator("switchable", "exchange", "Switch site");
                    $(".phpdebugbar-fa-exchange").click(function(event){
                      $("#switchable-modal").modal();
                    });
                    </script>';

        $html .=   '<div class="modal fade" id="switchable-modal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            Site switch
                          </div>';
                            $html .= '<ul class="nav nav-pills nav-stacked">';
                            foreach( $instances as $instance )
                            {
                                $html .= '<li class="'.($active == $instance['prefix'] ? 'active' : '').'"><a href="/?active_site='.$instance['prefix'].'">'.$instance['name'].' &nbsp; <small class="text-muted">'.$instance['host'].'</small></a></li>';
                            }
                            $html .= '</ul>';
                            $html .= '
                          </div>
                        </div>
                      </div>
                    </div>';

        return $html;
    }
}


