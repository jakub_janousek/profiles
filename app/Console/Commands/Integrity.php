<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class Integrity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'integrity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Platform type app integrity';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $extensions = app('extensions');

        // Find, register and sort extensions
        $extensions->findAndRegisterExtensions();
        $extensions->sortExtensions();

        // Loop through extensions, install, enable and boot each.
        foreach ($extensions->all() as $extension)
        {
            // Hydrates the database attributes for the extension
            $extension->setupDatabase();

            if ( $extension->isInstalled())
            {
                if ( $extension->isEnabled())
                {

                    $path = $extension->getPath();

                    if ( file_exists($path . '/extension.php') )
                    {

                        $configuration = include($path . '/extension.php');

                        if ( isset($configuration['integrity']) ) {

                            foreach( $configuration['integrity'] as $check )
                            {

                                if ( call_user_func($check['test']) )
                                {
                                    $this->info($extension->getSlug() . ': ' . $check['name'] . ' SUCCEED');
                                } else {
                                    $this->error($extension->getSlug() . ': ' . $check['name'] . ' FAILED');
                                }

                            }

                        }

                    }

                }

            }
        }

        // line, info, comment, question and error
    }
}

