# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: eshop.mysql.eu2.frbit.com (MySQL 5.6.23-log)
# Database: eshop
# Generation Time: 2016-04-12 14:01:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}activations`;

CREATE TABLE `{DBPREFIX}activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}activations` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}activations` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`)
VALUES
	(1,1,'T4T0ZOjDd9vJVAufPcfCrKknrYdxcKjz',1,'2016-03-12 19:06:17','2016-03-12 19:06:17','2016-03-12 19:06:17'),
  (2,2,'HKlAarIcjlOXSBMCdOmWhIDY9mYVhMjm',1,'2016-06-06 15:16:01','2016-06-06 15:16:01','2016-06-06 15:16:01');

/*!40000 ALTER TABLE `{DBPREFIX}activations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attribute_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}attribute_values`;

CREATE TABLE `{DBPREFIX}attribute_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `attribute_values_attribute_id_index` (`attribute_id`),
  KEY `attribute_values_entity_type_index` (`entity_type`),
  KEY `attribute_values_entity_id_index` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}attribute_values` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}attribute_values` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}attribute_values` (`id`, `attribute_id`, `entity_type`, `entity_id`, `value`, `created_at`, `updated_at`)
VALUES
	(1,1,'PlatformPagesModelsPage',1,'About','2016-03-12 19:06:16','2016-03-12 19:06:16'),
	(2,2,'PlatformPagesModelsPage',1,'About Platform','2016-03-12 19:06:16','2016-03-12 19:06:16'),
	(3,1,'PlatformPagesModelsPage',2,'Welcome','2016-03-12 19:06:17','2016-03-12 19:06:17'),
	(4,2,'PlatformPagesModelsPage',2,'The default home page.','2016-03-12 19:06:17','2016-03-12 19:06:17');

/*!40000 ALTER TABLE `{DBPREFIX}attribute_values` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}attributes`;

CREATE TABLE `{DBPREFIX}attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `options` text COLLATE utf8_unicode_ci,
  `validation` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_slug_unique` (`slug`),
  KEY `attributes_slug_index` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}attributes` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}attributes` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}attributes` (`id`, `namespace`, `slug`, `name`, `description`, `type`, `options`, `validation`, `enabled`, `created_at`, `updated_at`)
VALUES
	(1,'platform/pages','meta_title','Meta Title','Page meta title.','input','',NULL,1,'2016-03-12 19:06:16','2016-03-12 19:06:16'),
	(2,'platform/pages','meta_description','Meta Description','Page meta description.','input','',NULL,1,'2016-03-12 19:06:16','2016-03-12 19:06:16');

/*!40000 ALTER TABLE `{DBPREFIX}attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}config`;

CREATE TABLE `{DBPREFIX}config` (
  `item` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  UNIQUE KEY `config_item_unique` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}content`;

CREATE TABLE `{DBPREFIX}content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}content` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}content` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}content` (`id`, `name`, `slug`, `type`, `value`, `file`, `enabled`, `created_at`, `updated_at`)
VALUES
	(1,'Featurette Packages','featurette-packages','filesystem',NULL,'featurettes/packages.md',1,'2016-03-12 19:06:16','2016-03-12 19:06:16'),
	(2,'Featurette Extensions','featurette-extensions','filesystem',NULL,'featurettes/extensions.md',1,'2016-03-12 19:06:16','2016-03-12 19:06:16'),
	(3,'Featurette Themes','featurette-themes','filesystem',NULL,'featurettes/themes.md',1,'2016-03-12 19:06:16','2016-03-12 19:06:16');

/*!40000 ALTER TABLE `{DBPREFIX}content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table extensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}extensions`;

CREATE TABLE `{DBPREFIX}extensions` (
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}extensions` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}extensions` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}extensions` (`slug`, `version`, `enabled`)
VALUES
	('platform/access','3.2.0',1),
	('platform/attributes','3.2.0',1),
	('platform/content','3.2.0',1),
	('platform/dashboard','3.2.0',1),
	('platform/media','3.2.0',1),
	('platform/menus','3.2.0',1),
	('platform/operations','3.2.0',1),
	('platform/pages','3.2.0',1),
	('platform/permissions','3.1.0',1),
	('platform/roles','3.2.0',1),
	('platform/settings','3.2.0',1),
	('platform/tags','3.2.0',1),
	('platform/themes','3.0.0',1),
	('platform/users','3.2.0',1),
	('sanatorium/hooks','0.1.0',1),
	('sanatorium/inputs','0.1.0',1);

/*!40000 ALTER TABLE `{DBPREFIX}extensions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}media`;

CREATE TABLE `{DBPREFIX}media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `is_image` tinyint(1) NOT NULL DEFAULT '0',
  `thumbnail` text COLLATE utf8_unicode_ci,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `roles` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table media_assign
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}media_assign`;

CREATE TABLE `{DBPREFIX}media_assign` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `entity_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media_id` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}menus`;

CREATE TABLE `{DBPREFIX}menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'static',
  `page_id` int(11) DEFAULT NULL,
  `secure` tinyint(1) DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'self',
  `visibility` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'always',
  `roles` text COLLATE utf8_unicode_ci,
  `regex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_slug_unique` (`slug`),
  UNIQUE KEY `menus_lft_rgt_menu_unique` (`lft`,`rgt`,`menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}menus` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}menus` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}menus` (`id`, `slug`, `lft`, `rgt`, `menu`, `extension`, `name`, `type`, `page_id`, `secure`, `uri`, `class`, `target`, `visibility`, `roles`, `regex`, `enabled`, `created_at`, `updated_at`)
VALUES
	(1,'admin',1,26,1,NULL,'Admin','static',NULL,NULL,NULL,NULL,'self','always',NULL,NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(2,'admin-access',12,17,1,'platform/access','Access','static',NULL,NULL,'users','fa fa-user','self','always','','/:admin\/(users|roles)/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(3,'admin-access-users',13,14,1,'platform/access','Users','static',NULL,NULL,'users','fa fa-user','self','always','','/:admin\/users/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(4,'admin-access-roles',15,16,1,'platform/access','Roles','static',NULL,NULL,'roles','fa fa-group','self','always','','/:admin\/roles/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(5,'admin-attributes',8,9,1,'platform/attributes','Attributes','static',NULL,NULL,'attributes','fa fa-asterisk','self','always','','/:admin\/attributes/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(6,'admin-media',6,7,1,'platform/media','Media','static',NULL,NULL,'media','fa fa-picture-o','self','always','','/:admin\/media/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(7,'admin-menus',10,11,1,'platform/menus','Menus','static',NULL,NULL,'menus','fa fa-th-list','self','always','','/:admin\/menus/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(8,'system',1,6,2,NULL,'System','static',NULL,NULL,NULL,NULL,'self','always',NULL,NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(9,'system-preview',2,3,2,'platform/menus','Preview','static',NULL,NULL,'/','fa fa-laptop','blank','always','',NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(10,'system-settings',4,5,2,'platform/menus','Settings','static',NULL,NULL,':admin/settings','fa fa-sliders','self','always','','/:admin\/settings/i',1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(11,'main',1,12,3,NULL,'Main','static',NULL,NULL,NULL,NULL,'self','always',NULL,NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(12,'main-about',2,3,3,'platform/menus','About','static',NULL,NULL,'about','fa fa-info','self','always','',NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(13,'main-help',4,11,3,'platform/menus','Help','static',NULL,NULL,NULL,'fa fa-life-ring','self','always','',NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(14,'main-help-support',5,6,3,'platform/menus','Support','static',NULL,NULL,'https://cartalyst.com/support','fa fa-bug','blank','always','',NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(15,'main-help-docs',7,8,3,'platform/menus','Documentation','static',NULL,NULL,'https://cartalyst.com/manual/platform','fa fa-graduation-cap','blank','always','',NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(16,'main-help-license',9,10,3,'platform/menus','License','static',NULL,NULL,'https://cartalyst.com/license','fa fa-book','blank','always','',NULL,1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(17,'account',1,14,4,NULL,'Account','static',NULL,NULL,NULL,NULL,'self','always',NULL,NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(18,'account-menu',2,13,4,'platform/menus','Account','static',NULL,NULL,NULL,'fa fa-user','self','always','','/profile/i',1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(19,'account-admin',3,4,4,'platform/menus','Administrator','static',NULL,NULL,':admin/','fa fa-gears','self','admin','',NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(20,'account-profile',5,6,4,'platform/menus','Profile','static',NULL,NULL,'profile','fa fa-gear','self','logged_in','',NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(21,'account-login',7,8,4,'platform/menus','Sign In','static',NULL,NULL,'login','fa fa-sign-in','self','logged_out','',NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(22,'account-logout',9,10,4,'platform/menus','Logout','static',NULL,NULL,'logout','fa fa-sign-out','self','logged_in','',NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(23,'account-register',11,12,4,'platform/menus','Register','static',NULL,NULL,'register','fa fa-edit','self','logged_out','',NULL,1,'2016-03-12 19:06:17','2016-03-12 19:06:19'),
	(24,'admin-tags',24,25,1,'platform/tags','Tags','static',NULL,NULL,'tags','fa fa-tag','self','always','','/:admin\/(tags)/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(25,'admin-content',4,5,1,'platform/content','Content','static',NULL,NULL,'content','fa fa-briefcase','self','always','','/:admin\/content/i',1,'2016-03-12 19:06:17','2016-03-12 21:11:46'),
	(26,'admin-operations',18,23,1,'platform/operations','Operations','static',NULL,NULL,'operations','fa fa-rocket','self','always','','/:admin\/operations/i',1,'2016-03-12 19:06:18','2016-03-12 21:11:46'),
	(27,'admin-operations-extensions',19,20,1,'platform/operations','Extensions','static',NULL,NULL,'operations/extensions','fa fa-fighter-jet','self','always','','/:admin\/operations\/extensions/i',1,'2016-03-12 19:06:18','2016-03-12 21:11:46'),
	(28,'admin-operations-workshop',21,22,1,'platform/operations','Workshop','static',NULL,NULL,'operations/workshop','fa fa-magic','self','always','','/:admin\/operations\/workshop/i',1,'2016-03-12 19:06:18','2016-03-12 21:11:46'),
	(29,'admin-pages',2,3,1,'platform/pages','Pages','static',NULL,NULL,'pages','fa fa-file','self','always','','/:admin\/pages/i',1,'2016-03-12 19:06:18','2016-03-12 21:11:46');

/*!40000 ALTER TABLE `{DBPREFIX}menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}migrations`;

CREATE TABLE `{DBPREFIX}migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}migrations` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}migrations` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}migrations` (`migration`, `batch`)
VALUES
	('2012_11_12_055224_migration_cartalyst_extensions_install',1),
	('2014_07_02_230147_migration_cartalyst_sentinel',2),
	('2013_09_11_010213_migration_platform_attributes_install_tables',3),
	('2013_01_28_024954_migration_platform_media_install_media',4),
	('2012_11_19_071147_migration_platform_menus_install_menus',5),
	('2013_01_08_225921_migration_cartalyst_composite_config_install',6),
	('2012_11_17_050051_migration_platform_settings_install',6),
	('2014_10_29_202547_migration_cartalyst_tags_create_tables',7),
	('2014_11_30_230342_migration_platform_tags_install_tags',7),
	('2014_10_08_114228_migration_cartalyst_sentinel_social',8),
	('2012_12_19_034250_migration_platform_users_install_sentinel_social',8),
	('2014_03_07_025421_migration_platform_content_create_table',9),
	('2014_03_13_040514_migration_platform_pages_create_table',10),
	('2014_03_13_040635_migration_platform_pages_update_menus_table',10),
	('2015_09_22_201025_create_media_assign_table',11);

/*!40000 ALTER TABLE `{DBPREFIX}migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}pages`;

CREATE TABLE `{DBPREFIX}pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visibility` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` text COLLATE utf8_unicode_ci,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `https` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`),
  KEY `pages_uri_index` (`uri`),
  KEY `pages_type_index` (`type`),
  KEY `pages_visibility_index` (`visibility`),
  KEY `pages_enabled_index` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}pages` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}pages` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}pages` (`id`, `name`, `slug`, `uri`, `type`, `visibility`, `roles`, `template`, `section`, `value`, `file`, `enabled`, `https`, `created_at`, `updated_at`)
VALUES
	(1,'About','about','/about','filesystem','always',NULL,NULL,NULL,NULL,'about',1,0,'2016-03-12 19:06:16','2016-03-12 19:06:16'),
	(2,'Welcome','welcome','/','filesystem','always',NULL,NULL,NULL,NULL,'welcome',1,0,'2016-03-12 19:06:17','2016-03-12 19:06:17');

/*!40000 ALTER TABLE `{DBPREFIX}pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table persistences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}persistences`;

CREATE TABLE `{DBPREFIX}persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}persistences` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}persistences` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`)
VALUES
	(1,1,'WHf8EXotuS39nPXmEgRAyCIdYVv93pTP','2016-03-12 19:06:47','2016-03-12 19:06:47'),
	(4,1,'jkSVuiXz2frT7rv5xc0AcTQE3oEFRLhH','2016-03-12 19:34:21','2016-03-12 19:34:21');

/*!40000 ALTER TABLE `{DBPREFIX}persistences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reminders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}reminders`;

CREATE TABLE `{DBPREFIX}reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table role_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}role_users`;

CREATE TABLE `{DBPREFIX}role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}role_users` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}role_users` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}role_users` (`user_id`, `role_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'2016-03-12 19:06:17','2016-03-12 19:06:17'),
  (2,1,'2016-03-12 19:06:17','2016-03-12 19:06:17');

/*!40000 ALTER TABLE `{DBPREFIX}role_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}roles`;

CREATE TABLE `{DBPREFIX}roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}roles` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}roles` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Admin','{\"superuser\":true}','2016-03-12 19:06:17','2016-03-12 19:06:17'),
	(2,'registered','Registered',NULL,'2016-03-12 19:06:20','2016-03-12 19:06:20');

/*!40000 ALTER TABLE `{DBPREFIX}roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}sessions`;

CREATE TABLE `{DBPREFIX}sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table social
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}social`;

CREATE TABLE `{DBPREFIX}social` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `uid` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oauth1_token_identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth1_token_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth2_access_token` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth2_refresh_token` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth2_expires` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_provider_user_id_unique` (`provider`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tagged
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}tagged`;

CREATE TABLE `{DBPREFIX}tagged` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `taggable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagged_taggable_type_taggable_id_index` (`taggable_type`,`taggable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}tags`;

CREATE TABLE `{DBPREFIX}tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table throttle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}throttle`;

CREATE TABLE `{DBPREFIX}throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `{DBPREFIX}users`;

CREATE TABLE `{DBPREFIX}users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `{DBPREFIX}users` WRITE;
/*!40000 ALTER TABLE `{DBPREFIX}users` DISABLE KEYS */;

INSERT INTO `{DBPREFIX}users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `created_at`, `updated_at`)
VALUES
	(1,'jan.rozklad@gmail.com','$2y$10$4Lenqau5jrJPoYprDu6TKOWoKIqDps.XUONqhuCvTNMmI3iTW/t6m',NULL,'2016-03-12 21:10:18',NULL,NULL,'2016-03-12 19:06:17','2016-03-12 21:10:18'),
  (2, 'matejlukas12@gmail.com', '$2y$10$rresSWBs95L9Rax4ppspT.8N6YHbqwmL09I7h65wkMv7jxP7ZHM2S', '', NULL, 'Matěj', 'Lukáš', '2016-05-08 17:54:11', '2016-05-08 17:54:11');

/*!40000 ALTER TABLE `{DBPREFIX}users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
