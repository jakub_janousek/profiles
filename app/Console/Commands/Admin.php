<?php

/**
 * Admin
 *
 * Create new user with admin rights
 *  admin@example.tld:pass123
 *
 * NOTICE OF LICENSE
 *
 * @package    App\Console\Commands
 * @version    1.0.0
 * @author     Sanatorium
 * @license    WTFPL
 * @copyright  (c) 2015-2016, Sanatorium
 * @link       http://sanatorium.ninja
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Sentinel;
use Event;

class Admin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user with admin rights admin@example.tld:pass123';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Register the user
        $user = Sentinel::registerAndActivate([
            'email'     => 'admin@example.tld',
            'password'  => 'pass123',
        ]);

        $role = Sentinel::findRoleBySlug('admin');
        $user->roles()->attach($role);

        $this->comment(PHP_EOL.'Admin user admin@example.tld:pass123 was created'.PHP_EOL);

    }

}