<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use DB;

class Installed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'installed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mimic installed platform';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createBasicTables();

        $this->rewriteConfig();
    }

    private function createBasicTables()
    {


        $installations = json_decode( file_get_contents(__DIR__ . '/../../../config/domains.json' ), true);

        foreach( $installations as $installation ) {
            extract($installation);
            $this->createBasicTablesPrefixed($prefix);
        }


    }

    private function createBasicTablesPrefixed($prefix)
    {

        self::createBasicTablesStatic($prefix);

        $this->comment(PHP_EOL.'DB created and filled for ' . $prefix .PHP_EOL);
            
    }

    public static function createBasicTablesStatic($prefix)
    {

        // Make raw db query, cant use Schema::hasTable as that adds
        // prefix by default console prefix is default value
        $table = DB::select("SHOW TABLES LIKE '{$prefix}config'");

        // Already exists
        if ( count($table) == 1 )
            return true;

        // Database does not have the basic tables
        $query = str_replace('{DBPREFIX}', $prefix, file_get_contents( __DIR__ . '/installed.sql' ) );
        DB::unprepared($query);


    }

    private function rewriteConfig()
    {
        $contents = "<?php

/**
 * Part of the Platform Foundation extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Foundation extension
 * @version    3.0.4
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

    /*
    |                                                 ,,~~--___---,
    |                                                /            .~,
    |                                          /  _,~             )
    |                                         (_-(~)   ~, ),,,(  /'
    |                                 A Goat   Z6  .~`' ||     \ |
    |                                          /_,/     ||      ||
    |---------------------------------------------------W`------W`-------------
    | Installed Version
    |--------------------------------------------------------------------------
    |
    | This variable holds the current installed version of Platform.
    |
    | You are highly discouraged from touching this, ever.
    |
    | How It Works:
    |
    |  - On a blank installation, the installed version will be FALSE, which
    |    means Platform isn't installed.
    |
    |  - We'll check this version against the PLATFORM_VERSION constant and
    |    if this version is less, it means you have upgraded the Platform
    |    codebase, we'll then lock out the application and send you to
    |    the installer where you'll be taken through the upgrade process.
    |
    */

    'installed_version' => '4.0.0',

    /*
    |--------------------------------------------------------------------------
    | Release Name
    |--------------------------------------------------------------------------
    |
    | Platform follows semantic versioning (Major.Minor.Patch).
    |
    | Major releases are given a code name.
    |
    */

    'release_name' => 'Ornery Octopus',

];
";
        file_put_contents( __DIR__ . '/../../../config/platform-foundation.php', $contents);

        $this->comment(PHP_EOL.'Platform now looks as installed'.PHP_EOL);
    }

}
