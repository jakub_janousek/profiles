<?php

/**
 * Cheat
 *
 * Custom cheat sheet command
 *
 * NOTICE OF LICENSE
 *
 * @package    App\Console\Commands
 * @version    1.0.0
 * @author     Sanatorium
 * @license    WTFPL
 * @copyright  (c) 2015-2016, Sanatorium
 * @link       http://sanatorium.ninja
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Cheat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cheat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cheat sheet';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('mysql.server start Starts MySQL' . PHP_EOL);

        $this->info('mysql.server stop Stops MySQL' . PHP_EOL);
    }

}