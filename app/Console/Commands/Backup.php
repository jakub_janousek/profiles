<?php

/**
 * Backup
 *
 * Backup command to not ruin the extensions updates when runing composer update
 *
 * NOTICE OF LICENSE
 *
 * @package    App\Console\Commands
 * @version    1.0.0
 * @author     Sanatorium
 * @license    WTFPL
 * @copyright  (c) 2015-2016, Sanatorium
 * @link       http://sanatorium.ninja
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cheat sheet';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = base_path( '/extensions' );

        $date_time = date('Y-m-d-H-i');

        $backup = base_path( '/backup/' . $date_time );

        if ( !file_exists($path) )
        {
            $this->info('Extension directory does not exist, nothing to backup');
        }

        if ( !file_exists($backup) )
        {
            mkdir($backup, 0777, true);
            chmod($backup, 0777);
        }

        $success = File::copyDirectory($path, $backup);

        if ( $success )
        {

            $this->info('Extension directory was succesfully backed up as backup/' . $date_time);

        } else {

            $this->info('There were problems backing up the extensions directory');

        }

    }

}




