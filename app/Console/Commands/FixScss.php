<?php

/**
 * Fix scss
 *
 * Added by composer post-install, post-update to optimize result for Twitter Bootstrap 3
 * @see: http://stackoverflow.com/questions/23971428/twitter-bootstrap-3-button-1px-too-small
 *
 * NOTICE OF LICENSE
 *
 * @package    App\Console\Commands
 * @version    1.0.0
 * @author     Sanatorium
 * @license    WTFPL
 * @copyright  (c) 2015-2016, Sanatorium
 * @link       http://sanatorium.ninja
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FixScss extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:scss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cheat sheet';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = base_path( '/vendor/kriswallsmith/assetic/src/Assetic/Filter/ScssphpFilter.php' );

        if ( file_exists( $path )  )
        {

            $original = file_get_contents($path);

            $fixed = str_replace(
                [
'       $asset->setContent($sc->compile($asset->getContent()));',
                ],
'       // Added by composer post-install, post-update to optimize result for Twitter Bootstrap 3
        // @see: http://stackoverflow.com/questions/23971428/twitter-bootstrap-3-button-1px-too-small
        $sc->setNumberPrecision(10);

        $asset->setContent($sc->compile($asset->getContent())/*updated by php artisan fix:scss*/);',
                $original
            );

            file_put_contents($path, $fixed);

            $this->info('SCSS compiling precision succesfully set to 10' . PHP_EOL);

        }
        else
        {

            $this->info('Assetis/Filter is not installed, nothing to fix' . PHP_EOL);

        }


    }

}




