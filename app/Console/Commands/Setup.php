<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class Setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will print out server secrets.json configuration';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // read credentials from secrets.json
        if ( isset($_SERVER['APP_SECRETS']) ) {
            $creds = json_decode(file_get_contents($_SERVER['APP_SECRETS']), true);

            var_dump($creds);
        }

        $lang_path = base_path('resources/lang/cs');

        if ( file_exists( $lang_path ) ) {

            echo $lang_path . ' exists';

            var_dump( scandir ( $lang_path ) );

        } else {

            echo $lang_path . ' does not exist';

        }
    }
}
