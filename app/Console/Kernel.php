<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Admin::class,
        \App\Console\Commands\Cheat::class,
        \App\Console\Commands\Dev::class,
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\Installed::class,
        \App\Console\Commands\Integrity::class,
        \App\Console\Commands\Setup::class,
        \App\Console\Commands\FixScss::class,
        \App\Console\Commands\Backup::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
