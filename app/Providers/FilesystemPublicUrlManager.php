<?php

namespace App\Providers;

/**
 * Class FilesystemPublicUrlManager
 *
 * @todo remove when updated to Laravel 5.2
 *
 *       This class is workaround for Storage::url($path)
 *
 * @example $s3Url = StorageUrl::publicUrl('s3') //using the s3 driver
 *          $localUrl = StorageUrl::publicUrl('local') //using the local driver
 *          $defaultUrl = StorageUrl::publicUrl() //default driver
 *          $objectUrl = StorageUrl::publicUrl('s3', '/path/to/object');
 */

use Illuminate\Filesystem\FilesystemManager;
use URL;
use Illuminate\Support\Arr;
use Aws\S3\S3Client;

class FilesystemPublicUrlManager extends FilesystemManager
{

    public function url($object_path = '')
    {
        $name = $this->getDefaultDriver();
        $config = $this->getConfig($name);

        return $this->{'get' . ucfirst($config['driver']) . 'PublicUrl'}($config,$object_path);
    }

    public function publicUrl($name = null, $object_path = '')
    {
        $name = $name ?: $this->getDefaultDriver();
        $config = $this->getConfig($name);

        return $this->{'get' . ucfirst($config['driver']) . 'PublicUrl'}($config,$object_path);
    }

    public function getLocalPublicUrl($config, $object_path = '')
    {
        return URL::to('/public') . $object_path;
    }

    public function getS3PublicUrl($config, $object_path = '')
    {
        $config += ['version' => 'latest'];

        if ( $config['key'] && $config['secret'] )
        {
            $config['credentials'] = Arr::only($config, ['key', 'secret']);
        }

        return (new S3Client($config))->getObjectUrl($config['bucket'], $config['prefix'] . '/' . $object_path);
    }
}