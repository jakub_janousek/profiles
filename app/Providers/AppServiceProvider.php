<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local'))
        {
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        }

        // @todo: Remove this on Laravel 5.2 update
        $this->app->singleton('filesystemPublicUrl', function () {
            return new FilesystemPublicUrlManager($this->app);
        });
    }
}
