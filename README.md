# Platform

## Installation from complete archive (recommended for client)

1. Unpack the archive
2. Change the .env file according to your environment setup
3. Set the /public folder as 
4. Use dump.sql to popuplate the database
5. Use login: agnostic@sld.gs - password: agnostic to login to the system

## Download

    git clone https://bitbucket.org/sleighdogs/profiles.git

## Environment setup

Setup .env file to fit your needs, you will need a mysql server runing and database on it. Sample setup might look like this:

    APP_ENV=local
    APP_DEBUG=true
    APP_KEY=fnRdfO7ntU2W7XVT8SBfJzaHUerphxnv
    
    DB_HOST=localhost
    DB_DATABASE=platform
    DB_USERNAME=root
    DB_PASSWORD=
    DB_CHARSET=utf8
    DB_COLLATION=utf8_unicode_ci
    DB_PREFIX=_
    
    CACHE_DRIVER=file
    SESSION_DRIVER=database
    
    PREFER_GIT=1
    PREFER_GIT_REGEX=/sanatorium\/((?!lang).)*$/
    
    AWS3_KEY=AKIAJIJNJXKGXBXFV4FQ
    AWS3_SECRET=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
    AWS3_BUCKET=fortrabbit
    AWS3_REGION=us-east-1

## Install dependencies

    composer install

## Installation

There are two ways to setup the project:

1. Artisan
2. Apache

Choose one that fits you the best.

## 1. Artisan

    php artisan serve

## 2. Apache

Let's run the installation on "platform.dev" address:

### Setup Apache (XAMP, LAMP, WAMP)

Add following to your httpd-vhosts.conf:

    <VirtualHost *:80>
        ServerName platform.dev
        ServerAlias www.platform.dev
    
        DocumentRoot "/path/to/repo/public"
        <Directory "/path/to/repo/public">
            Options Indexes FollowSymLinks
            AllowOverride All
            Order allow,deny
            Allow from all
            Require all granted
        </Directory>
    
        ErrorLog /var/log/apache2/project_error.log
        CustomLog /var/log/apache2/project_access.log combined
    </VirtualHost>

**Location**
- /private/etc/apache2/extra/httpd-vhots.conf

**Replace**
- Replace /path/to/repo/ with valid path to this repository

### Change hosts file

Add following line to your hosts file:

    127.0.0.1		  platform.dev

**Location**
- /private/etc/hosts

## Troubleshooting

**Q: File could not be found in theme or fallback theme even if it exists**
A: There is most probably wrong permissions on the file/folder. Use following command to change it:

    sudo chmod -R 755 .


## Credits

Sleighdogs - info@sld.gs
honzao - honza.o@sld.gs
farag - farag@sld.gs


