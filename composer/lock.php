<?php
/**
 * Check if composer.lock file is up to date with composer.json
 */

$lock = json_decode(file_get_contents('composer.lock'))->hash;
$json = md5(file_get_contents('composer.json'));

if ($lock !== $json) {
    echo "Lock file out of date\n";
    exit(1);
}

echo "Lock file up to date\n";
exit(0);