<?php

/**
 * Some procedure to fix most common problems after fresh installation
 *
 * @todo refactoring
 */

$roots = [
	realpath(__DIR__ . '/../storage'),
	realpath(__DIR__ . '/../public/cache')
];

foreach( $roots as $root )
{

	$iter = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
		RecursiveIteratorIterator::SELF_FIRST,
		RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
	);

	$paths = [$root];
	foreach ( $iter as $path => $dir )
	{
		if ( $dir->isDir() )
		{
			$paths[] = $path;
		}
	}

	foreach( $paths as $path )
	{

		if ( !chmod($path, 0777) ) {

			echo "{$path} chmod could not be set";

		}

	}

	$json_path = realpath(__DIR__ . '/../config/domains.json');

	$json = file_get_contents($json_path);

	$sites = json_decode($json, true);

	$storage_path = realpath(__DIR__ . '/../storage/framework/sessions/');

	foreach ( $sites as $site )
	{

		if ( !file_exists(__DIR__ . '/../storage/framework/sessions/' . $site['host']) )
		{

			mkdir(__DIR__ . '/../storage/framework/sessions/' . $site['host'], 0777, true);

		}

		if ( !file_exists(__DIR__ . '/../public/cache/media/' . $site['filesystem_prefix']) )
		{

			mkdir(__DIR__ . '/../public/cache/media/' . $site['filesystem_prefix'], 0777, true);

		}

		if ( !file_exists(__DIR__ . '/../storage/app/db-blade-compiler/views') )
		{

			mkdir(__DIR__ . '/../storage/app/db-blade-compiler/views', 0777, true);

		}

	}

}

// Workbench

$path = __DIR__ . '/../workbench';

if ( !file_exists($path) ) {
	mkdir($path, 0777, true);
}

chmod($path, 0777);
