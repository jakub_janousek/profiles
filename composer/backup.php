<?php
/**
 * Composer script for backing up,
 * composer agnostic copy of
 * php artisan backup
 */

if ( !function_exists('base_path') ) {

    function base_path( $path = null )
    {
        return __DIR__ . '/..' . $path;
    }

}

$path = base_path( '/extensions' );

$date_time = date('Y-m-d-H-i');

$backup = base_path( '/backup/' . $date_time );

if ( !file_exists($backup) )
{
    mkdir($backup, 0777, true);
    chmod($backup, 0777);
}

foreach (
    $iterator = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::SELF_FIRST) as $item
) {
    if ($item->isDir()) {
        mkdir($backup . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    } else {
        copy($item, $backup . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    }
}
