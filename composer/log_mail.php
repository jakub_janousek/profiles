<?php
/**
 * Send debug log summary email to interested receivers
 */

include_once(__DIR__ . '/mail.php');

$extra_receivers = [];

$receivers = array_merge($global_receivers, $extra_receivers);

class LaravelLogViewer
{


    private static $levels_classes = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'danger',
        'critical' => 'danger',
        'alert' => 'danger',
        'emergency' => 'danger',
    ];

    private static $levels_imgs = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'warning',
        'critical' => 'warning',
        'alert' => 'warning',
        'emergency' => 'warning',
    ];

    /**
     * @return array
     */
    public static function all()
    {
        $log = array();

        $log_levels = self::getLogLevels();

        $pattern = '/\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\].*/';

        $path = __DIR__ . '/../storage/logs/laravel-' . date('Y-m-d', time() - 7200) . '.log';

        if (!file_exists($path)) return [];

        $file = file_get_contents($path);

        preg_match_all($pattern, $file, $headings);

        if (!is_array($headings)) return $log;

        $log_data = preg_split($pattern, $file);

        if ($log_data[0] < 1) {
            array_shift($log_data);
        }

        foreach ($headings as $index => $file) {

            foreach( $file as $h )
            {
                $level_key = false;

                foreach ( $log_levels as $log_level => $log_name )
                {

                    if ( strpos($h, '.' . $log_level) !== false )
                    {
                        $level_key = $log_level;
                    }

                }

                if ( !$level_key )
                    continue;

                preg_match('/^\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\].*?\.' . $level_key . ': (.*?)( in .*?:[0-9]+)?$/', $h, $current);

                $log[] = array(
                    'level' => $level_key,
                    'date' => $current[1],
                    'text' => $current[2],
                    'in_file' => isset($current[3]) ? $current[3] : null,
                    'stack' => preg_replace("/^\n*/", '', $h)
                );
            }

        }

        return array_reverse($log);
    }

    /**
     * @return array
     */
    private static function getLogLevels()
    {
        return [
            'DEBUG' => 'info',
            'INFO' => 'info',
            'NOTICE' => 'info',
            'WARNING' => 'warning',
            'ERROR' => 'danger',
            'CRITICAL' => 'danger',
            'ALERT' => 'danger',
            'EMERGENCY' => 'danger',
        ];
    }
}

$debug_html = '';

$debug_html .= '
<style type="text/css">
pre.level {
  border-width: 1px;
  border-style: solid;
  padding: 5px 10px;
  margin-bottom: 10px;
  max-width: 500px;
}
.level-EMERGENCY {
  color: #FF0000;
  border-color: #FF0000;
  background-color: #FF6666;
  font-weight: 700;
}
.level-ALERT {
  color: #FF0000;
  border-color: #FF0000;
  background-color: #FFA3A3;
}
.level-CRITICAL {
  color: #FF0000;
  border-color: #FF0000;
  background-color: #FFD1D1;
}
.level-ERROR {
  color: #B2B200;
  border-color: #B2B200;
  background-color: #ECECC1;
}
.level-WARNING {
  color: #B2B200;
  border-color: #B2B200;
  background-color: #ECECC1;
}
.level-NOTICE {
  color: #1919FF;
  border-color: #1919FF;
  background-color: #BABAFF;
}
.level-INFO {
  color: #1919FF;
  border-color: #1919FF;
  background-color: #BABAFF;
}
.level-DEBUG {
  color: #329932;
  border-color: #329932;
  background-color: #D6EAD6;
}
</style>
';
// @todo: to inline styles

$errors = LaravelLogViewer::all();

if ( empty($errors) ) {

    $debug_html .= '<strong>Dnes žádná chyba nenastala.</strong>';

} else
{

    foreach ( $errors as $line )
    {

        $debug_html .= '<pre class="level level-' . $line['level'] . '">';
        $debug_html .= $line['stack'];
        $debug_html .= '</pre>';

    }

}


if (mandrillmail(date('j.n.Y') . ' debug log', mandrillview('Debug log', 'Hello,<br>this is regular log selection:<pre>'.$debug_html.'</pre><br>Your Wicked Bot'), $receivers) )
{
    echo "debug log notification mail dispatched" . "\n";
}