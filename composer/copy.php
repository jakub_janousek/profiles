<?php

/**
 * Copy files after composer run
 *
 * These serve as vendor overrides for fixes that could not be
 * integrated nicely.
 */

if ( !function_exists('base_path') ) {

    function base_path( $path = null )
    {
        return __DIR__ . '/..' . $path;
    }

}

$path = base_path( '/composer/copy' );

$dest = base_path( '/' );

if ( !file_exists($dest) )
{
    mkdir($dest, 0777, true);
    chmod($dest, 0777);
}

foreach (
    $iterator = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::SELF_FIRST) as $item
) {
    if ($item->isDir()) {
        @mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    } else {
        @copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    }
}
