<?php
/**
 * Vanilla mail functions to send via Mandrill API
 */

if ( !function_exists('br2nl') )
{
    function br2nl($input)
    {
        return preg_replace('/<br(\s+)?\/?>/i', "\n", $input);
    }
}

if ( !function_exists('mandrillmail') ) {

    function mandrillmail(
        $subject = 'Place your subject here',
        $html = 'Paste your content here',
        $receivers = [],
        $from_name = 'Wicked bot',
        $from_email = 'jan.rozklad@gmail.com',
        $key = 'WvymXAxvFMnod0C_MQOBUg'
    ) {

        $uri = 'https://mandrillapp.com/api/1.0/messages/send.json';

        $postString = '{
    "key": "'.$key.'",
    "message": {
        "subject": "'.$subject.'",
        "from_email": "'.$from_email.'",
        "from_name": "'.$from_name.'",
        "to": '.json_encode($receivers).',
        "headers": {},
        "track_opens": true,
        "track_clicks": true,
        "auto_text": true,
        "url_strip_qs": true,
        "preserve_recipients": true,

        "merge": true,
        "global_merge_vars": [
        
        ],
        "merge_vars": [
        
        ],
        "tags": [
        
        ],
        "google_analytics_domains": [
        
        ],
        "google_analytics_campaign": "",
        "metadata": [
        
        ],
        "recipient_metadata": [
        
        ],
        "attachments": [
        
        ]
    },
    "async": false
}';

        $decoded = json_decode($postString, true);
        $decoded['message']['html'] = $html;
        $decoded['message']['text'] = strip_tags(br2nl($html));
        $postString = json_encode($decoded);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);

        $result = curl_exec($ch);

        return $result;

    }

}

if ( !function_exists('mandrillview') ) {

    function mandrillview(
        $headline = null,
        $content = null)
    {

        return "
        <style type=\"text/css\">
          body, p {
            font-size: 16px;
            line-height: 1.5em;
            color: #333;
          }
          small {
            font-size: 12px;
            color: #aaa;
            line-height: 1em;
          }
        </style>
        <h1 style=\"font-family:monospace;\">{$headline}</h1>
        <hr>
        {$content}
        ";

    }

}

$global_receivers = [
    [
        'email' => 'honza.o@sld.gs',
        'name' => 'Rozklad'
    ],
    [
        'email' => 'jan.rozklad@gmail.com',
        'name' => 'Rozklad'
    ],
    [
        'email' => 'prochor666@gmail.com',
        'name' => 'Prochor'
    ],
    [
        'email' => 'matejlukas12@gmail.com',
        'name' => 'Matej'
    ]
];
