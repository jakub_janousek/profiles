<?php
/**
 * Send composer update notification email to interested receivers
 */

include_once(__DIR__ . '/mail.php');

$extra_receivers = [];

$receivers = array_merge($global_receivers, $extra_receivers);

if ( mandrillmail(date('j.n.Y H:i') . ' composer update', mandrillview('Composer update', 'Nazdar,<br><strong>'.date('j.n.Y H:i').'</strong> ran on.<br><br><pre style="color:blue;">'.$_SERVER['PWD'].'</pre><br>Your Wicked Bot'), $receivers) )
{
    echo "composer update notification mail dispatched" . "\n";
}