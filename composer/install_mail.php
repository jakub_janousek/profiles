<?php
/**
 * Send composer install notification email to interested receivers
 */

include_once(__DIR__ . '/mail.php');

$extra_receivers = [];

$receivers = array_merge($global_receivers, $extra_receivers);

if (mandrillmail(date('j.n.Y H:i') . ' ran composer install', mandrillview('Composer install', 'Hello,<br><strong>'.date('j.n.Y H:i').'</strong> ran composer install<br><br><pre style="color:blue;">'.$_SERVER['PWD'].'</pre><br>Your wicked bot'), $receivers))
{
    echo "composer install notification mail dispatched" . "\n";
}