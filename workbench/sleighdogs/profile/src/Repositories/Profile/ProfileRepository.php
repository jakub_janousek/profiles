<?php namespace Sleighdogs\Profile\Repositories\Profile;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;

class ProfileRepository implements ProfileRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Sleighdogs\Profile\Handlers\Profile\ProfileDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent profile model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sleighdogs.profile.profile.handler.data'];

		$this->setValidator($app['sleighdogs.profile.profile.validator']);

		$this->setModel(get_class($app['Sleighdogs\Profile\Models\Profile']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.profile.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.profile.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new profile
		$profile = $this->createModel();

		// Fire the 'sleighdogs.profile.profile.creating' event
		if ($this->fireEvent('sleighdogs.profile.profile.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
                        // Remove qualifications from the array of profile attributes
                        $qualifications = $data['qualifications'];
                        unset($data['qualifications']);
                        
			// Save the profile
			$profile->fill($data)->save();
                        
                        // profile saved, proceed to saving qualifications
                        $profile->qualifications()->detach();
                        $profile->qualifications()->attach($qualifications);
                        
			// Fire the 'sleighdogs.profile.profile.created' event
			$this->fireEvent('sleighdogs.profile.profile.created', [ $profile ]);
		}

		return [ $messages, $profile ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the profile object
		$profile = $this->find($id);

		// Fire the 'sleighdogs.profile.profile.updating' event
		if ($this->fireEvent('sleighdogs.profile.profile.updating', [ $profile, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($profile, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
                        // Remove qualifications from the array of profile attributes
                        $qualifications = $data['qualifications'];
                        unset($data['qualifications']);
			// Update the profile
			$profile=$profile->fill($data);
                        
                        $profile->save();
                        // profile saved, proceed to saving qualifications
                        $profile->qualifications()->detach();
                        $profile->qualifications()->attach($qualifications);
                        
			// Fire the 'sleighdogs.profile.profile.updated' event
			$this->fireEvent('sleighdogs.profile.profile.updated', [ $profile ]);
		}

		return [ $messages, $profile ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the profile exists
		if ($profile = $this->find($id))
		{
			// Fire the 'sleighdogs.profile.profile.deleting' event
			$this->fireEvent('sleighdogs.profile.profile.deleting', [ $profile ]);

			// Delete the profile entry
			$profile->delete();

			// Fire the 'sleighdogs.profile.profile.deleted' event
			$this->fireEvent('sleighdogs.profile.profile.deleted', [ $profile ]);

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
