<?php namespace Sleighdogs\Profile\Repositories\Profile;

interface ProfileRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Sleighdogs\Profile\Models\Profile
	 */
	public function grid();

	/**
	 * Returns all the profile entries.
	 *
	 * @return \Sleighdogs\Profile\Models\Profile
	 */
	public function findAll();

	/**
	 * Returns a profile entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Sleighdogs\Profile\Models\Profile
	 */
	public function find($id);

	/**
	 * Determines if the given profile is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given profile is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given profile.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a profile entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Sleighdogs\Profile\Models\Profile
	 */
	public function create(array $data);

	/**
	 * Updates the profile entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Sleighdogs\Profile\Models\Profile
	 */
	public function update($id, array $data);

	/**
	 * Deletes the profile entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
