<?php namespace Sleighdogs\Profile\Repositories\Individual;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;
use Platform\Users\Repositories\UserRepository;

class IndividualRepository extends UserRepository implements IndividualRepositoryInterface {

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sleighdogs.profile.individual.handler.data'];

		$this->setValidator($app['sleighdogs.profile.individual.validator']);

		$this->setModel($app['sentinel.users']);
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createLocalModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.individual.all', function()
		{
			return $this->createLocalModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.individual.'.$id, function() use ($id)
		{
			return $this->createLocalModel()->find($id);
		});
	}

	public function createLocalModel()
	{
		return new \Sleighdogs\Profile\Models\Individual;
	}
}
