<?php namespace Sleighdogs\Profile\Repositories\Individual;

interface IndividualRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Sleighdogs\Profile\Models\Individual
	 */
	public function grid();

	/**
	 * Returns all the profile entries.
	 *
	 * @return \Sleighdogs\Profile\Models\Individual
	 */
	public function findAll();

	/**
	 * Returns a profile entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Sleighdogs\Profile\Models\Individual
	 */
	public function find($id);

	/**
	 * Creates or updates the given profile.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a profile entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Sleighdogs\Profile\Models\Individual
	 */
	public function create(array $data);

	/**
	 * Updates the profile entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Sleighdogs\Profile\Models\Individual
	 */
	public function update($id, array $data);

	/**
	 * Deletes the profile entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
