<?php namespace Sleighdogs\Profile\Repositories\Corporate;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;
use Platform\Users\Repositories\UserRepository;

class CorporateRepository extends UserRepository implements CorporateRepositoryInterface {

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sleighdogs.profile.corporate.handler.data'];

		$this->setValidator($app['sleighdogs.profile.corporate.validator']);

		$this->setModel($app['sentinel.users']);
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createLocalModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.corporate.all', function()
		{
			return $this->createLocalModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.corporate.'.$id, function() use ($id)
		{
			return $this->createLocalModel()->find($id);
		});
	}

	public function createLocalModel()
	{
		return new \Sleighdogs\Profile\Models\Corporate;
	}

	/**
     * {@inheritDoc}
     */
    public function getAllTags()
    {
    	$corporate_tags = $this->getCorporateTags();

    	$user_tags = $this->getUserTags();

        return array_unique( array_merge($corporate_tags->toArray(), $user_tags->toArray()), SORT_REGULAR);
    }

    public function getCorporateTags()
    {
    	return (new \Sleighdogs\Profile\Models\Corporate)->allTags()->lists('name');
    }

    public function getUserTags()
    {
    	return (new \Platform\Users\Models\User)->allTags()->lists('name');
    }
}
