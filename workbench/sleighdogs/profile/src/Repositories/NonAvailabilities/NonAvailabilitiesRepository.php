<?php namespace Sleighdogs\Profile\Repositories\NonAvailabilities;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;

class NonAvailabilitiesRepository implements NonAvailabilitiesRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Sleighdogs\Profile\Handlers\NonAvailabilities\NonAvailabilitiesDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent profile model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sleighdogs.profile.nonavailabilities.handler.data'];

		$this->setValidator($app['sleighdogs.profile.nonavailabilities.validator']);

		$this->setModel(get_class($app['Sleighdogs\Profile\Models\NonAvailabilities']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.nonavailabilities.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sleighdogs.profile.nonavailabilities.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
                unset($input['submit']);
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new nonavailabilities
		$nonavailabilities = $this->createModel();

		// Fire the 'sleighdogs.profile.nonavailabilities.creating' event
		if ($this->fireEvent('sleighdogs.profile.nonavailabilities.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the nonavailabilities
			$nonavailabilities->fill($data)->save();

			// Fire the 'sleighdogs.profile.nonavailabilities.created' event
			$this->fireEvent('sleighdogs.profile.nonavailabilities.created', [ $nonavailabilities ]);
		}

		return [ $messages, $nonavailabilities ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the nonavailabilities object
		$nonavailabilities = $this->find($id);

		// Fire the 'sleighdogs.profile.nonavailabilities.updating' event
		if ($this->fireEvent('sleighdogs.profile.nonavailabilities.updating', [ $nonavailabilities, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($nonavailabilities, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the nonavailabilities
			$nonavailabilities->fill($data)->save();

			// Fire the 'sleighdogs.profile.nonavailabilities.updated' event
			$this->fireEvent('sleighdogs.profile.nonavailabilities.updated', [ $nonavailabilities ]);
		}

		return [ $messages, $nonavailabilities ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the nonavailabilities exists
		if ($nonavailabilities = $this->find($id))
		{
			// Fire the 'sleighdogs.profile.nonavailabilities.deleting' event
			$this->fireEvent('sleighdogs.profile.nonavailabilities.deleting', [ $nonavailabilities ]);

			// Delete the nonavailabilities entry
			$nonavailabilities->delete();

			// Fire the 'sleighdogs.profile.nonavailabilities.deleted' event
			$this->fireEvent('sleighdogs.profile.nonavailabilities.deleted', [ $nonavailabilities ]);

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
