<?php namespace Sleighdogs\Profile\Models;

use Platform\Users\Models\User as PlatformUser;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;
use Sleighdogs\Profile\Scopes\CorporateScope;
use Sanatorium\Inputs\Traits\MediableTrait;
use Sleighdogs\Profile\Traits\MaillogTrait;

class Corporate extends PlatformUser implements TaggableInterface 
{

	//public $morphClass = 'Sleighdogs\Profile\Models\Corporate';
	
	use TaggableTrait, MediableTrait, MaillogTrait;

	/**
     * The model namespace.
     *
     * @var string
     */
    protected static $entityNamespace = 'sleighdogs/profile.corporate';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CorporateScope);
    }

    public function individuals()
    {
        return $this
            ->belongsToMany('Sleighdogs\Profile\Models\User', 'corporate_users', 'corporate_id', 'user_id')
            ->withPivot('position', 'started_at', 'ended_at', 'allowed_to_change');
    }

    public function maillists()
    {
        return $this->belongsToMany('Sleighdogs\Maillist\Models\Maillist', 'maillist_users', 'user_id', 'maillist_id')
            ->withPivot('opt_out');
    }


}