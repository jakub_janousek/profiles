<?php namespace Sleighdogs\Profile\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Profile extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'profiles';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'sleighdogs/profile.profile';
        
        public function salutation(){
            
            return $this->belongsTo('Sleighdogs\Profile\Models\Salutation');
        }

        public function category(){
            return $this->belongsTo('Sleighdogs\Profile\Models\Category');
        }
        
        public function degree() {
            return $this->belongsTo('Sleighdogs\Profile\Models\Degree');
        }
        
        public function discipline() {
            return $this->belongsTo('Sleighdogs\Profile\Models\Discipline');
        }
        
        public function qualifications() {
            return $this->belongsToMany('Sleighdogs\Qualifications\Models\Qualifications');
        }

}
