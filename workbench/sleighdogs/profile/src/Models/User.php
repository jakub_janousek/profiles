<?php namespace Sleighdogs\Profile\Models;

use Platform\Users\Models\User as PlatformUser;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;
use Sleighdogs\Profile\Traits\MaillogTrait;

class User extends PlatformUser implements TaggableInterface 
{

	public $morphClass = 'Platform\Users\Models\User';
	
	use TaggableTrait, MaillogTrait;

	public function corporates()
	{
		return $this
			->belongsToMany('Sleighdogs\Profile\Models\Corporate', 'corporate_users', 'user_id', 'corporate_id')
			->withPivot('position', 'started_at', 'ended_at', 'allowed_to_change');
	}

	public function current_corporates()
    {
        $now_db_format = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        return $this
            ->belongsToMany('Sleighdogs\Profile\Models\Corporate', 'corporate_users', 'user_id', 'corporate_id')
            ->withPivot('position', 'started_at', 'ended_at', 'allowed_to_change')
            ->where(function($q) use ($now_db_format){
                return $q->where('started_at', '<', $now_db_format)
                    ->orWhere('started_at', null);
            })
            ->where(function($q) use ($now_db_format){
            	return $q->where('ended_at', '>', $now_db_format)
            		->orWhere('ended_at', null);
            });
    }

    public function managed_corporates()
    {
        $now_db_format = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        return $this
            ->belongsToMany('Sleighdogs\Profile\Models\Corporate', 'corporate_users', 'user_id', 'corporate_id')
            ->withPivot('position', 'started_at', 'ended_at', 'allowed_to_change')
            ->where('allowed_to_change', 1)
            ->where(function($q) use ($now_db_format){
                return $q->where('started_at', '<', $now_db_format)
                    ->orWhere('started_at', null);
            })
            ->where(function($q) use ($now_db_format){
                return $q->where('ended_at', '>', $now_db_format)
                    ->orWhere('ended_at', null);
            });
    }

    public function isCapableToCorporate($corporate_id)
    {
    	$corporate = $this->current_corporates()->find($corporate_id);

    	if ( is_object($corporate) ) {
        	return $corporate->pivot->allowed_to_change;
    	}
    	
    	return false;
    }

	public function getGravatarUrlAttribute()
	{
        if ( !empty($this->avatar) )
        {
            $medium = \Platform\Media\Models\Media::find($this->avatar);

            return route('thumb.view', $medium->path);
        }


		return 'http://www.gravatar.com/avatar/'.md5( strtolower( trim( $this->email) ) );
	}

    public function maillists()
    {
        return $this->belongsToMany('Sleighdogs\Maillist\Models\Maillist', 'maillist_users', 'user_id', 'maillist_id')
            ->withPivot('opt_out');
    }

}