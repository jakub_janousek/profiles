<?php namespace Sleighdogs\Profile\Models;

use Platform\Users\Models\User as PlatformUser;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;
use Sleighdogs\Profile\Scopes\IndividualScope;
use Sleighdogs\Profile\Traits\MaillogTrait;

class Individual extends PlatformUser implements TaggableInterface 
{

	public $morphClass = 'Platform\Users\Models\User';
	
	use TaggableTrait, MaillogTrait;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IndividualScope);
    }    


}