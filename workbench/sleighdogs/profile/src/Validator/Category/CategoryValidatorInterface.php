<?php namespace Sleighdogs\Profile\Validator\Category;

interface CategoryValidatorInterface {

	/**
	 * Updating a category scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
