<?php namespace Sleighdogs\Profile\Validator\NonAvailabilities;

interface NonAvailabilitiesValidatorInterface {

	/**
	 * Updating a nonavailabilities scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
