<?php namespace Sleighdogs\Profile\Validator\NonAvailabilities;

use Cartalyst\Support\Validator;

class NonAvailabilitiesValidator extends Validator implements NonAvailabilitiesValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [

	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
