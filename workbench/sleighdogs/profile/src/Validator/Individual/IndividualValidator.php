<?php namespace Sleighdogs\Profile\Validator\Individual;

use Cartalyst\Support\Validator;

class IndividualValidator extends Validator implements IndividualValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [

	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
