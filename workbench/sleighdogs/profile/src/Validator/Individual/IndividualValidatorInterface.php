<?php namespace Sleighdogs\Profile\Validator\Individual;

interface IndividualValidatorInterface {

	/**
	 * Updating a corporate scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
