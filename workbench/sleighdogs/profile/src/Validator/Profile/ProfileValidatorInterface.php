<?php namespace Sleighdogs\Profile\Validator\Profile;

interface ProfileValidatorInterface {

	/**
	 * Updating a profile scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
