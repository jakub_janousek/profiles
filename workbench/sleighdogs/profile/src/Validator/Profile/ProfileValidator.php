<?php namespace Sleighdogs\Profile\Validator\Profile;

use Cartalyst\Support\Validator;

class ProfileValidator extends Validator implements ProfileValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
            'firstname' => 'required|min:2|max:30',
            'lastname' => 'required|min:2|max:30',
            'salutation' => 'required',
            'gender' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'category' => 'required',
            'education' => 'required|min:5',
            'degree' => 'required',
            'discipline' => 'required',
            'company' => 'required',
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
