<?php namespace Sleighdogs\Profile\Validator\Corporate;

interface CorporateValidatorInterface {

	/**
	 * Updating a corporate scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
