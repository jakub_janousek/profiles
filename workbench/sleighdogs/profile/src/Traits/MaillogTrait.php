<?php namespace Sleighdogs\Profile\Traits;

trait MaillogTrait {

	public function maillogs()
    {
        return \Sanatorium\Mailer\Models\Maillog::where('receiver', $this->email);
    }

    public function getLastEmail()
    {
        $entry = $this->maillogs()->orderBy('created_at', 'DESC')->first();

        if ( is_object($entry) ) {
            $translation = trans('sanatorium/mailer::events.' .$entry->event);

            if ( $translation !== 'sanatorium/mailer::events.' .$entry->event ) {
                $display_event = $translation;
            } else {
                $display_event = $entry->event;
            }

            $entry->event = $display_event;

            return $entry;
        }

        return null;
    }
	
}