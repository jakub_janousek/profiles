<?php namespace Sleighdogs\Profile\Handlers\NonAvailabilities;

interface NonAvailabilitiesDataHandlerInterface {

	/**
	 * Prepares the given data for being stored.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function prepare(array $data);

}
