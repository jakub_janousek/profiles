<?php namespace Sleighdogs\Profile\Handlers\NonAvailabilities;

use Sleighdogs\Profile\Models\NonAvailabilities;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface NonAvailabilitiesEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a nonavailabilities is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a nonavailabilities is created.
	 *
	 * @param  \Sleighdogs\Profile\Models\NonAvailabilities  $nonavailabilities
	 * @return mixed
	 */
	public function created(NonAvailabilities $nonavailabilities);

	/**
	 * When a nonavailabilities is being updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\NonAvailabilities  $nonavailabilities
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(NonAvailabilities $nonavailabilities, array $data);

	/**
	 * When a nonavailabilities is updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\NonAvailabilities  $nonavailabilities
	 * @return mixed
	 */
	public function updated(NonAvailabilities $nonavailabilities);

	/**
	 * When a nonavailabilities is being deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\NonAvailabilities  $nonavailabilities
	 * @return mixed
	 */
	public function deleting(NonAvailabilities $nonavailabilities);

	/**
	 * When a nonavailabilities is deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\NonAvailabilities  $nonavailabilities
	 * @return mixed
	 */
	public function deleted(NonAvailabilities $nonavailabilities);

}
