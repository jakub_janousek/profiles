<?php namespace Sleighdogs\Profile\Handlers\NonAvailabilities;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Profile\Models\NonAvailabilities;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class NonAvailabilitiesEventHandler extends BaseEventHandler implements NonAvailabilitiesEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.profile.nonavailabilities.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.profile.nonavailabilities.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.profile.nonavailabilities.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.profile.nonavailabilities.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.profile.nonavailabilities.deleted', __CLASS__.'@deleting');
		$dispatcher->listen('sleighdogs.profile.nonavailabilities.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(NonAvailabilities $nonavailabilities)
	{
		$this->flushCache($nonavailabilities);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(NonAvailabilities $nonavailabilities, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(NonAvailabilities $nonavailabilities)
	{
		$this->flushCache($nonavailabilities);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleting(NonAvailabilities $nonavailabilities)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(NonAvailabilities $nonavailabilities)
	{
		$this->flushCache($nonavailabilities);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Profile\Models\NonAvailabilities  $nonavailabilities
	 * @return void
	 */
	protected function flushCache(NonAvailabilities $nonavailabilities)
	{
		$this->app['cache']->forget('sleighdogs.profile.nonavailabilities.all');

		$this->app['cache']->forget('sleighdogs.profile.nonavailabilities.'.$nonavailabilities->id);
	}

}
