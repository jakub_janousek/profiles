<?php namespace Sleighdogs\Profile\Handlers\Corporate;

use Sleighdogs\Profile\Models\Corporate;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface CorporateEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a corporate is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a corporate is created.
	 *
	 * @param  \Sleighdogs\Profile\Models\Corporate  $corporate
	 * @return mixed
	 */
	public function created(Corporate $corporate);

	/**
	 * When a corporate is being updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Corporate  $corporate
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Corporate $corporate, array $data);

	/**
	 * When a corporate is updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Corporate  $corporate
	 * @return mixed
	 */
	public function updated(Corporate $corporate);

	/**
	 * When a corporate is deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\Corporate  $corporate
	 * @return mixed
	 */
	public function deleted(Corporate $corporate);

}
