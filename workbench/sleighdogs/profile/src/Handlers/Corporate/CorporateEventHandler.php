<?php namespace Sleighdogs\Profile\Handlers\Corporate;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Profile\Models\Corporate;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class CorporateEventHandler extends BaseEventHandler implements CorporateEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.profile.corporate.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.profile.corporate.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.profile.corporate.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.profile.corporate.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.profile.corporate.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Corporate $corporate)
	{
		$this->flushCache($corporate);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Corporate $corporate, array $data)
	{
		
	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Corporate $corporate)
	{
		$this->flushCache($corporate);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Corporate $corporate)
	{
		$this->flushCache($corporate);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Profile\Models\Corporate  $corporate
	 * @return void
	 */
	protected function flushCache(Corporate $corporate)
	{
		$this->app['cache']->forget('sleighdogs.profile.corporate.all');

		$this->app['cache']->forget('sleighdogs.profile.corporate.'.$corporate->id);
	}

}
