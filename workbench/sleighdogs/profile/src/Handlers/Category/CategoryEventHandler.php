<?php namespace Sleighdogs\Profile\Handlers\Category;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Profile\Models\Category;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class CategoryEventHandler extends BaseEventHandler implements CategoryEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.profile.category.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.profile.category.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.profile.category.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.profile.category.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.profile.category.deleted', __CLASS__.'@deleting');
		$dispatcher->listen('sleighdogs.profile.category.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Category $category)
	{
		$this->flushCache($category);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Category $category, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Category $category)
	{
		$this->flushCache($category);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleting(Category $category)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Category $category)
	{
		$this->flushCache($category);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Profile\Models\Category  $category
	 * @return void
	 */
	protected function flushCache(Category $category)
	{
		$this->app['cache']->forget('sleighdogs.profile.category.all');

		$this->app['cache']->forget('sleighdogs.profile.category.'.$category->id);
	}

}
