<?php namespace Sleighdogs\Profile\Handlers\Category;

use Sleighdogs\Profile\Models\Category;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface CategoryEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a category is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a category is created.
	 *
	 * @param  \Sleighdogs\Profile\Models\Category  $category
	 * @return mixed
	 */
	public function created(Category $category);

	/**
	 * When a category is being updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Category  $category
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Category $category, array $data);

	/**
	 * When a category is updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Category  $category
	 * @return mixed
	 */
	public function updated(Category $category);

	/**
	 * When a category is being deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\Category  $category
	 * @return mixed
	 */
	public function deleting(Category $category);

	/**
	 * When a category is deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\Category  $category
	 * @return mixed
	 */
	public function deleted(Category $category);

}
