<?php namespace Sleighdogs\Profile\Handlers\Individual;

use Sleighdogs\Profile\Models\Individual;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface IndividualEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a individual is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a individual is created.
	 *
	 * @param  \Sleighdogs\Profile\Models\Individual  $individual
	 * @return mixed
	 */
	public function created(Individual $individual);

	/**
	 * When a individual is being updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Individual  $individual
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Individual $individual, array $data);

	/**
	 * When a individual is updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Individual  $individual
	 * @return mixed
	 */
	public function updated(Individual $individual);

	/**
	 * When a individual is deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\Individual  $individual
	 * @return mixed
	 */
	public function deleted(Individual $individual);

}
