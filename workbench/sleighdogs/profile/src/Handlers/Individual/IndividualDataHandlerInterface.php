<?php namespace Sleighdogs\Profile\Handlers\Individual;

interface IndividualDataHandlerInterface {

	/**
	 * Prepares the given data for being stored.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function prepare(array $data);

}
