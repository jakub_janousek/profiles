<?php namespace Sleighdogs\Profile\Handlers\Individual;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Profile\Models\Individual;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class IndividualEventHandler extends BaseEventHandler implements IndividualEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.profile.individual.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.profile.individual.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.profile.individual.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.profile.individual.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.profile.individual.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Individual $individual)
	{
		$this->flushCache($individual);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Individual $individual, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Individual $individual)
	{
		$this->flushCache($individual);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Individual $individual)
	{
		$this->flushCache($individual);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Profile\Models\Individual  $individual
	 * @return void
	 */
	protected function flushCache(Individual $individual)
	{
		$this->app['cache']->forget('sleighdogs.profile.individual.all');

		$this->app['cache']->forget('sleighdogs.profile.individual.'.$individual->id);
	}

}
