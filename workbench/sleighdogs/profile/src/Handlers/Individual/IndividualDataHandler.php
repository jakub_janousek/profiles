<?php namespace Sleighdogs\Profile\Handlers\Individual;

class IndividualDataHandler implements IndividualDataHandlerInterface {

	/**
     * {@inheritDoc}
     */
    public function prepare(array $data, $removePassword = false)
    {
        $permissions = array_filter(array_map(function ($permission) {
            if ($permission == 0) {
                return;
            }

            return $permission == 1 ? true : false;
        }, array_get($data, 'permissions', [])), function ($v) { return ! is_null($v); });

        // Check if we should remove the password from the
        // submitted data, this is because the password
        // is not required when updating a user.
        $data = array_where($data, function ($key, $value) use ($removePassword) {
            if ($removePassword && $key === 'password' && empty($value)) {
                return false;
            }

            return true;
        });

        return array_merge($data, compact('permissions'));
    }

}
