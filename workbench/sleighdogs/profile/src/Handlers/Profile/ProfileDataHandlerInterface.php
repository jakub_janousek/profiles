<?php namespace Sleighdogs\Profile\Handlers\Profile;

interface ProfileDataHandlerInterface {

	/**
	 * Prepares the given data for being stored.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function prepare(array $data);

}
