<?php namespace Sleighdogs\Profile\Handlers\Profile;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Profile\Models\Profile;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class ProfileEventHandler extends BaseEventHandler implements ProfileEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.profile.profile.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.profile.profile.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.profile.profile.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.profile.profile.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.profile.profile.deleted', __CLASS__.'@deleting');
		$dispatcher->listen('sleighdogs.profile.profile.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Profile $profile)
	{
		$this->flushCache($profile);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Profile $profile, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Profile $profile)
	{
		$this->flushCache($profile);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleting(Profile $profile)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Profile $profile)
	{
		$this->flushCache($profile);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Profile\Models\Profile  $profile
	 * @return void
	 */
	protected function flushCache(Profile $profile)
	{
		$this->app['cache']->forget('sleighdogs.profile.profile.all');

		$this->app['cache']->forget('sleighdogs.profile.profile.'.$profile->id);
	}

}
