<?php namespace Sleighdogs\Profile\Handlers\Profile;

use Sleighdogs\Profile\Models\Profile;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ProfileEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a profile is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a profile is created.
	 *
	 * @param  \Sleighdogs\Profile\Models\Profile  $profile
	 * @return mixed
	 */
	public function created(Profile $profile);

	/**
	 * When a profile is being updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Profile  $profile
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Profile $profile, array $data);

	/**
	 * When a profile is updated.
	 *
	 * @param  \Sleighdogs\Profile\Models\Profile  $profile
	 * @return mixed
	 */
	public function updated(Profile $profile);

	/**
	 * When a profile is being deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\Profile  $profile
	 * @return mixed
	 */
	public function deleting(Profile $profile);

	/**
	 * When a profile is deleted.
	 *
	 * @param  \Sleighdogs\Profile\Models\Profile  $profile
	 * @return mixed
	 */
	public function deleted(Profile $profile);

}
