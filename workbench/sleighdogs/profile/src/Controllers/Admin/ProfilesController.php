<?php

namespace Sleighdogs\Profile\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Sleighdogs\Profile\Repositories\Profile\ProfileRepositoryInterface;
use Sleighdogs\Profile\Models\Salutation;
use \Sleighdogs\Profile\Models\Category;
use \Sleighdogs\Profile\Models\Degree;
use \Sleighdogs\Profile\Models\Discipline;
use \Sleighdogs\Qualifications\Models\Qualifications;

class ProfilesController extends AdminController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Profile repository.
     *
     * @var \Sleighdogs\Profile\Repositories\Profile\ProfileRepositoryInterface
     */
    protected $profiles;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
        'enable',
        'disable',
    ];

    /**
     * Constructor.
     *
     * @param  \Sleighdogs\Profile\Repositories\Profile\ProfileRepositoryInterface  $profiles
     * @return void
     */
    public function __construct(ProfileRepositoryInterface $profiles) {
        parent::__construct();

        $this->profiles = $profiles;
    }

    /**
     * Display a listing of profile.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('sleighdogs/profile::profiles.index');
    }

    /**
     * Datasource for the profile Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $data = $this->profiles->grid();

        $columns = [
            'id',
            'firstname',
            'lastname',
            'salutation',
            'gender',
            'portrait',
            'email',
            'password',
            'phone',
            'category',
            'education',
            'degree',
            'discipline',
            'company',
            'created_at',
        ];

        $settings = [
            'sort' => 'created_at',
            'direction' => 'desc',
        ];

        $transformer = function($element) {
            $element->edit_uri = route('admin.sleighdogs.profile.profiles.edit', $element->id);

            return $element;
        };

        return datagrid($data, $columns, $settings, $transformer);
    }

    /**
     * Show the form for creating new profile.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new profile.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating profile.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('update', $id);
    }

    /**
     * Handle posting of the form for updating profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->profiles->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
                trans("sleighdogs/profile::profiles/message.{$type}.delete")
        );

        return redirect()->route('admin.sleighdogs.profile.profiles.all');
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction() {
        $action = request()->input('action');

        if (in_array($action, $this->actions)) {
            foreach (request()->input('rows', []) as $row) {
                $this->profiles->{$action}($row);
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {
        $salutations = Salutation::lists('name','id');
        $categories = Category::lists('name','id');
        $degrees = Degree::lists('name','id');
        $disciplines = Discipline::lists('name','id');
        $qualifications = Qualifications::lists('name','id');
        $selectedQualifications = array();
        // Do we have a profile identifier?
        if (isset($id)) {
            if (!$profile = $this->profiles->find($id)) {
                $this->alerts->error(trans('sleighdogs/profile::profiles/message.not_found', compact('id')));
                
                return redirect()->route('admin.sleighdogs.profile.profiles.all');
            }
            if(count($profile->qualifications)){
                foreach($profile->qualifications as $q){
                    $selectedQualifications[] = $q->id;
                }
            }
            
        } else {
            $profile = $this->profiles->createModel();
        }

        // Show the page
        return view('sleighdogs/profile::profiles.form', compact('mode', 'profile', 'salutations', 'categories', 'degrees', 'disciplines', 'qualifications','selectedQualifications'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        // Store the profile
        list($messages) = $this->profiles->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("sleighdogs/profile::profiles/message.success.{$mode}"));

            return redirect()->route('admin.sleighdogs.profile.profiles.all');
        }

        $this->alerts->error($messages, 'form');

        return redirect()->back()->withInput();
    }

}
