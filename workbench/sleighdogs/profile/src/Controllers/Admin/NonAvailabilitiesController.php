<?php 

namespace Sleighdogs\Profile\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Sleighdogs\Profile\Repositories\NonAvailabilities\NonAvailabilitiesRepositoryInterface;
use Sleighdogs\Profile\Models\Profile;
use Mail;

class NonAvailabilitiesController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Profile repository.
	 *
	 * @var \Sleighdogs\Profile\Repositories\NonAvailabilities\NonAvailabilitiesRepositoryInterface
	 */
	protected $nonavailabilities;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Sleighdogs\Profile\Repositories\NonAvailabilities\NonAvailabilitiesRepositoryInterface  $nonavailabilities
	 * @return void
	 */
	public function __construct(NonAvailabilitiesRepositoryInterface $nonavailabilities)
	{
		parent::__construct();
                
		$this->nonavailabilities = $nonavailabilities;
	}

	/**
	 * Display a listing of nonavailabilities.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sleighdogs/profile::nonavailabilities.index');
	}

	/**
	 * Datasource for the nonavailabilities Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->nonavailabilities->grid();

		$columns = [
			'*',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.sleighdogs.nonavailabilities.nonavailabilities.edit', $element->id);

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new nonavailabilities.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new nonavailabilities.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating nonavailabilities.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating nonavailabilities.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified nonavailabilities.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->nonavailabilities->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("sleighdogs/profile::nonavailabilities/message.{$type}.delete")
		);

		return redirect()->route('admin.sleighdogs.nonavailabilities.nonavailabilities.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			foreach (request()->input('rows', []) as $row)
			{
				$this->nonavailabilities->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
                $profiles = Profile::lists('lastname','id');
//                dd($profiles);
		// Do we have a nonavailabilities identifier?
		if (isset($id))
		{
			if ( ! $nonavailabilities = $this->nonavailabilities->find($id))
			{
				$this->alerts->error(trans('sleighdogs/profile::nonavailabilities/message.not_found', compact('id')));

				return redirect()->route('admin.sleighdogs.nonavailabilities.nonavailabilities.all');
			}
		}
		else
		{
			$nonavailabilities = $this->nonavailabilities->createModel();
		}

		// Show the page
		return view('sleighdogs/profile::nonavailabilities.form', compact('mode', 'nonavailabilities','profiles'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
                $request = request()->all();
                
		// Store the nonavailabilities
		list($messages) = $this->nonavailabilities->store($id, $request);
                
		// Do we have any errors?
		if ($messages->isEmpty() && !request()->ajax())
		{
			$this->alerts->success(trans("sleighdogs/profile::nonavailabilities/message.success.{$mode}"));
                        $this->sendEmail(request()->all());
			return redirect()->route('admin.sleighdogs.nonavailabilities.nonavailabilities.all');
                }elseif($messages->isEmpty()){
                    return response()->json(["success"=>"true"]);
                }

		$this->alerts->error($messages, 'form');

		return redirect()->back()->withInput();
	}
        
        public function sendEmail($request)
        {
            $user = Profile::findOrFail($request['profile_id']);

            Mail::send('sleighdogs/profile::nonavailabilities.mail.body', ['request' => $request,'user' => $user], function ($m) use ($user,$request) {
                $m->from('hello@app.com', 'SWB EXPERT SERVICE');

                $m->to($user->email, $user->firstname . " " . $user->lastname)->subject("You're Booked!");
            });
        }

}
