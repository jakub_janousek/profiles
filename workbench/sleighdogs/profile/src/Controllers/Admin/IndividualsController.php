<?php namespace Sleighdogs\Profile\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Sleighdogs\Profile\Repositories\Individual\IndividualRepositoryInterface;

class IndividualsController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Profile repository.
	 *
	 * @var \Sleighdogs\Profile\Repositories\Individual\IndividualRepositoryInterface
	 */
	protected $individuals;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
		'static_mail',
		'dynamic_mail',
		'remind_percent',
		'set_tag',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Sleighdogs\Profile\Repositories\Individual\IndividualRepositoryInterface  $individuals
	 * @return void
	 */
	public function __construct(IndividualRepositoryInterface $individuals)
	{
		parent::__construct();

		$this->individuals = $individuals;
	}

	/**
	 * Display a listing of individual.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		// Get a list of all the available tags
        // @todo - do not draw it from corporate repository, create new for all
        $tags = app('sleighdogs.corporate')->getAllTags();

        // @todo - remove in production
		$this->cachetest();

		return view('sleighdogs/profile::individuals.index', compact('tags'));
	}

	public function cachetest()
	{
		/*
		$users = app('platform.users');

		foreach( $users->all() as $user ) {
			// Add slug
	        if ( !$user->slug ) {
	            switch($user->type) {
	                case 'corporate':
	                    $user->slug = str_slug($user->brand_name);
	                    $user->save();
	                break;
	                default:
	                    $user->slug = str_slug($user->first_name . ' ' . $user->last_name);
	                    $user->save();
	                break;
	            }
	        }
		}
		*/

		foreach( $this->individuals->with('values')->get() as $user ) {
			$data = [];

			$searchable = [
				'website',
				'brand_name',
				'addresses',
				'number_of_employees',
				'founding_year',
				'official_administrative_name',
				'general_email',
				'short_description',
				'position',
			];

			foreach( $user->getAttributes() as $key => $value ) {
				if ( !empty($value) && $key != 'serialized_attributes') {
					$data[$key] = $value;
				}
			}

			if ( $user->type == 'corporate' ) {
				$user = \Sleighdogs\Profile\Models\Corporate::find($user->id);	
			}

			$tags = [];

			foreach( $user->tags()->lists('name') as $tag ) {
				$tags[] = $tag;
			}

			$data['tags'] = $tags;

			foreach ( $searchable as $key ) {
				if ( !empty($user->{$key}) ) {
					$data[$key] = $user->{$key};
				}
			}

			$user->serialized_attributes = serialize($data);

			if ( $user->type == 'corporate' ) {
				$user->persist_brand_name = $user->brand_name;
				$user->persist_number_of_employees = $user->number_of_employees;
				$user->persist_founding_year = $user->founding_year;
				$user->persist_website = $user->website;
				$user->persist_street = $user->street;
				$user->persist_postcode = $user->postcode;
				$user->persist_city = $user->city;
			} else {
				$user->persist_gender = $user->gender;

				$position = $user->position;
				if ( is_array($position) ) {
					$user->persist_position = $position[0];
				}
				$user->persist_office_phone = $user->office_phone;
			}

			if ( method_exists($user, 'getLastEmail') ) {
				$email = $user->getLastEmail();
				if ( is_object($email) ) {
					$user->persist_last_email = $email->event;
				}
			}
			$user->persist_completeness = round( \Sleighdogs\Profile\Widgets\Stats::getProfilePercentage($user) );
			$user->persist_tags = implode(',', $tags);

			// temporarily disable timestamps settings
			$user->timestamps = false;

			$user->save();
		}
	}

	/**
	 * Datasource for the individual Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->individuals->grid()->with('values')->with('tags');

		if ( request()->has('custom_filters') ) {

			$custom_filters = request()->get('custom_filters');

			$processed_filters = [];

			foreach ( $custom_filters as $filter ) {

				list($key, $compare, $value, $attribute_id) = explode(':', $filter);

				if ( !isset($processed_filters[$key]) ) {
					$processed_filters[$key] = [];
				}

				$processed_filters[$key][] = [
					'key' => $key,
					'value' => $value,
					'compare' => $compare,
					'attribute_id' => $attribute_id
				];

			}

			foreach( $processed_filters as $filters ) {

				$data->where(function($query) use ($filters) {

					$index_filter = 0;

					foreach( $filters as $filter ) {

						$index_filter++;

						extract($filter);

						if ( $index_filter == 1 ) {
							$wherehas_method = 'whereHas';
							$where_method = 'where';
						} else {
							$wherehas_method = 'orWhereHas';
							$where_method = 'orWhere';
						}

						if ( $key == 'serialized_attributes' ) {

							$query->{$where_method}('serialized_attributes', $compare, $value);

						} elseif ( $key == 'persist_tags' ) {

							$query->{$where_method}('persist_tags', $compare, $value);

						} else {

							$query->{$wherehas_method}('values', function($q) use ($compare, $value, $attribute_id) {
								$q->where('value', $compare, $value)
									->where('attribute_id', '=', $attribute_id);
							});

						}

					}

				});

			}

		}

		if ( request()->has('download') ) {
			$columns = [
				'id',
				'first_name',
				'last_name',
				'email',
				'created_at',
				'updated_at',

				'persist_tags',
				'persist_completeness',
				'persist_position',
				'persist_gender',
				'persist_city',
				'persist_postcode',
				'persist_street',
				'persist_website',
				'persist_last_email',
				'persist_office_phone',
				'persist_founding_year',
				'persist_brand_name',
				'persist_number_of_employees',
				'type',
			];
		} else {
			$columns = [
				'id',
				'first_name',
				'last_name',
				'email',
				'created_at',
				'updated_at',

				'persist_tags',
				'persist_completeness',
				'persist_position',
				'persist_gender',
				'persist_city',
				'persist_postcode',
				'persist_street',
				'persist_website',
				'persist_last_email',
				'persist_office_phone',
				'persist_founding_year',
				'persist_brand_name',
				'persist_number_of_employees',
			];
		}

		

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.sleighdogs.profile.individuals.edit', $element->id);

			// @todo - optimizing this can improve effectiveness
			if ( !request()->has('download') ) {
				$element->completeness = round( \Sleighdogs\Profile\Widgets\Stats::getProfilePercentage($element) );
			}

			if ( !request()->has('download') ) {
				$element->last_email = $element->getLastEmail();
			}

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new individual.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new individual.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating individual.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating individual.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified individual.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->individuals->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("sleighdogs/profile::individuals/message.{$type}.delete")
		);

		return redirect()->route('admin.sleighdogs.profile.individuals.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			switch( $action ) {

				case 'static_mail':

					$maillist = new \Sleighdogs\Maillist\Models\Maillist;

					if ( !request()->has('label') ) {
						$maillist->label = 'Static ' . \Carbon\Carbon::now()->format('j.n.Y H:i:s');
					} else {
						$maillist->label = request()->get('label');
					}

					$maillist->save();

					foreach (request()->input('rows', []) as $row)
					{
						$maillist->users()->attach($row);
					}

					$maillist->save();

					return response('Success');

				break;

				case 'dynamic_mail':

					$maillist = new \Sleighdogs\Maillist\Models\Maillist;

					if ( !request()->has('label') ) {
						$maillist->label = 'Static ' . \Carbon\Carbon::now()->format('j.n.Y H:i:s');
					} else {
						$maillist->label = request()->get('label');
					}

					if ( request()->has('filters') ) {
						$maillist->filters = serialize(request()->get('filters'));
					}

					if ( request()->has('custom_filters') ) {
						$maillist->custom_filters = serialize(request()->get('custom_filters'));
					}
					
					$maillist->save();

					return response('Success');

				break;

				case 'remind_percent':

					$users = app('platform.users');

					foreach (request()->input('rows', []) as $row)
					{
						$user = $users->find($row);
					}

					$object = new \stdClass;
					$object->user = $user;

					\Event::fire('yori.profile.percentage', ['object' => $object]);

					return response('Success');

				break;

				case 'set_tag':

					foreach (request()->input('rows', []) as $row)
					{
						$user = app('platform.users')->find($row);
					
						if ( request()->has('value') ) 
						{
							$value = request()->get('value');
							$user->tag($value);
						}
					}

					return response('Success');

				break;

			}

			foreach (request()->input('rows', []) as $row)
			{
				$this->individuals->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a individual identifier?
		if (isset($id))
		{
			if ( ! $individual = $this->individuals->find($id))
			{
				$this->alerts->error(trans('sleighdogs/profile::individuals/message.not_found', compact('id')));

				return redirect()->route('admin.sleighdogs.profile.individuals.all');
			}
		}
		else
		{
			$individual = $this->individuals->createModel();
		}

		if ( $individual->id ) {
			$individual = \Sleighdogs\Profile\Models\Individual::find($individual->id);
		} else {
			$individual = new \Sleighdogs\Profile\Models\Individual;
		}

		// Show the page
		return view('sleighdogs/profile::individuals.form', compact('mode', 'individual'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = request()->all();

		$input['type'] = 'individual';

		// Store the individual
		list($messages, $individual) = $this->individuals->store($id, $input);

		$tags = app('platform.tags');
		$tags->set($individual, request()->get('tags'));

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			$this->alerts->success(trans("sleighdogs/profile::individuals/message.success.{$mode}"));

			return redirect()->route('admin.sleighdogs.profile.individuals.edit', [$id]);
			return redirect()->route('admin.sleighdogs.profile.individuals.all');
		}

		$this->alerts->error($messages, 'form');

		//return redirect()->route('admin.sleighdogs.profile.individuals.edit', [$id]);
		return redirect()->back()->withInput();
	}

	/**
	 * Old filtering strategy
	if ( request()->has('custom_filters') ) {

			$custom_filters = request()->get('custom_filters');

			$key_index = [];

			foreach ( $custom_filters as $filter ) {

				list($key, $compare, $value) = explode(':', $filter);

				if ( !isset($key_index[$key]) ) {
					$key_index[$key] = 1;
				} else {
					 $key_index[$key]++;
				}

				if ( $key_index[$key] == 1 ) {
					$where_method = 'whereHas';
				} else {
					$where_method = 'orWhereHas';
				}

				$data->{$where_method}('values', function($q) use ($compare, $value) {
					$q->where('value', $compare, $value);
				});

			}

		}
	 */

}
