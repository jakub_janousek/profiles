<?php

/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    3.1.2
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Sleighdogs\Profile\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Platform\Users\Repositories\UserRepositoryInterface;

class ProfileController extends AdminController
{
    /**
     * The Users repository.
     *
     * @var \Platform\Users\Repositories\UserRepositoryInterface
     */
    protected $users;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];

    /**
     * Constructor.
     *
     * @param  \Platform\Users\Repositories\UserRepositoryInterface  $users
     * @return void
     */
    public function __construct(UserRepositoryInterface $users)
    {
        parent::__construct();

        $this->users = $users;
    }

    /**
     * Display a listing of users.
     *
     * @return \Illuminate\View\View
     */
    public function corporate()
    {
        return view('platform/users::index');
    }

    /**
     * Datasource for the users Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid()
    {
        $columns = [
            'id',
            'email',
            'first_name',
            'last_name',
            'created_at',
        ];

        $settings = [
            'sort'      => 'created_at',
            'direction' => 'desc',
            'pdf_view'  => 'pdf',
        ];

        $transformer = function ($element) {
            $element->edit_uri = route('admin.user.edit', $element->id);

            return $element;
        };

        return datagrid($this->users->grid(), $columns, $settings, $transformer);
    }

}
