<?php namespace Sleighdogs\Profile\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;

class ProfilesController extends Controller {

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sleighdogs/profile::index');
	}

}
