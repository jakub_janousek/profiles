<?php namespace Sleighdogs\Profile\Controllers\Frontend;

use Platform\Users\Controllers\Frontend\UsersController as BaseUsersController;
use Sentinel;
use Sleighdogs\Profile\Models\User;
use Sleighdogs\Profile\Models\Corporate;

class UsersController extends BaseUsersController {

    /**
     * Handle posting of the form for logging the user in.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processLogin()
    {
        // Register the user
        list($messages, $user) = $this->users->auth()->login(request()->all());

        // Do we have any errors?
        if (! $messages) {
            $this->alerts->success(
                trans('platform/users::auth/message.success.login')
            );

            switch ( $user->type ) {

                case 'individual':
                    return redirect()->route('sleighdogs.profile.profile');
                break;

                case 'corporate':
                    return redirect()->route('sleighdogs.profile.profile');
                break;

                case 'internal':
                default:
                    if ( Sentinel::inRole('admin') || Sentinel::inRole('editor') ) {
                        return redirect()->to(admin_uri());
                    }
                break;

            }

            return redirect()->intended('/');
        }

        $this->alerts->error($messages);

        return redirect()->back();
    }

    /**
     * Handle posting of the form for the user registration.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processRegistration()
    {
        // Store the user
        list($messages, $user) = $this->users->auth()->register(request()->except('root'));

        // Do we have any errors?
        if ($messages->isEmpty()) {
            if ( is_object($user) ) {
                if ( request()->has('root') ) 
                {
                    $position = request()->get('position');
                    $corporate_id = request()->get('root');

                    // ID is selected
                    if ( is_numeric( $corporate_id ) ) {

                        $corporate = \Sleighdogs\Profile\Models\Corporate::find($corporate_id);

                    
                    } else {

                        $password = \generateRandomString(10);

                        $corporate = \Sleighdogs\Profile\Models\Corporate::create([
                            'type'                  => 'corporate',
                            'email'                 => str_slug($corporate_id) . '@yori',    // temporary email
                            'password'              => $password,
                            'password_confirmation' => $password,
                        ]);

                        $corporate->save();

                        $corporate = \Sleighdogs\Profile\Models\Corporate::find($corporate->id);

                        $corporate->email = $corporate->id . '@yori';
                        $corporate->brand_name = $corporate_id;

                        $corporate->save();

                    }

                    $prepared_linked = [];

                    $prepared_linked[$user->id] = [
                        'allowed_to_change' => 0,
                        'position' => $position
                    ];

                    $corporate->individuals()->sync($prepared_linked, false);

                    $corporate->save();
                }
            }
            if ( $user->type == 'corporate' ) {
                $corporate = \Sleighdogs\Profile\Models\Corporate::find($user->id);

                $corporate->number_of_employees = request()->get('number_of_employees');
                $corporate->brand_name = request()->get('brand_name');
                $corporate->street = request()->get('street');
                $corporate->postcode = request()->get('postcode');
                $corporate->city = request()->get('city');

                $corporate->save();

            }
            return redirect('register')->with('registration-complete', $user);
        }

        $this->alerts->error($messages, 'form');

        return redirect()->back()->withInput();
    }

}
