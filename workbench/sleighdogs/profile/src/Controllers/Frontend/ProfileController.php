<?php namespace Sleighdogs\Profile\Controllers\Frontend;

use Platform\Users\Controllers\Frontend\ProfileController as BaseProfileController;
use Sentinel;
use Sleighdogs\Profile\Models\User;
use Sleighdogs\Profile\Models\Corporate;

class ProfileController extends BaseProfileController {

	/**
     * Show the form for the user profile.
     *
     * @return \Illuminate\View\View
     */
    public function profile($id = null)
    {
    	if ( !$id ) {
    		$currentUser = Sentinel::getUser();
    		$id = $currentUser->id;
    	}

    	$user = User::find($id);

        // @todo - make facade for deciding corporate and individual user
        if ( $user->type == 'corporate' ) {
            $user = Corporate::find($id);
        }

        /*
        Notification alerts test
        
        $this->alerts->success(
                trans('platform/users::auth/message.success.login')
            );

        $this->alerts->error(
                trans('platform/users::auth/message.success.login')
            );

        $this->alerts->info(
                trans('platform/users::auth/message.success.login')
            );

        $this->alerts->warning(
                trans('platform/users::auth/message.success.login')
            );

        $this->alerts->default(
                trans('platform/users::auth/message.success.login')
            );
        */
       
        return view('sleighdogs/profile::auth.profile', compact('user'));
    }

    public function embed($id = null)
    {
        if ( !$id )
            return null;

        $user = User::find($id);

        // @todo - make facade for deciding corporate and individual user
        if ( $user->type == 'corporate' ) {
            $user = Corporate::find($id);
        }

        return view('sleighdogs/profile::embed', compact('user'));
    }

}
