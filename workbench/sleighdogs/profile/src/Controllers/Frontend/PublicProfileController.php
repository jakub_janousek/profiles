<?php namespace Sleighdogs\Profile\Controllers\Frontend;

use Sentinel;
use Sleighdogs\Profile\Models\User;
use Sleighdogs\Profile\Models\Corporate;
use Platform\Foundation\Controllers\Controller;

class PublicProfileController extends Controller {

	/**
     * Show the form for the user profile.
     *
     * @return \Illuminate\View\View
     */
    public function profile($id = null)
    {
    	if ( !$id ) {
    		$currentUser = Sentinel::getUser();
    		$id = $currentUser->id;
    	}

    	$user = User::find($id);

        // Redirect to new slug routes
        if ( $user->slug ) {
            return redirect()->route('sleighdogs.profile.view.slug', ['slug' => $user->slug]);
        }

        // @todo - make facade for deciding corporate and individual user
        if ( $user->type == 'corporate' ) {
            $user = Corporate::find($id);
        }

        return view('sleighdogs/profile::auth.profile', compact('user'));
    }

    public function embed($id = null)
    {
        if ( !$id )
            return null;

        $user = User::find($id);

        // @todo - make facade for deciding corporate and individual user
        if ( $user->type == 'corporate' ) {
            $user = Corporate::find($id);
        }

        return view('sleighdogs/profile::embed', compact('user'));
    }

    public function profileBySlug($slug = null)
    {
        if ( !$slug ) {
            $currentUser = Sentinel::getUser();
            $slug = $currentUser->slug;
        }

        $user = User::whereSlug($slug)->first();
        
        // @todo - make facade for deciding corporate and individual user
        if ( $user->type == 'corporate' ) {
            $user = Corporate::whereSlug($slug)->first();
        }

        return view('sleighdogs/profile::auth.profile', compact('user'));
    }

}
