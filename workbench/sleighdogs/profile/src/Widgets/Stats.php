<?php namespace Sleighdogs\Profile\Widgets;

class Stats {

	public function completeness($user = null, $color = '#FCB03C', $trailColor = '#eee', $easing = 'bounce', $percentage = null)
	{

		if ( !is_object($user) )
			return null;

		$percentage = self::getProfilePercentage($user);

		return view('sleighdogs/profile::widgets/completeness', compact('color', 'percentage', 'trailColor', 'easing'));

	}

	public static function getProfilePercentage($user = null, $percentage = null)
	{
		$entity_namespace = $user->getEntityNamespace();

		$attributes = app('platform.attributes');

		$scorable_attributes = $attributes->where('namespace', $entity_namespace)->get();

		$total_score = $scorable_attributes->sum('score');

		$current_score = 0;

		foreach( $scorable_attributes as $attribute ) {

			if ( $user->{$attribute->slug} ) {

				$current_score = $current_score + $attribute->score;

			}

		}

		if ( is_null($percentage) )
			$percentage = ($current_score / $total_score) * 100;

		return $percentage;
	}

}