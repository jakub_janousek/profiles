<?php namespace Sleighdogs\Profile\Providers;

use Cartalyst\Support\ServiceProvider;

class ProfileServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Sleighdogs\Profile\Models\Profile']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('sleighdogs.profile.profile.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('sleighdogs.profile.profile', 'Sleighdogs\Profile\Repositories\Profile\ProfileRepository');

		// Register the data handler
		$this->bindIf('sleighdogs.profile.profile.handler.data', 'Sleighdogs\Profile\Handlers\Profile\ProfileDataHandler');

		// Register the event handler
		$this->bindIf('sleighdogs.profile.profile.handler.event', 'Sleighdogs\Profile\Handlers\Profile\ProfileEventHandler');

		// Register the validator
		$this->bindIf('sleighdogs.profile.profile.validator', 'Sleighdogs\Profile\Validator\Profile\ProfileValidator');
	}

}
