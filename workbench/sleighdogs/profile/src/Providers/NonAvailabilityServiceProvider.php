<?php namespace Sleighdogs\Profile\Providers;

use Cartalyst\Support\ServiceProvider;

class NonAvailabilityServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('sleighdogs.nonavailabilities.nonavailabilities', 'Sleighdogs\Profile\Repositories\NonAvailabilities\NonAvailabilitiesRepository');

		// Register the data handler
		$this->bindIf('sleighdogs.profile.nonavailabilities.handler.data', 'Sleighdogs\Profile\Handlers\NonAvailabilities\NonAvailabilitiesDataHandler');
//
//		// Register the event handler
//		$this->bindIf('sleighdogs.profile.profile.handler.event', 'Sleighdogs\Profile\Handlers\Profile\ProfileEventHandler');
//
		// Register the validator
		$this->bindIf('sleighdogs.profile.nonavailabilities.validator', 'Sleighdogs\Profile\Validator\NonAvailabilities\NonAvailabilitiesValidator');
	}

}
