<script type="text/template" data-grid="individual" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td><a href="<%= r.edit_uri %>" data-edit-uri="<%= r.edit_uri %>"><%= r.last_name %> <%= r.first_name %></a></td>
			<td><%= r.email %></td>
			<td>
				<% if ( r.gender == 'male' ) { %>
					<i class="fa fa-mars"></i>
				<% } else if ( r.gender == 'female' ) { %>
					<i class="fa fa-venus"></i>
				<% } %>
			</td>
			<td>
				<% for ( var key in r.values ) { %>
					<% var value = r.values[key]; %>
					<% if ( value.attribute.slug == 'position' ) { %>
						<% for ( var subkey in value.attribute.options ) { %>
							<% if ( subkey == r.position ) { %>
								<%= value.attribute.options[subkey] %>
							<% } %>
						<% } %>
					<% } %>
				<% } %>
			</td>
			<td><%= r.office_phone %></td>
			<td><%= r.completeness %>%</td>
			<td class="hidden-xs">
				<small>
				<% _.each(r.tags, function(tag) { %>
					<span class="label label-default"><%= tag.name %></span>
				<% }); %>
				</small>
			</td>
			<td>
				<% if ( typeof r.last_email !== 'undefined' ) { %>
					<% if ( r.last_email != null ) { %>
						<%= r.last_email.event %><br>
						<%= r.last_email.created_at %>
					<% } %>
				<% } %>
			</td>
			<td><%= r.created_at %></td>
			<td><%= r.updated_at %></td>
		</tr>

	<% }); %>

</script>
