<script type="text/template" data-grid="all" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td><a href="<%= r.edit_uri %>"><%= r.name %></a></td>
			<td class="hidden-xs">
				<small>
				<% _.each(r.tags, function(tag) { %>
					<span class="label label-default"><%= tag.name %></span>
				<% }); %>
				</small>
			</td>
			<td><%= r.persist_completeness %>%</td>
			<td><%= r.created_at %></td>
			<td><%= r.updated_at %></td>
		</tr>

	<% }); %>

</script>
