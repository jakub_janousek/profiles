@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('sleighdogs/profile::profiles/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

    {{-- Form --}}
    <form id="profile-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.sleighdogs.profile.profiles.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>

                        <span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $profile->exists ? $profile->id : null }}}</small></span>
                    </div>

                    {{-- Form: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-right">

                            @if ($profile->exists)
                            <li>
                                <a href="{{ route('admin.sleighdogs.profile.profiles.delete', $profile->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
                                    <i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
                                </a>
                            </li>
                            @endif

                            <li>
                                <button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
                                    <i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
                                </button>
                            </li>

                        </ul>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            <div role="tabpanel">

                {{-- Form: Tabs --}}
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('sleighdogs/profile::profiles/common.tabs.general') }}}</a></li>
                    <li role="presentation"><a href="#attributes" aria-controls="attributes" role="tab" data-toggle="tab">{{{ trans('sleighdogs/profile::profiles/common.tabs.attributes') }}}</a></li>
                </ul>

                <div class="tab-content">

                    {{-- Tab: General --}}
                    <div role="tabpanel" class="tab-pane fade in active" id="general-tab">

                        <fieldset>

                            <div class="row">

                                <div class="form-group{{ Alert::onForm('firstname', ' has-error') }}">

                                    <label for="firstname" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.firstname_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.firstname') }}}
                                    </label>

                                    <input type="text" class="form-control" name="firstname" id="firstname" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.firstname') }}}" value="{{{ input()->old('firstname', $profile->firstname) }}}">

                                    <span class="help-block">{{{ Alert::onForm('firstname') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('lastname', ' has-error') }}">

                                    <label for="lastname" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.lastname_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.lastname') }}}
                                    </label>

                                    <input type="text" class="form-control" name="lastname" id="lastname" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.lastname') }}}" value="{{{ input()->old('lastname', $profile->lastname) }}}">

                                    <span class="help-block">{{{ Alert::onForm('lastname') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('salutation', ' has-error') }}">

                                    <label for="salutation" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.salutation_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.salutation') }}}
                                    </label>

                                    <select class="form-control" name="salutation" id="salutation" >
                                        <option value="">{{{ trans('sleighdogs/profile::profiles/model.general.salutation') }}}</option>
                                        @foreach ($salutations as $id=>$sal)
                                        <option value="{{{ $id }}}" @if(input()->old('salutation', $profile->salutation) == $id) selected @endif>{{{ $sal }}}</option>
                                        
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{{ Alert::onForm('salutation') }}}</span>

                                </div>
                                
                                <div class="form-group{{ Alert::onForm('gender', ' has-error') }}">

                                    <label for="gender" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.gender_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.gender') }}}
                                    </label>

                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="gender" id="male" @if($profile->gender) checked @endif value="1"> {{ ucfirst('male') }}
                                            <input type="radio" name="gender" id="female" @if(!$profile->gender) checked @endif value="0"> {{ ucfirst('female') }}
                                        </label>
                                    </div>

                                    <span class="help-block">{{{ Alert::onForm('gender') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('portrait', ' has-error') }}">

                                    <label for="portrait" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.portrait_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.portrait') }}}
                                    </label>

                                    <input type="text" class="form-control" name="portrait" id="portrait" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.portrait') }}}" value="{{{ input()->old('portrait', $profile->portrait) }}}">

                                    <span class="help-block">{{{ Alert::onForm('portrait') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                                    <label for="email" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.email_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.email') }}}
                                    </label>

                                    <input type="text" class="form-control" name="email" id="email" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.email') }}}" value="{{{ input()->old('email', $profile->email) }}}">

                                    <span class="help-block">{{{ Alert::onForm('email') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('password', ' has-error') }}">

                                    <label for="password" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.password_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.password') }}}
                                    </label>

                                    <input type="password" class="form-control" name="password" id="password" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.password') }}}" value="{{{ input()->old('password', $profile->password) }}}">

                                    <span class="help-block">{{{ Alert::onForm('password') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                                    <label for="phone" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.phone_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.phone') }}}
                                    </label>

                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.phone') }}}" value="{{{ input()->old('phone', $profile->phone) }}}">

                                    <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('category', ' has-error') }}">

                                    <label for="category" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.category_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.category') }}}
                                    </label>

                                    <select class="form-control" name="category" id="category" >
                                        <option value="">{{{ trans('sleighdogs/profile::profiles/model.general.category') }}}</option>
                                        @foreach ($categories as $id => $cat)
                                        <option value="{{{ $id }}}" @if(input()->old('category', $profile->category) == $id) selected @endif>{{{ $cat }}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{{ Alert::onForm('category') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('education', ' has-error') }}">

                                    <label for="education" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.education_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.education') }}}
                                    </label>

                                    <textarea class="form-control" name="education" id="education" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.education') }}}">{{{ input()->old('education', $profile->education) }}}</textarea>

                                    <span class="help-block">{{{ Alert::onForm('education') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('degree', ' has-error') }}">

                                    <label for="degree" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.degree_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.degree') }}}
                                    </label>

                                    
                                    <select class="form-control" name="degree" id="degree" >
                                        <option value="">{{{ trans('sleighdogs/profile::profiles/model.general.degree') }}}</option>
                                        @foreach ($degrees as $id => $deg)
                                        <option value="{{{ $id }}}" @if(input()->old('degree', $profile->degree) == $id) selected @endif>{{{ $deg }}}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('degree') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('discipline', ' has-error') }}">

                                    <label for="discipline" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.discipline_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.discipline') }}}
                                    </label>

                                    <select class="form-control" name="discipline" id="discipline" >
                                        <option value="">{{{ trans('sleighdogs/profile::profiles/model.general.discipline') }}}</option>
                                        @foreach ($disciplines as $id => $dis)
                                        <option value="{{{ $id }}}" @if(input()->old('discipline', $profile->discipline) == $id) selected @endif>{{{ $dis }}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{{ Alert::onForm('discipline') }}}</span>

                                </div>
                                
                                <div class="form-group{{ Alert::onForm('qualifications', ' has-error') }}">

                                    <label for="qualifications" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.qualifications_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.qualifications') }}}
                                    </label>

                                    <select class="form-control" name="qualifications[]" id="qualifications" multiple >
                                        @foreach ($qualifications as $id => $qul)
                                        <option value="{{{ $id }}}" @if(is_array(input()->old('qualifications', $selectedQualifications)) && (in_array($id, $selectedQualifications)||in_array($id, input()->old('qualifications', $selectedQualifications)) )) selected @endif>{{{ $qul }}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{{ Alert::onForm('qualifications') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('company', ' has-error') }}">

                                    <label for="company" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::profiles/model.general.company_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::profiles/model.general.company') }}}
                                    </label>

                                    <input type="text" class="form-control" name="company" id="company" placeholder="{{{ trans('sleighdogs/profile::profiles/model.general.company') }}}" value="{{{ input()->old('company', $profile->company) }}}">

                                    <span class="help-block">{{{ Alert::onForm('company') }}}</span>

                                </div>


                            </div>

                        </fieldset>

                    </div>

                    {{-- Tab: Attributes --}}
                    <div role="tabpanel" class="tab-pane fade" id="attributes">
                        @attributes($profile)
                    </div>

                </div>

            </div>

        </div>

    </form>

</section>
@stop
