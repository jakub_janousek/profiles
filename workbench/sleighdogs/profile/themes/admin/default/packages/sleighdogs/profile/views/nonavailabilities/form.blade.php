@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('sleighdogs/profile::profiles/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

    {{-- Form --}}
    <form id="booking-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.sleighdogs.nonavailabilities.nonavailabilities.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>

                        <span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $nonavailabilities->exists ? $nonavailabilities->id : null }}}</small></span>
                    </div>

                    {{-- Form: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-right">

                            @if ($nonavailabilities->exists)
                            <li>
                                <a href="{{ route('admin.sleighdogs.nonavailabilities.nonavailabilities.delete', $nonavailabilities->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
                                    <i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
                                </a>
                            </li>
                            @endif

                            <li>
                                <button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
                                    <i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
                                </button>
                            </li>

                        </ul>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            <div role="tabpanel">

                {{-- Form: Tabs --}}
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('sleighdogs/profile::profiles/common.tabs.general') }}}</a></li>
                    <li role="presentation"><a href="#attributes" aria-controls="attributes" role="tab" data-toggle="tab">{{{ trans('sleighdogs/profile::profiles/common.tabs.attributes') }}}</a></li>
                </ul>

                <div class="tab-content">

                    {{-- Tab: General --}}
                    <div role="tabpanel" class="tab-pane fade in active" id="general-tab">

                        <fieldset>

                            <div class="row">
                                
                                <div class="form-group{{ Alert::onForm('profile_id', ' has-error') }}">

                                    <label for="profile_id" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.profile_id_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::nonavailabilities/model.general.profile_id') }}}
                                    </label>

                                    <select class="form-control" name="profile_id" id="profile_id" >
                                        <option value="">{{{ trans('sleighdogs/profile::nonavailabilities/model.general.profile_id') }}}</option>
                                        @foreach ($profiles as $id=>$nam)
                                        <option value="{{{ $id }}}" @if(input()->old('profile_id', $nonavailabilities->profile_id) == $id) selected @endif>{{{ $nam }}}</option>
                                        
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{{ Alert::onForm('profile_id') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('date_start', ' has-error') }}">

                                    <label for="date_start" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.date_start_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::nonavailabilities/model.general.date_start') }}}
                                    </label>

                                    <input type="text" class="form-control" name="date_start" id="date_start" placeholder="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.date_start') }}}" value="{{{ input()->old('date_start', $nonavailabilities->date_start) }}}">

                                    <span class="help-block">{{{ Alert::onForm('date_start') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('date_end', ' has-error') }}">

                                    <label for="date_end" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.date_end_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::nonavailabilities/model.general.date_end') }}}
                                    </label>

                                    <input type="text" class="form-control" name="date_end" id="date_end" placeholder="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.date_end') }}}" value="{{{ input()->old('date_end', $nonavailabilities->date_end) }}}">

                                    <span class="help-block">{{{ Alert::onForm('date_end') }}}</span>

                                </div>
                                
                                <div class="form-group{{ Alert::onForm('notes', ' has-error') }}">

                                    <label for="notes" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.notes_help') }}}"></i>
                                        {{{ trans('sleighdogs/profile::nonavailabilities/model.general.notes') }}}
                                    </label>

                                    <textarea class="form-control" name="notes" id="notes" placeholder="{{{ trans('sleighdogs/profile::nonavailabilities/model.general.notes') }}}">{{{ input()->old('notes', $nonavailabilities->notes) }}}</textarea>

                                    <span class="help-block">{{{ Alert::onForm('notes') }}}</span>

                                </div>

                            </div>

                        </fieldset>

                    </div>

                    {{-- Tab: Attributes --}}
                    <div role="tabpanel" class="tab-pane fade" id="attributes">
                        @attributes($nonavailabilities)
                    </div>

                </div>

            </div>

        </div>

    </form>

</section>
@stop
