<script type="text/template" data-grid="nonavailabilities" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td><a href="<%= r.edit_uri %>" href="<%= r.edit_uri %>"><%= r.id %></a></td>
			<td><%= r.profile_id %></td>
			<td><%= r.date_start %></td>
			<td><%= r.date_end %></td>
                        <td><%= r.notes %></td>

		</tr>

	<% }); %>

</script>
