@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{ trans('sleighdogs/profile::corporates/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bootstrap/css/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'moment/js/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'underscore/js/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'sleighdogs/profile::corporates/js/index.js', 'platform') }}
{{ Asset::queue('bootstrap-daterange', 'bootstrap/js/daterangepicker.js', 'jquery') }}
{{ Asset::queue('jscookie', 'sleighdogs/profile::jscookie.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
<script type="text/javascript">

var CustomFilters = {

	delimiter: '/',

	buildUri: function(filters) {

		var stringUri = '';

		for ( var key in filters ) {

			stringUri += this.delimiter + filters[key];

		}

		return stringUri;

	},

	changeUri: function(filters) {

		var url = this.buildUri(filters),
			$grid = $('#data-grid'),
			source = $grid.data('source'),
			route = window.location.protocol+'//'+window.location.host+window.location.pathname;

		window.location = route + "#" + url;

	}

};

window.custom_filters = [];

$(function(){

	var cookieFilters = Cookies.getJSON('custom_filters_corporates');

	if ( typeof cookieFilters !== 'undefined' ) {

		window.custom_filters = cookieFilters;

		$(window).trigger('custom_filters');

		for ( var key in cookieFilters ) {

			var current_filter = cookieFilters[key];

			if ( typeof current_filter != 'undefined' ) {

				if ( current_filter != '' ) {

					$('[data-custom-filter="'+current_filter+'"]').attr('checked', 'checked');
				}

			}
		}
		
	}

	$('[data-custom-filter]').change(function(event){

		window.custom_filters = [];

		$('[data-custom-filter]:checked').each(function(){

			window.custom_filters.push($(this).data('custom-filter'));

		});

		$(window).trigger('custom_filters');

		CustomFilters.changeUri(window.custom_filters);

		// Remember filters for 7 days
		Cookies.set('custom_filters_corporates', window.custom_filters, { expires: 7 });

	});

	$(window).on('hashchange', function()
	{
		
	});

	
	if ( window.location.hash ) {

		var hashroute = window.location.hash.replace('#', ''),
			hashroute_array = hashroute.split('/');

		for ( var key in hashroute_array ) {

			var current_filter = hashroute_array[key];

			if ( typeof current_filter != 'undefined' ) {

				if ( current_filter != '' ) {

					$('[data-custom-filter="'+current_filter+'"]').attr('checked', 'checked');
				}

			}
		}

		$('[data-custom-filter]:first').trigger('change');
	}

});
</script>
@stop

{{-- Inline styles --}}
@section('styles')
@parent
<style type="text/css">
.nav-tabs.nav-tabs-left {
	width: 25%;
}
.nav-tabs.nav-tabs-left ~ .tab-content {
	width: 75%;
}

.table-responsive .table > thead > tr > th {
	width: 120px;
	padding-left: 0;
	padding-right: 2px;
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
}
.table-responsive .table > thead > tr > th.imgcell {
	width: 60px;
}
.table-responsive .table > thead > tr > th.checkboxcell {
	width: 35px;
}
#data-grid thead th.sortable:before {
	top: 6px;
}
.table-responsive .table > tbody > tr > td {
	font-size: 11px;
	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 0;
	padding-right: 2px;
}
</style>
@stop

{{-- Page content --}}
@section('page')

{{-- Grid --}}
<section class="panel panel-default panel-grid">

	{{-- Grid: Header --}}
	<header class="panel-heading">

		<nav class="navbar navbar-default navbar-actions">

			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<span class="navbar-brand">{{{ trans('sleighdogs/profile::corporates/common.title') }}}</span>

				</div>

				{{-- Grid: Actions --}}
				<div class="collapse navbar-collapse" id="actions">

					<ul class="nav navbar-nav navbar-left">
						
						<li>
							<a data-grid="corporate" data-grid-bulk-action="static_mail" data-toggle="tooltip" data-original-title="Static mail list">
								<i class="fa fa-envelope-o"></i> <span class="visible-xs-inline">Static mail list</span>
							</a>
						</li>
						
						<li>
							<a data-grid="corporate" data-grid-filter-action="dynamic_mail" data-toggle="tooltip" data-original-title="Dynamic mail list">
								<i class="fa fa-filter"></i> <span class="visible-xs-inline">Dynamic mail list</span>
							</a>
						</li>

						<li>
							<a data-grid="corporate" data-grid-bulk-action="remind_percent" data-toggle="tooltip" data-original-title="Profile reminder">
								<i class="fa fa-bell"></i> <span class="visible-xs-inline">Profile reminder</span>
							</a>
						</li>

						<li class="danger disabled">
							<a data-grid-bulk-action="delete" data-toggle="tooltip" data-target="modal-confirm" data-original-title="{{{ trans('action.bulk.delete') }}}">
								<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.bulk.delete') }}}</span>
							</a>
						</li>

						@if ( ! empty($tags))
						<li class="dropdown" data-toggle="tooltip" data-original-title="Tag selected rows">

							<a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-tags"></i>
							</a>

							<ul class="dropdown-menu" role="tags">
								@foreach ($tags as $tag)
								<li><a href="#" data-grid-bulk-action="set_tag" data-grid-bulk-action-value="{{ $tag }}">{{{ $tag }}}</a></li>
								@endforeach
							</ul>

						</li>
						@endif

						<li class="dropdown">
							<a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button" aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
								<i class="fa fa-download"></i> <span class="visible-xs-inline">{{{ trans('action.export') }}}</span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a data-download="json"><i class="fa fa-file-code-o"></i> JSON</a></li>
								<li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
								<li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
							</ul>
						</li>

						<li class="primary">
							<a href="{{ route('admin.sleighdogs.profile.corporates.create') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
								<i class="fa fa-plus"></i> <span class="visible-xs-inline">{{{ trans('action.create') }}}</span>
							</a>
						</li>

					</ul>

					{{-- Grid: Filters --}}
					<form class="navbar-form navbar-right" method="post" accept-charset="utf-8" data-search data-grid="corporate" role="form">
						{{--
						@if ( ! empty($tags))
						<div class="btn-group">

							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-tags"></i> <span class="caret"></span>
							</button>

							<ul class="dropdown-menu" role="tags">
								@foreach ($tags as $tag)
								<li><a href="#" data-grid="main" data-filter="tags..name:{{{ $tag }}}" data-label="tags..name::{{{ $tag }}}">{{{ $tag }}}</a></li>
								@endforeach
							</ul>

						</div>
						@endif
						--}}

						<div class="input-group">

							<span class="input-group-btn">
								
								<!-- 
								<button class="btn btn-default" type="button" disabled>
									{{{ trans('common.filters') }}}
								</button>
								-->

								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>

								<ul class="dropdown-menu" role="menu">

									<li>
										<a data-grid-calendar-preset="day">
											<i class="fa fa-calendar"></i> {{{ trans('date.day') }}}
										</a>
									</li>

									<li>
										<a data-grid-calendar-preset="week">
											<i class="fa fa-calendar"></i> {{{ trans('date.week') }}}
										</a>
									</li>

									<li>
										<a data-grid-calendar-preset="month">
											<i class="fa fa-calendar"></i> {{{ trans('date.month') }}}
										</a>
									</li>

								</ul>

								<button class="btn btn-default hidden-xs" type="button" data-grid-calendar data-range-filter="created_at">
									<i class="fa fa-calendar"></i>
								</button>

							</span>

							<input class="form-control " name="filter" type="text" placeholder="{{{ trans('common.search') }}}">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit">
									<span class="fa fa-search"></span>
								</button>

								<button class="btn btn-default" data-grid="corporate" data-reset>
									<i class="fa fa-refresh fa-sm"></i>
								</button>

							</span>

						</div>

					</form>

				</div>

			</div>

		</nav>

	</header>

	<div class="panel-body">

		<div class="row">

			<div class="col-md-12">

				<?php

				$filters = [
					'tags' => [
						'comparison' => 'like',
					],
					'industry' => [
						'comparison' => '='
					],
					'number_of_employees' => [
						'comparison' => '='
					]
				];
				?>

				@include('sleighdogs/profile::filters')
 
			</div>

		</div>

		{{-- Grid: Applied Filters --}}
		<div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

			<div id="data-grid_applied" class="btn-group" data-grid="corporate"></div>

		</div>

	</div>

	{{-- Grid: Table --}}
	<div class="table-responsive">

		<table id="data-grid" class="table table-hover" data-source="{{ route('admin.sleighdogs.profile.corporates.grid') }}" data-grid="corporate" style="max-width:100%;table-layout: fixed;">
			<thead>
				<tr>
					<th class="checkboxcell"><input data-grid-checkbox="all" type="checkbox"></th>
					<th class="imgcell"></th>
					<th data-sort="persist_brand_name" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.brand_name') }}}</th>
					<th data-sort="persist_number_of_employees" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.number_of_employees') }}}</th>
					<th data-sort="persist_founding_year" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.founding_year') }}}</th>
					<th data-sort="email" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.general_email') }}}</th>
					<th data-sort="persist_website" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.website') }}}</th>
					<!-- <th>{{{ trans('sleighdogs/profile::corporates/model.general.addresses') }}}</th> -->
					<th data-sort="persist_street" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.street') }}}</th>
					<th data-sort="persist_postcode" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.postcode') }}}</th>
					<th data-sort="persist_city" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.city') }}}</th>
					<th class="col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.tags') }}} <i class="fa fa-tag"></i></th>
					<th data-sort="persist_completeness" class="sortable col-xs-1">{{{ trans('sleighdogs/profile::corporates/model.general.completeness') }}}</th>
					<th data-sort="persist_last_email" class="sortable">{{{ trans('sleighdogs/profile::corporates/model.general.last_email') }}}</th>
					<th class="sortable col-xs-1" data-sort="created_at">{{{ trans('sleighdogs/profile::corporates/model.general.created_at') }}}</th>
					<th class="sortable col-xs-1" data-sort="updated_at">{{{ trans('sleighdogs/profile::corporates/model.general.updated_at') }}}</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>

	</div>

	<footer class="panel-footer clearfix">

		{{-- Grid: Pagination --}}
		<div id="data-grid_pagination" data-grid="corporate"></div>

	</footer>

	{{-- Grid: templates --}}
	@include('sleighdogs/profile::corporates/grid/index/results')
	@include('sleighdogs/profile::corporates/grid/index/pagination')
	@include('sleighdogs/profile::corporates/grid/index/filters')
	@include('sleighdogs/profile::corporates/grid/index/no_results')

</section>

@if (config('platform.app.help'))
	@include('sleighdogs/profile::corporates/help')
@endif

@stop
