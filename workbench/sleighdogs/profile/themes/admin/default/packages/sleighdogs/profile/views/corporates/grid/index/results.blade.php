<script type="text/template" data-grid="corporate" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td class="id"><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td class="img">
				<a href="<%= r.edit_uri %>" style="display:block;" data-edit-uri="<%= r.edit_uri %>">
					<img src="<%= r.imgurl %>" width="50" height="50">
				</a>
			</td>
			<td class="brand_name"><a href="<%= r.edit_uri %>" data-edit-uri="<%= r.edit_uri %>"><%= r.brand_name %></a></td>
			<td class="number_of_employees"><%= r.number_of_employees %></td>
			<td class="founding_year"><%= r.founding_year %></td>
			<td class="general_email"><%= r.general_email %></td>
			<td class="website"><%= r.website %></td>
			<!-- <td class="">
				<% if ( typeof r.addresses != 'undefined' ) { %>
					<% for ( var key in r.addresses ) { %>
						<%= r.addresses[key] %>
					<% } %>
				<% } %>
			</td> -->
			<td class="street"><%= r.street %></td>
			<td class="postcode"><%= r.postcode %></td>
			<td class="city"><%= r.city %></td>
			<td class="hidden-xs tags">
				<small>
				<% _.each(r.tags, function(tag) { %>
					<span class="label label-default"><%= tag.name %></span>
				<% }); %>
				</small>
			</td>
			<td class="completeness"><%= r.completeness %>%</td>
			<td class="last_email">
				<span class="last-email-inner">
				<% if ( typeof r.last_email !== 'undefined' ) { %>
					<% if ( r.last_email != null ) { %>
						<%= r.last_email.event %><br>
						<%= r.last_email.created_at %>
					<% } %>
				<% } %>
				</span>
			</td>
			<td class="created_at"><%= r.created_at %></td>
			<td class="updated_at"><%= r.updated_at %></td>
		</tr>

	<% }); %>

</script>
