@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
 {{{ trans("action.{$mode}") }}} {{{ trans('platform/users::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('selectize', 'selectize/css/selectize.bootstrap3.css', 'styles') }}
{{ Asset::queue('redactor', 'redactor/css/redactor.css', 'styles') }}

{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}
{{ Asset::queue('redactor', 'redactor/js/redactor.min.js', 'jquery') }}
{{ Asset::queue('form', 'platform/users::js/form.js', 'platform') }}

{{-- Inline styles --}}
@section('styles')
@parent
<style type="text/css">
.btn.btn-primary.btn-lg {
	display: none;
}
</style>
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
<script type="text/javascript">

var select_html = '<select class="form-control" name="linked[__INDEX__][id]" data-parsley-trigger="change">'

@foreach ($linkable as $linkable_single)
	select_html += '<option value="{{ $linkable_single->id }}">{{ $linkable_single->first_name }} {{ $linkable_single->last_name }} ({{ $linkable_single->email }})</option>';
@endforeach

select_html += '</select>';


$(function(){
	$('[data-linked-add]').click(function(event){
		
		var last_index = $('[data-linked-contact]:last').data('linked-index');
			template = $('[data-linked-contact]:last')[0].outerHTML,
			new_index = (parseInt(last_index) + 1);

		$('[data-linked-contact]:last').after(template);

		$('[data-linked-contact]:last').data('linked-index', new_index);

		$('[data-linked-contact]:last .cell-select').html(select_html);

		$('[data-linked-contact]:last input, [data-linked-contact]:last select').each(function(){
			var name = $(this).attr('name');
			if ( typeof name != 'undefined' ) {
				var newname = name.replace(last_index, new_index);
				newname = newname.replace('__INDEX__', new_index);
				$(this).attr('name', newname);
				$(this).val('');
			}
		});

		$('[data-linked-contact]:last select').selectize();
	});
});
</script>
@stop

{{-- Page content --}}
@section('page')
<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="users-form" action="{{ request()->fullUrl() }}" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.sleighdogs.profile.corporates.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">
						{{{ trans("action.{$mode}") }}} <small>{{{ $corporate->brand_name }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($corporate->exists && $corporate->id !== $currentUser->id)
							<li>
								<a href="{{ route('admin.user.delete', $corporate->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>
							<li>
								<a href="{{ route('sleighdogs.profile.view', $corporate->id) }}">
									<i class="fa fa-eye"></i> <span class="visible-xs-inline">Preview</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('platform/users::common.tabs.general') }}}</a></li>
					<li role="presentation"><a href="#history-tab" aria-controls="history-tab" role="tab" data-toggle="tab">History</a></li>
					<li role="presentation"><a href="#tags-tab" aria-controls="tags-tab" role="tab" data-toggle="tab">Tags</a></li>
					<li role="presentation"><a href="#linked-tab" aria-controls="linked-tab" role="tab" data-toggle="tab">Linked contacts</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">


						<fieldset>

							<legend>{{{ trans('platform/users::model.authorization.legend') }}}</legend>

							<div class="row">

								<div class="col-md-6">

									{{-- Email --}}
									<div class="form-group{{ Alert::onForm('email', ' has-error') }}">

										<label for="email" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.general.email_help') }}}" required></i>
											{{{ trans('platform/users::model.general.email') }}}
										</label>

										<input type="email" class="form-control" name="email" id="email" placeholder="{{{ trans('platform/users::model.general.email') }}}" value="{{{ request()->old('email', $corporate->email) }}}" required data-parsley-trigger="change">

										<span class="help-block">{{{ Alert::onForm('email') }}}</span>

									</div>

								</div>

								<div class="col-md-6">

									{{-- Activation status --}}
									<div class="form-group{{ Alert::onForm('activated', ' has-error') }}">

										<label for="activated" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.authorization.activated_help') }}}"></i>
											{{{ trans('platform/users::model.authorization.activated') }}}
										</label>

										<select class="form-control" name="activated" id="activated" required>
											<option value="1"{{ request()->old('activated', $corporate->activated) == '1' ? ' selected="selected"' : null }}>{{ trans('common.yes') }}</option>
											<option value="0"{{ request()->old('activated', $corporate->activated) == '0' ? ' selected="selected"' : null }}>{{ trans('common.no') }}</option>
										</select>

										<span class="help-block">{{{ Alert::onForm('activated') }}}</span>

									</div>

								</div>

							</div>

						</fieldset>

						<fieldset>

							<legend>{{{ trans('platform/users::model.authentication.legend') }}}</legend>

							<div class="row">

								<div class="col-md-6">

									{{-- Password --}}
									<div class="form-group{{ Alert::onForm('password', ' has-error') }}">

										<label for="password" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans("platform/users::model.authentication.{$mode}.password_help") }}}"></i>
											{{{ trans("platform/users::model.authentication.{$mode}.password") }}}
										</label>

										<input type="password" class="form-control" name="password" id="password" placeholder="{{{ trans("platform/users::model.authentication.{$mode}.password") }}}"{{ ! $corporate->exists ? ' required' : null }} data-parsley-trigger="change" data-parsley-minlength="6">

										<span class="help-block">{{{ Alert::onForm('password') }}}</span>

									</div>

								</div>

								<div class="col-md-6">

									{{-- Password Confirmation --}}
									<div class="form-group{{ Alert::onForm('password_confirmation', ' has-error') }}">

										<label for="password_confirmation" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.authentication.password_confirmation_help') }}}"></i>
											{{{ trans("platform/users::model.authentication.password_confirmation") }}}
										</label>

										<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="{{{ trans("platform/users::model.authentication.password_confirmation") }}}"{{ ! $corporate->exists ? ' required' : null }} data-parsley-trigger="change" data-parsley-equalto="#password">

										<span class="help-block">{{{ Alert::onForm('password_confirmation') }}}</span>

									</div>

								</div>

							</div>

						</fieldset>

						<fieldset>

							@attributes($corporate, [
								'official_administrative_name',
								'brand_name',
								'founding_year',
								'number_of_employees',
								'industry',
								'fax',
								'website',
								'phone',
								'street',
								'postcode',
								'city'
							])
							
							@attributes($corporate, [
								'general_email',
								'company_linkedin_url',
								'company_facebook_url',
								'company_twitter_url',
								'company_custom_social',
								'short_description',
								'logo',
								'picture_gallery',
								'links_to_material',
								'video'
							])

						</fieldset>

					</div>

					{{-- Tab: History --}}
					<div role="tabpanel" class="tab-pane fade" id="history-tab">

						@if ( $corporate->email )
							@foreach(\Sanatorium\Mailer\Models\Maillog::where('receiver', $corporate->email)->get() as $entry)
								<div class="well">
									<?php
									$translation = trans('sanatorium/mailer::events.' .$entry->event);

									if ( $translation !== 'sanatorium/mailer::events.' .$entry->event ) {
										$display_event = $translation;
									} else {
										$display_event = $entry->event;
									}
									?>
									<small class="hint-text">
										{{ trans('sanatorium/mailer::status.'.$entry->status) }}
									</small>
									<h3>{{ $display_event }}</h3>
									{{ $entry->created_at->format('j.n.Y H:i:s') }}
								</div>
							@endforeach
						@endif

					</div>

					{{-- Tab: Tags --}}
					<div role="tabpanel" class="tab-pane fade" id="tags-tab">

						@tags($corporate, 'tags')

					</div>

					{{-- Tab: Tags --}}
					<div role="tabpanel" class="tab-pane fade" id="linked-tab">

						<div class="form-group">

							<table class="table">

								<thead>
									<th>Contact</th>
									<th>Capable</th>
									<th>Position</th>
									<th>Since</th>
									<th>Until</th>
								</thead>
								
								<?php $index = 0; ?>

								@foreach( $corporate->individuals()->where('type', 'individual')->get() as $linked )

								<?php $index++; ?>
								
								<tr data-linked-contact data-linked-index="{{ $index }}">
									<td class="cell-select">
										
										<select class="form-control" name="linked[{{ $index }}][id]" data-parsley-trigger="change">
											@foreach ($linkable as $linkable_single)
											<option value="{{ $linkable_single->id }}"{{ $linkable_single->id == $linked->id ? ' selected' : null }}>{{ $linkable_single->first_name }} {{ $linkable_single->last_name }} ({{ $linkable_single->email }})</option>
											@endforeach
										</select>

									</td>
									<td>

										<input type="checkbox" value="1" name="linked[{{ $index }}][allowed_to_change]" {{ $linked->pivot->allowed_to_change ? 'checked' : '' }}>
										
									</td>
									<td>
										
										<input type="text" name="linked[{{ $index }}][position]" value="{{ $linked->pivot->position }}" class="form-control">

									</td>
									<td>
										
										<input type="text" name="linked[{{ $index }}][started_at]" value="{{ $linked->pivot->started_at }}" class="form-control">

									</td>
									<td>
										
										<input type="text" name="linked[{{ $index }}][ended_at]" value="{{ $linked->pivot->ended_at }}" class="form-control">

									</td>
								</tr>

								@endforeach

								<tr>
									<td colspan="4">
										<span class="btn btn-primary" data-linked-add>
											<i class="fa fa-plus">
												
											</i>
										</span>
									</td>
								</tr>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
