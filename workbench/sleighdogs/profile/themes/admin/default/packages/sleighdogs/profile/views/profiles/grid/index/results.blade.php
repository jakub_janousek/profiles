<script type="text/template" data-grid="profile" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
                        <td><a class="book"><i class="fa fa-calendar book-icon" data-value="<%= r.id %>"></i></a></td>
			<td><a href="<%= r.edit_uri %>" href="<%= r.edit_uri %>"><%= r.id %></a></td>
			<td><%= r.firstname %></td>
			<td><%= r.lastname %></td>
			<td><%= r.salutation %></td>
			<td><%= r.gender %></td>
			<td><%= r.portrait %></td>
			<td><%= r.email %></td>
			<td><%= r.password %></td>
			<td><%= r.phone %></td>
			<td><%= r.category %></td>
			<td><%= r.education %></td>
			<td><%= r.degree %></td>
			<td><%= r.discipline %></td>
			<td><%= r.company %></td>
			<td><%= r.created_at %></td>
		</tr>

	<% }); %>

</script>
