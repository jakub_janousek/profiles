{{ Asset::queue('progressbar', 'sleighdogs/profile::progressbar/progressbar.js') }}

@section('styles')
@parent
<style type="text/css">
#profile-stats-completeness-container {
	transform: rotate(-90deg);
}
#profile-stats-completeness-container .progressbar-text {
	transform: rotate(90deg) translate(-50%, -50%)!important;
	transform-origin: 0 0 0;
	margin-left: 0.45em!important;
	   text-indent: -0.2em;
	font-size: 1.8em;
}
.progressbar-half-circle {
	height: 95px;
	overflow: hidden;
}
<?php echo '@media only screen and (max-width:1195px) {'; ?>
	.progressbar-half-circle {
		height: 78px;
		margin-top: 17px;
		overflow: hidden;
	}
}
</style>
@stop

<div id="profile-stats-completeness-container"></div>

@section('scripts')
@parent
<script type="text/javascript">
function drawCompletenessCircle() {
	window.circle = new ProgressBar.Circle('#profile-stats-completeness-container', {
	    color: '{{ $color }}',
	    strokeWidth: 20,
	    trailWidth: 20,
	    trailColor: '{{ $trailColor }}',
	    duration: 1500,
	    easing: '{{ $easing }}',
	    text: {
	        value: '0'
	    },
	    step: function(state, bar) {
	    	var value = (bar.value() * 200).toFixed(0);
	    	if ( value == '-0' ) {
	    		value = 0;
	    	}
	        bar.setText(value + '%');
	    }
	});

	window.circle.animate({{ $percentage / 200 }}, function() {
	    //circle.animate(0);
	});
}

drawCompletenessCircle();
</script>
@stop