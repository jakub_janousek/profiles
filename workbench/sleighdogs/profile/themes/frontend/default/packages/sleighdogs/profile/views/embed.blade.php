<!DOCTYPE html>
<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<style type="text/css">
			body {
				font-family: 'Open Sans', sans-serif;
				font-weight: 300;
			}
			.bg-primary {
				background-image: linear-gradient(141deg, #13bfdf 0%, #42afe3 71%, #53a1eb 100%);
				color: #fff;
			}
			h1 {
				margin: 0;
				font-weight: 100;
				padding-top: 0.5em;
				padding-bottom: 0.5em;
			}
			.text-center {
				text-align: center;
			}
			.preview-img {
				-webkit-box-shadow: 0 -1px 3px rgba(0,0,0,0.4);
				box-shadow: 0 -1px 3px rgba(0,0,0,0.4);
				display: block;
				width: 150px;
				height: 150px;
				border-radius: 4px;
				margin: 0 auto;
			}
			a {
				text-decoration: none!important;
			}
			.yori-link {
				color: #42afe3;
			}
			.overflow-hidden {
				overflow: hidden;
			}
			.m-b-30 {
				margin-bottom: 25px;
			}
			.text-sm {
				font-size: 14px;
			}
			p {
				height: 19px;
			}
		</style>
	</head>
	<body style="margin:0;padding:0;background-color:#fff">

		@if ( $user->type == 'internal' || $user->type == 'individual' )
			
			<a href="{{ route('sleighdogs.profile.view', [$user->id]) }}" class="wrapper-link" target="_blank">

				<div class="m-b-30 bg-primary text-center" style="height:200px;">
					
					<h1>{{ $user->first_name }} {{ $user->last_name }}</h1>
					
					<div class="text-center">
						<div class="preview-img">
							<img src="{{ $user->gravatar_url }}?s=150" class="img-preview" alt="~" style="max-width:100%;height:auto;">
						</div>
					</div>

				</div>
				
				<div class="overflow-hidden text-sm text-center" style="height:70px">
					
					{{-- @todo - display real select value --}}
					@if ( is_string($user->position) )
						<p>{{ str_replace('_', ' ', $user->position) }}</p>
					@else
						<p>{{ str_replace('_', ' ', $user->position[0]) }}</p>
					@endif

					<p class="yori-link">See on @setting('platform.app.title')</p>

				</div>

			</a>

		@else

			<a href="{{ route('sleighdogs.profile.view', [$user->id]) }}" class="wrapper-link" target="_blank">

				<div class="m-b-30 bg-primary text-center" style="height:200px;">
					
					<h1>{{ $user->brand_name }}</h1>
					
					@foreach( $user->mediaByTag('logo') as $medium )
					<div class="text-center">
						<div class="preview-img">
							<img src="{{ route('thumb.view', $medium->path) . '?w=150&h=150' }}" class="img-preview" alt="~" title="{{{ $medium->name }}}" style="max-width:100%;height:auto;">
						</div>
					</div>
					@endforeach

				</div>

				<div class="overflow-hidden text-sm text-center" style="height:70px">
					
					<p>Since {{ $user->founding_year }}</p>

					<p class="yori-link">See on @setting('platform.app.title')</p>

				</div>

			</a>

		@endif
	
	</body>
</html>