@extends('layouts/default')

{{-- Page title --}}
@section('title')
{{{ trans('platform/users::auth/form.profile.legend') }}} ::
@parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('magnific-popup', 'sleighdogs/profile::magnific-popup/jquery.magnific-popup.js', 'jquery') }}
{{ Asset::queue('magnific-popup', 'sleighdogs/profile::magnific-popup/magnific-popup.css') }}

{{ Asset::queue('sweetalert', 'sweetalert/sweetalert.min.js') }}
{{ Asset::queue('sweetalert', 'sweetalert/sweetalert.css') }}

{{-- Inline Scripts --}}
@section('scripts')
@parent
<script type="text/javascript">
$(document).ready(function() {
	$('.lightbox').magnificPopup({type:'image'});

	$('[data-opt-out]').change(function(event){

		var list_id = $(this).data('list-id'),
			user_id = $(this).data('user-id'),
			opt_out = $(this).is(':checked');

		$.ajax({
			'url'  : '{{ route('sleighdogs.maillist.maillists.unsubscribe') }}',
			'type' : 'POST',
			'data' : {
				'list_id' : list_id,
				'user_id' : user_id,
				'opt_out' : opt_out
			}
		});

	});

	$('[data-toggle="tooltip"]').tooltip();

	$('.btn-invite').click(function(event){

		event.preventDefault();

		var data = $(this).parents('.modal').find('form').serializeObject();

		$.ajax({
			type: 'POST',
			url: '{{ route('yori.invite.public') }}',
			data: data
		}).done(function(msg){

			//window.location.reload();
			$('.modal').modal('hide');

			if ( data['invite']['0']['type'] == 'corporate' ) {
				swal({   
					title: "Company created",   
					text: "Company was succesfully created",   
					timer: 2000,   
					showConfirmButton: false 
				});

				window.location.reload();
			} else {
				swal({   
					title: "User invited",   
					text: "User was succesfully invited",   
					timer: 2000,   
					showConfirmButton: false 
				});

				window.location.reload();
			}

		});
	});

	$('#upload-avatar').click(function(event){
		event.preventDefault();

		$('[name="avatar"]').trigger('click');
	});

	$('[name="avatar"]').change(function(event){
		
		$('#profile-form').submit();
	
	});

	$('#upload-logo').click(function(event){
		event.preventDefault();

		$('[name="logo"]').trigger('click');
	});

	$('[name="logo"]').change(function(event){
		
		$('#profile-form').submit();
	
	});

	$('#claim_btn').click(function(event){
		event.preventDefault();
	
		var data = {
			email: $('#claim_email').val(),
			id: {{ $user->id }}
		};

		$.ajax({
			type: 'POST',
			url: '{{ route('yori.claim') }}',
			data: data
		}).success(function(data){
			
			$('#claim-contact-modal').modal('hide');

			swal({   
				title: "Contact claimed",   
				text: "Contact was succesfully claimed, you will be notified",   
				timer: 2000,   
				showConfirmButton: false 
			});

		})
	});
});

(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);
</script>
@stop

{{-- Inline styles --}}
@section('styles')
@parent
<style type="text/css">
.lightbox {
	outline: 0!important;
}
</style>
@stop

{{-- Page content --}}
@section('page')

</div>

@if ( $user->type == 'internal' || $user->type == 'individual' || $user->type == '' )

<div class="upper-stripe bg-primary p-t-30 p-b-30">

	<div class="container">
		<div class="row">
			<div class="col-md-10 title-text-md">
				<h1>
					@live_custom($user, 'fullname')
				</h1>
				
				@if ( true )
					<h6 class="montserrat sinking-title">Personal<br>Profile</h6>
				@else
					<h6 class="montserrat sinking-title">Company<br>Profile</h6>
				@endif
			</div>
			<div class="col-md-2 hidden-xs hidden-sm">

				<div class="progressbar-half-circle">
					@widget('sleighdogs/profile::stats.completeness', [$user, '#ffffff', '#13bfdf'])
				</div>

				<h6 class="montserrat sinking-title">Profile<br>Complete</h6>

			</div>
		</div>
	</div>

</div>

@if ( Sentinel::check() )
<div class="upper-stripe-shadow bg-primary">
	<div class="container">
		
		@if ( $user->id == $currentUser->id )
		<ul class="nav nav-tabs primary-tabs">
			<li role="presentation" class="active">
				<a href="#profile-tab" aria-controls="profile-tab" role="tab" data-toggle="tab">Personal Profile</a>
			</li>
			<?php 
			$managed_corporates = false;
			$managed_corporates_display = false;

			$managed_corporates = $user->managed_corporates()->get(); 
			$managed_corporates_display = $user->managed_corporates()->count() > 0 ? true : false;
			?>

			@if ( $managed_corporates_display )
					
				@foreach( $managed_corporates as $corporate )
					
					<li role="presentation">
						<a href="{{ route('sleighdogs.profile.view', [$corporate->id]) }}">
							{{ $corporate->brand_name }}
						</a>
					</li>

				@endforeach

			@endif
			<li role="presentation">
				<a href="#invitations-tab" aria-controls="invitations-tab" role="tab" data-toggle="tab">Invitations</a>
			</li>
		</ul>
		@endif

	</div>
</div>
@endif

<div class="page__content container">

	<div class="row">
		
		<div class="col-md-12">
				
			<div class="tab-content">
    			<div role="tabpanel" class="tab-pane active" id="profile-tab">

			@if ( strpos($user->email, '@yori') !== false )
				
				<br>
				<div class="alert alert-info text-center">
					<p>Displayed contact is not active, is it yours?</p>
					<br>
					<button type="button" class="btn btn-default" data-toggle="modal" data-target="#claim-contact-modal">
						Claim contact
					</button>
				</div>

			@endif
		
		

			{{-- Form --}}
			<form id="profile-form" role="form" method="post" accept-char="UTF-8" enctype="multipart/form-data" data-parsley-validate action="{{ route('yori.upload.avatar') }}">

				{{-- Form: CSRF Token --}}
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
				<input type="hidden" name="id" value="{{ $user->id }}">

				<input type="hidden" name="type" value="{{ $user->type }}">

				<div class="row">

					{{-- General information --}}
					<div class="col-md-4 profile-col">
						
						<div class="cell-md-standard">
							
						</div>
						
						<div class="text-center">
							<a href="{{ $user->gravatar_url }}?s=400&w=400&h=400" class="lightbox" style="margin:0 auto 20px;">
								<img src="{{ $user->gravatar_url }}?s=250&w=250&h=250" class="img-preview" alt="~" style="max-width:100%;height:auto;">
							</a>
						</div>

						@if ( $currentUser->id == $user->id )
							<input type="file" class="hidden" name="avatar">
							<div class="text-center">
								<a href="#" class="btn btn-primary" id="upload-avatar">
									Upload avatar
								</a>
							</div>
						@endif

						<table class="table table-noborder">
							<tbody>
								<tr>
									<td colspan="2">
										<h5 class="montserrat">Basic</h5>
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'first_name')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'last_name')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'position')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'gender')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'founder')
									</td>
								</tr>
							</tbody>
						</table>

					</div>

					<div class="col-md-4 profile-col">
						
						<div class="cell-md-standard">

						</div>

						<table class="table table-noborder">
							<tbody>
								<tr>
									<td colspan="2">
										<h5 class="montserrat">Contacts</h5>
									</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-work_email">
											Work e-mail
										</label>
									</th>
									<td>@live($user, 'work_email')</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-private_email">
											Private e-mail
										</label>
									</th>
									<td>@live($user, 'private_email')</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-office_phone">
											Office phone
										</label>
									</th>
									<td>@live($user, 'office_phone')</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-personal_phone">
											Personal phone
										</label>
									</th>
									<td>@live($user, 'personal_phone')</td>
								</tr>
								
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="2">
										<h5 class="montserrat">Birthday</h5>
									</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-birthday">
											Birthday
										</label>
									</th>
									<td>@live($user, 'birthday')</td>
								</tr>

								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								
								<tr>
									<td colspan="2">
										<h5 class="montserrat" data-placement="left" data-toggle="tooltip" title="Short description of your contact">
											About
											<i class="fa fa-info-circle"></i>
										</h5>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										@live($user, 'job_description')
									</td>
								</tr>
							</tbody>
						</table>

					</div>

					<div class="col-md-4 profile-col">
						
						<div class="cell-md-standard">

						</div>

						<table class="table table-noborder">
							<tbody>
								<!--
								<tr>
									<td colspan="2">
										<button type="button" class="btn btn-link btn-block btn-lg btn-invite-modal" data-toggle="modal" data-target="#invite-modal">
											<i class="ion-ios-plus-outline"></i>
											<span class="btn-label">
												Create company
											</span>
										</button>
									</td>
								</tr>
								-->
								<?php /*
								<tr>
									<td colspan="2">
										<ul class="list-group">
											<?php $listed_positions = ['CEO', 'CFO', 'COO', 'CMO']; ?>
											@if ( $user->corporates()->count() > 0 )
											@foreach( $user->corporates()->get() as $corporate )
											<li class="list-group-item">
												<a href="{{ route('sleighdogs.profile.view', [$corporate->id]) }}" class="row">
													<div class="col-xs-2">
													@foreach( $corporate->mediaByTag('logo') as $medium )
													<img src="{{ route('thumb.view', $medium->path) . '?w=30&h=30' }}" alt="{{ $corporate->brand_name }}" title="{{{ $medium->name }}}" class="avatar-xs m-t-10">
													@endforeach
													</div>
													<div class="col-xs-10">
														<small><strong>{{ $corporate->brand_name }}</strong><br>
														{{ $corporate->pivot->position }}
														</small>
													</div>
												
												</a>
											</li>
											@endforeach
											@endif
											<?php $index = 0; ?>
											<li class="list-group-item">
												<div class="row">
													<div class="col-xs-6 col-xs-offset-2">
														<select name="invite[{{ $index }}][position]" class="form-control">
															@foreach($listed_positions as $position)
															<option value="{{ $position }}">{{ $position }}</option>
															@endforeach
														</select>
														<input type="brand_name" class="form-control input-sm" name="invite[{{ $index }}][brand_name]" placeholder="Brand name">
														<input type="email" class="form-control input-sm" name="invite[{{ $index }}][email]" placeholder="E-mail" data-field="email">
														<input type="hidden" name="invite[{{ $index }}][root]" value="{{ $user->id }}">
														<input type="hidden" name="invite[{{ $index }}][type]" value="corporate">
													</div>
													<div class="col-xs-4">
														
													</div>
												</div>
											</li>

											<li class="list-group-item">
												<div class="row">
													<div class="col-xs-6 col-xs-offset-2">
														<button type="submit" class="btn btn-link btn-invite">
															Create company
														</button>
													</div>
												</div>
											</li>
										</ul>
									</td>
								</tr>
								*/ ?>
								<tr>
									<td colspan="2">
										<h5 class="montserrat" data-placement="left" data-toggle="tooltip" title="Tags are used to better target your contact in communication">
											Tags
											<i class="fa fa-info-circle"></i>
										</h5>
									</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-tags">
											Tags
										</label>
									</th>
									<td>
										@live_custom($user, 'tags')
									</td>
								</tr>
							</tbody>
						</table>


						<table class="table table-noborder">
							<tbody>
								<tr>
									<td colspan="2">
										<h5 class="montserrat">Social profiles</h5>
									</td>
								</tr>
								<tr>
									<th class="hidden-label">LinkedIn URL</th>
									<td>
										@live($user, 'linkedin_url')
									</td>
								</tr>
								<tr>
									<th class="hidden-label">Facebook URL</th>
									<td>
										@live($user, 'facebook_url')
									</td>
								</tr>
								<tr>
									<th class="hidden-label">Twitter URL</th>
									<td>
										@live($user, 'twitter_url')
									</td>
								</tr>
								<tr>
									<th class="hidden-label">Custom social links</th>
									<td>
										@live($user, 'custom_social_links')
									</td>
								</tr>
							</tbody>
						</table>


					</div>

				</div>

			</form>

				</div><!-- /#profile-tab -->
					
				<div role="tabpanel" class="tab-pane" id="invitations-tab">
					
					<div class="row">
						<div class="col-md-6 col-md-offset-3">

							<div class="cell-md-standard">
							
							</div>

							<h5 class="montserrat">
								Companies
							</h5>
							
							<button type="button" class="btn btn-link btn-block btn-lg btn-invite-modal" data-toggle="modal" data-target="#invite-modal">
								<i class="ion-ios-plus-outline"></i>
								<span class="btn-label">
									Create company
								</span>
							</button>

							<?php 
							$managed_corporates = false;
							$managed_corporates_display = false;

							$managed_corporates = $user->managed_corporates()->get(); 
							$managed_corporates_display = $user->managed_corporates()->count() > 0 ? true : false;
							?>

							@if ( $managed_corporates_display )
									
								@foreach( $managed_corporates as $corporate )

									<h5 class="montserrat">
										Invitations
									</h5>
									
									<button type="button" class="btn btn-link btn-block btn-lg btn-invite-modal" data-toggle="modal" data-target="#invite-modal-{{ $corporate->id }}">
										<i class="ion-ios-plus-outline"></i>
										<span class="btn-label">
											Invite colleagues to {{ $corporate->brand_name }}
										</span>
									</button>

									<div class="modal fade" id="invite-modal-{{ $corporate->id }}" tabindex="-1" role="dialog" aria-labelledby="invite-modalLabel">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									        <h4 class="modal-title" id="invite-modalLabel">Invite colleague</h4>
									      </div>
									      <div class="modal-body">
									      	<form id="invite-form-{{ $corporate->id }}">

												<?php $listed_positions = ['CEO', 'CFO', 'COO', 'CMO']; ?>
												<div class="form-group">
													<label for="" class="control-label">
														Colleague's position in the company
													</label>
													<select name="invite[0][position]" class="form-control">
														@foreach($listed_positions as $position)
														<option value="{{ $position }}">{{ $position }}</option>
														@endforeach
													</select>
												</div>
										        
										        <div class="form-group">
										        	<label for="first_name" class="control-label">
										        		First name
										        	</label>
										        	<input type="first_name" class="form-control input-sm" name="invite[0][first_name]" placeholder="First name" required>
										        </div>

										        <div class="form-group">
										        	<label for="last_name" class="control-label">
										        		Last name
										        	</label>
										        	<input type="last_name" class="form-control input-sm" name="invite[0][last_name]" placeholder="Last name" required>
										        </div>
												
												<div class="form-group">
													<label for="invite_email" class="control-label">
														Colleague's e-mail
													</label>
													<input type="email" class="form-control" name="invite[0][email]" placeholder="E-mail" data-field="email" required id="invite_email-{{ $corporate->id }}">
												</div>
												
												<input type="hidden" name="invite[0][root]" value="{{ $corporate->id }}">
												<input type="hidden" name="invite[0][type]" value="individual">

											</form>
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        <button type="button" class="btn btn-primary btn-invite">Invite colleague</button>
									      </div>
									    </div>
									  </div>
									</div>


								@endforeach

							@endif

						</div>
					</div>

				</div>

			</div><!-- /.tab-content -->

		</div>

	</div>

</div>

@if ( $user->corporates()->count() > 0 )
<div class="lower-stripe bg-primary p-t-30 p-b-30">

	<h2 class="text-center">Linked corporates</h2>

	<div class="container">
		
		<div class="linked-contacts col-sm-6 col-sm-offset-3">
		@foreach( $user->corporates()->get() as $corporate )
			<div class="row card-row">

				<div class="">

					<a href="{{ route('sleighdogs.profile.view', [$corporate->id]) }}" class="card bg-white">
						
						<div class="gravatar">
							
							@foreach( $corporate->mediaByTag('logo') as $medium )
							<img src="{{ route('thumb.view', $medium->path) . '?w=150&h=150' }}" alt="{{ $corporate->brand_name }}" title="{{{ $medium->name }}}">
							@endforeach

						</div>

						<h4>
							{{ $corporate->brand_name }}
						</h4>

						<p>
							{{ $corporate->pivot->position }}
							@if( (int)$corporate->pivot->started_at > 0 )
								
								since {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $corporate->pivot->started_at)->format('Y') }}

							@endif
						</p>

						<i class="ion-ios-arrow-thin-right pull-right"></i>

					</a>

				</div>

			</div>
		@endforeach
		</div>

	</div>
	
</div>

@endif

<!-- Modal -->
<div class="modal fade" id="invite-modal" tabindex="-1" role="dialog" aria-labelledby="invite-modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="invite-modalLabel">Create company</h4>
      </div>
      <div class="modal-body">
      	<form id="invite-form">
			
			<?php $listed_positions = ['CEO', 'CFO', 'COO', 'CMO']; ?>
			<div class="form-group">
				<label for="" class="control-label">
					Your position in the company
				</label>
				<select name="invite[0][position]" class="form-control">
					@foreach($listed_positions as $position)
					<option value="{{ $position }}">{{ $position }}</option>
					@endforeach
				</select>
			</div>
	        
	        <div class="form-group">
	        	<label for="invite_name" class="control-label">
	        		Company name
	        	</label>
	        	<input type="text" class="form-control" name="invite[0][brand_name]" placeholder="Brand name" required id="invite_name">
	        </div>
			
			<div class="form-group">
				<label for="invite_email" class="control-label">
					Company e-mail
				</label>
				<input type="email" class="form-control" name="invite[0][email]" placeholder="E-mail" data-field="email" required id="invite_email">
			</div>
			
			<input type="hidden" name="invite[0][root]" value="{{ $user->id }}">
			<input type="hidden" name="invite[0][type]" value="corporate">

		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-invite">Create company</button>
      </div>
    </div>
  </div>
</div>

@else

<div class="upper-stripe bg-primary p-t-30 p-b-30">

	<div class="container">
		<div class="row">
			<div class="col-md-10 title-text-md">
				<h1>
					{{ $user->brand_name }}
				</h1>
				

				<h6 class="montserrat sinking-title">Company<br>Profile</h6>
			</div>
			<div class="col-md-2 hidden-xs hidden-sm">

				<div class="progressbar-half-circle">
					@widget('sleighdogs/profile::stats.completeness', [$user, '#ffffff', '#13bfdf'])
				</div>

				<h6 class="montserrat sinking-title">Profile<br>Complete</h6>

			</div>
		</div>
	</div>

</div>

@if ( Sentinel::check() )
<div class="upper-stripe-shadow bg-primary">
	<div class="container">
		
		@if ( $user->id == $currentUser->id || Sanatorium\Inputs\Widgets\Live::canEdit($user) )
		<ul class="nav nav-tabs primary-tabs">
			@if ( $currentUser->type == 'individual' || $currentUser->type == 'internal' )
				<li role="presentation">
					<a href="/profile">
						Personal profile
					</a>
				</li>
			@endif
			<li role="presentation" class="active">
				<a href="#profile-tab" aria-controls="profile-tab" role="tab" data-toggle="tab">{{ $user->brand_name }}</a>
			</li>
			<li role="presentation">
				<a href="#invitations-tab" aria-controls="invitations-tab" role="tab" data-toggle="tab">Invitations</a>
			</li>
		</ul>
		@endif

	</div>
</div>
@endif

<div class="page__content container">

	<div class="row">
		
		<div class="col-md-12">

			<div class="tab-content">
				<div id="profile-tab" role="tabpanel" class="tab-pane active" >
				
			@if ( strpos($user->email, '@yori') !== false )
				
				<br>
				<div class="alert alert-info text-center">
					<p>Displayed contact is not active, is it yours?</p>
					<br>
					<button type="button" class="btn btn-default" data-toggle="modal" data-target="#claim-contact-modal">
						Claim contact
					</button>
				</div>

			@endif
		
			{{-- Form --}}
			<form id="profile-form" action="{{ route('yori.upload.avatar') }}" role="form" method="post" accept-char="UTF-8" data-parsley-validate enctype="multipart/form-data">

				{{-- Form: CSRF Token --}}
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<input type="hidden" name="id" value="{{ $user->id }}">

				<input type="hidden" name="type" value="{{ $user->type }}">

				<div class="row">

					{{-- General information --}}
					<div class="col-md-4 profile-col">
						
						<div class="cell-md-standard">
							
						</div>
						
						@foreach( $user->mediaByTag('logo') as $medium )
						<div class="text-center">
							<a href="{{ route('media.view', $medium->path) }}" class="lightbox">
								<img src="{{ route('thumb.view', $medium->path) . '?w=250&h=250' }}" class="img-preview" alt="~" title="{{{ $medium->name }}}" style="max-width:100%;height:auto;">
							</a>
						</div>
						@endforeach


						@if ( Sanatorium\Inputs\Widgets\Live::canEdit($user) )
							<input type="file" class="hidden" name="logo">
							<div class="text-center">
								<a href="#" class="btn btn-primary" id="upload-logo">
									Upload logo
								</a>
							</div>
						@endif
						
						<table class="table table-noborder">
							<tbody>
								<tr>
									<td>
										@live($user, 'official_administrative_name')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'founding_year')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'number_of_employees')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'industry')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'street')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'postcode')
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'city')
									</td>
								</tr>
							</tbody>
						</table>

						@if ( $user->video )
							<h5 class="montserrat">Video</h5>
							<!-- 16:9 aspect ratio -->
							<div class="embed-responsive embed-responsive-16by9">
							  <iframe class="embed-responsive-item" src="{{ $user->video }}"></iframe>
							</div>
						@endif

					</div>

					<div class="col-md-4 profile-col">
						
						<div class="cell-md-standard">

						</div>
						
						<table class="table table-noborder">
							<tbody>
								<tr>
									<td>
										<h5 class="montserrat">Contacts</h5>
									</td>
								</tr>
								<tr>
									<td>@live($user, 'fax')</td>
								</tr>
								<tr>
									<td>@live($user, 'website')</td>
								</tr>
								<tr>
									<td>@live($user, 'phone')</td>
								</tr>
								<tr>
									<td>@live($user, 'general_email')</td>
								</tr>
								<tr>
									<td>@live($user, 'company_linkedin_url')</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
										<h5 class="montserrat">Social profiles</h5>
									</td>
								</tr>
								<tr>
									<td>@live($user, 'company_facebook_url')</td>
								</tr>
								<tr>
									<td>@live($user, 'company_twitter_url')</td>
								</tr>	
								<tr>
									<td>@live($user, 'company_custom_social')</td>
								</tr>								

								<tr>
									<td>&nbsp;</td>
								</tr>
								
								<tr>
									<td>
										<h5 class="montserrat" data-placement="left" data-toggle="tooltip" title="Short description of your contact">
											About
											<i class="fa fa-info-circle"></i>
										</h5>
									</td>
								</tr>
								<tr>
									<td>
										@live($user, 'short_description')
									</td>
								</tr>
							</tbody>
						</table>

					</div>

					<div class="col-md-4 profile-col">
						
						<div class="cell-md-standard">

						</div>

						<table class="table table-noborder">
							<tbody>
								<!--
								<tr>
									<td colspan="2">
										<button type="button" class="btn btn-link btn-block btn-lg btn-invite-modal" data-toggle="modal" data-target="#invite-modal">
											<i class="ion-ios-plus-outline"></i>
											<span class="btn-label">
												Invite colleague
											</span>
										</button>
									</td>
								</tr>
								-->
								<?php /*
								<tr>
									<td>
										<ul class="list-group">
										<?php 
										$listed_positions = ['CEO', 'CFO', 'COO', 'CMO'];
										$index = 0;
										?>
										@foreach( $listed_positions as $position )
										@if ( $user->individuals()->wherePivot('position', $position)->count() > 0 )
											@foreach( $user->individuals()->wherePivot('position', $position)->get() as $individual )
												<li class="list-group-item">
													<a href="{{ route('sleighdogs.profile.view', [$individual->id])  }}" class="row">
														<div class="col-xs-2">
															<img src="{{ $individual->gravatar_url }}?s=30&w=30&h=30" class="avatar-xs m-t-10" alt="{{ $individual->first_name }} {{ $individual->last_name }}">
														</div>
														<div class="col-xs-10">
															<small>
															<strong>{{ $individual->first_name }} {{ $individual->last_name }}</strong><br>
															{{ $individual->pivot->position }}
															</small>
														</div>
													</a>
												</li>
											@endforeach
										@else
											
											<li class="list-group-item">
												<div class="row">
													<div class="col-xs-6 col-xs-offset-2">
														<h5 class="montserrat">{{ $position }}</h5>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-2">
														<img src="http://www.gravatar.com/avatar/?s=30" style="opacity:0.5">
													</div>
													<div class="col-xs-6">
														<input type="email" class="form-control input-sm" name="invite[{{ $index }}][email]" placeholder="E-mail" data-field="email" data-input="">
														<input type="first_name" class="form-control input-sm" name="invite[{{ $index }}][first_name]" placeholder="First name">
														<input type="last_name" class="form-control input-sm" name="invite[{{ $index }}][last_name]" placeholder="Last name">
														<input type="hidden" name="invite[{{ $index }}][position]" value="{{ $position }}">
														<input type="hidden" name="invite[{{ $index }}][root]" value="{{ $user->id }}">
														<input type="hidden" name="invite[{{ $index }}][type]" value="individual">
													</div>
													<div class="col-xs-4">
														
													</div>
												</div>
											</li>
											<?php $index++; ?>
										@endif
										@endforeach
										@if ( $index != 0 )
										<li class="list-group-item">
											<div class="row">
												<div class="col-xs-6 col-xs-offset-2">
													<button type="submit" class="btn btn-link btn-invite">
														Invite
													</button>
												</div>
											</div>
											
										</li>
										@endif
										</ul>
									</td>
								</tr>
								*/ ?>
								<tr>
									<td>
										<h5 class="montserrat" data-placement="left" data-toggle="tooltip" title="Tags are used to better target your contact in communication">
											Tags
											<i class="fa fa-info-circle"></i>
										</h5>
									</td>
								</tr>
								<tr>
									<th class="hidden-label">
										<label class="control-label" for="input-tags">
											Tags
										</label>
									</th>
									<td>
										@live_custom($user, 'tags')
									</td>
								</tr>
							</tbody>
						</table>
						

						@if ( $user->mediaByTag('picture_gallery')->count() > 0 )
							<h5 class="montserrat">Gallery</h5>
							
							@foreach( $user->mediaByTag('picture_gallery') as $medium )

			                    @if ( $medium->is_image )
			                    	<a href="{{ route('media.view', $medium->path) }}" class="lightbox">
			                        	<img src="{{ route('thumb.view', $medium->path) . '?w=250&h=150' }}" class="img-preview" alt="~" title="{{{ $medium->name }}}" style="max-width:100%;height:auto;">
			                        </a>
			                        @else
				                        @if ( ($medium->mime == 'audio/ogg') || ($medium->mime == 'video/mp4') || ($medium->mime == 'video/ogg') )

				                        <i class="fa fa-file-movie-o fa-5x"></i>

				                        @elseif ( $medium->mime == 'application/zip')

				                        <i class="fa fa-file-zip-o fa-5x"></i>

				                        @elseif ( $medium->mime == 'application/pdf')

				                        <i class="fa fa-file-pdf-o fa-5x"></i>

				                        @else

				                        <i class="fa fa-file-o fa-5x"></i>

				                        @endif
			                    @endif
							@endforeach

						@endif

					</div>

				</div>

			</form>

				</div><!-- /#profile-tab -->

				<div role="tabpanel" class="tab-pane" id="invitations-tab">
					
					<div class="row">
						<div class="col-md-4 col-md-offset-4">

							<div class="cell-md-standard">
							
							</div>

							<h5 class="montserrat">
								Invitations
							</h5>
							
							<button type="button" class="btn btn-link btn-block btn-lg btn-invite-modal" data-toggle="modal" data-target="#invite-modal">
								<i class="ion-ios-plus-outline"></i>
								<span class="btn-label">
									Invite colleague
								</span>
							</button>

						</div>
					</div>

				</div>

			</div><!-- /.tab-content -->

		</div>

	</div>

</div>

@if ( $user->individuals()->count() > 0 )
<div class="lower-stripe bg-primary p-t-30 p-b-30">

	<h2 class="text-center">Linked contacts</h2>

	<div class="container">
		
		<div class="linked-contacts col-sm-6 col-sm-offset-3">
		@foreach( $user->individuals()->get() as $individual )
			<div class="row card-row">

				<div class="">

					<a href="{{ route('sleighdogs.profile.view', [$individual->id]) }}" class="card bg-white">
						
						<div class="gravatar">
							
							<img src="{{ $individual->gravatar_url }}?w=60&h=60">

						</div>

						<h4>
							{{ $individual->first_name }}
							{{ $individual->last_name }}
						</h4>

						<p>
							{{ $individual->pivot->position }}
							@if( (int)$individual->pivot->started_at > 0 )
								
								since {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $individual->pivot->started_at)->format('Y') }}

							@endif
						</p>

						<i class="ion-ios-arrow-thin-right pull-right"></i>

					</a>

				</div>

			</div>
		@endforeach
		</div>

	</div>
	
</div>
@endif

<!-- Modal -->
<div class="modal fade" id="invite-modal" tabindex="-1" role="dialog" aria-labelledby="invite-modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="invite-modalLabel">Invite colleague</h4>
      </div>
      <div class="modal-body">
      	<form id="invite-form">

			<?php $listed_positions = ['CEO', 'CFO', 'COO', 'CMO']; ?>
			<div class="form-group">
				<label for="" class="control-label">
					Colleague's position in the company
				</label>
				<select name="invite[0][position]" class="form-control">
					@foreach($listed_positions as $position)
					<option value="{{ $position }}">{{ $position }}</option>
					@endforeach
				</select>
			</div>
	        
	        <div class="form-group">
	        	<label for="first_name" class="control-label">
	        		First name
	        	</label>
	        	<input type="first_name" class="form-control input-sm" name="invite[0][first_name]" placeholder="First name"required>
	        </div>

	        <div class="form-group">
	        	<label for="last_name" class="control-label">
	        		Last name
	        	</label>
	        	<input type="last_name" class="form-control input-sm" name="invite[0][last_name]" placeholder="Last name"required>
	        </div>
			
			<div class="form-group">
				<label for="invite_email" class="control-label">
					Colleague's e-mail
				</label>
				<input type="email" class="form-control" name="invite[0][email]" placeholder="E-mail" data-field="email" required id="invite_email">
			</div>
			
			<input type="hidden" name="invite[0][root]" value="{{ $user->id }}">
			<input type="hidden" name="invite[0][type]" value="individual">

		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-invite">Invite colleague</button>
      </div>
    </div>
  </div>
</div>

@endif

@if ( is_object($currentUser) )
	@if ( $currentUser->id == $user->id )
	@if ( $user->maillists()->count() > 0 )
	<div class="lower-stripe bg-gray-lightest p-t-30 p-b-30">

		<h2 class="text-center">Manage newsletters</h2>

		<div class="container">
			
			<div class="row">
				
				<div class="linked-contacts col-sm-6 col-sm-offset-3">

				<p class="text-center text-muted">
					Uncheck newsletter type to opt out.
				</p>

				@foreach( $user->maillists()->get() as $list )
				
					<div class="row card-row">

						<div class="">
					
							<label for="list-{{ $list->id }}">
								<input type="checkbox" id="list-{{ $list->id }}" {{ $list->pivot->opt_out == 1 ? '' : 'checked' }} data-opt-out data-user-id="{{ $user->id }}" data-list-id="{{ $list->id }}" class="js-switch">
								{{ $list->label }}
							</label>

						</div>

					</div>
				
				@endforeach
				</div>

			</div>

		</div>

	</div>
	@endif
	@endif
@endif

<div class="page__content container">


@stop
