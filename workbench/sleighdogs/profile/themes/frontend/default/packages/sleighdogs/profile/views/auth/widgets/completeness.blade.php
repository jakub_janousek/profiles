{{ Asset::queue('progressbar', 'sleighdogs/profile::progressbar/progressbar.js') }}


<div id="example-clock-container"></div>

<script type="text/javascript">
var element = document.getElementById('example-clock-container');

var seconds = new ProgressBar.Circle(element, {
    duration: 200,
    color: "#FCB03C",
    trailColor: "#ddd"
});

setInterval(function() {
    var second = new Date().getSeconds();
    seconds.animate(second / 60, function() {
        seconds.setText(second);
    });
}, 1000);
</script>