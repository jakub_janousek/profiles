<?php

return [

	'index'  => 'List Profiles',
	'create' => 'Create new Profile',
	'edit'   => 'Edit Profile',
	'delete' => 'Delete Profile',

];
