<?php

return [

	// General messages
	'not_found' => 'Profile [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Profile was successfully created.',
		'update' => 'Profile was successfully updated.',
		'delete' => 'Profile was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the profile. Please try again.',
		'update' => 'There was an issue updating the profile. Please try again.',
		'delete' => 'There was an issue deleting the profile. Please try again.',
	],

];
