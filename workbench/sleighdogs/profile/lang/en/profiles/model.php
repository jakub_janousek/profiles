<?php

return [

	'general' => [

		'id' => 'Id',
		'firstname' => 'Firstname',
		'lastname' => 'Lastname',
		'salutation' => 'Salutation',
		'gender' => 'Gender',
		'portrait' => 'Portrait',
		'email' => 'Email',
		'password' => 'Password',
		'phone' => 'Phone',
		'category' => 'Category',
		'education' => 'Education',
		'degree' => 'Degree',
		'discipline' => 'Discipline',
		'company' => 'Company',
                'qualifications' => 'Qualifications',
		'created_at' => 'Created At',
		'firstname_help' => 'Enter the Firstname here',
		'lastname_help' => 'Enter the Lastname here',
		'salutation_help' => 'Enter the Salutation here',
		'gender_help' => 'Enter the Gender here',
		'portrait_help' => 'Enter the Portrait here',
		'email_help' => 'Enter the Email here',
		'password_help' => 'Enter the Password here',
		'phone_help' => 'Enter the Phone here',
		'category_help' => 'Enter the Category here',
		'education_help' => 'Enter the Education here',
		'degree_help' => 'Enter the Degree here',
		'discipline_help' => 'Enter the Discipline here',
		'company_help' => 'Enter the Company here',
                'qualifications_help' => 'Select Qualifications here',

	],

];
