<?php

return [

    'index'  => 'List Corporates',
    'create' => 'Create new Corporates',
    'edit'   => 'View / Edit Corporates',
    'delete' => 'Delete Corporates',
    'executeAction' => 'Bulk actions',

];
