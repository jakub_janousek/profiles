<?php

return [

	'general' => [

		'id' => 'Id',
		'created_at' => 'Created At',
		'updated_at' => 'Updated At',
		'brand_name' => 'Brand',
		'number_of_employees' => '# Employees',
		'founding_year' => 'Founded',
		'addresses' => 'Address',
		'website' => 'Website',
		'general_email' => 'General E-mail',
		'tags' => 'Tags',
		'completeness' => 'Completeness',
		'last_email' => 'Last E-mail',
		'name' => 'Name',
	],

];
