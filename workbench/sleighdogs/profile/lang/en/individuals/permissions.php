<?php

return [

    'index'  => 'List Individual contacts',
    'create' => 'Create new Individual contacts',
    'edit'   => 'View / Edit Individual contacts',
    'delete' => 'Delete Individual contacts',
    'executeAction' => 'Bulk actions',

];
