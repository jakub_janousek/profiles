<?php

return [

	// General messages
	'not_found' => 'Individual contact [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Individual contact was successfully created.',
		'update' => 'Individual contact was successfully updated.',
		'delete' => 'Individual contact was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the individual contact. Please try again.',
		'update' => 'There was an issue updating the individual contact. Please try again.',
		'delete' => 'There was an issue deleting the individual contact. Please try again.',
	],

];
