<?php

return [

	'general' => [

		'id' => 'Id',
		'created_at' => 'Created At',
		'updated_at' => 'Updated At',
		'name' => 'Name',
		'email' => 'E-mail',
		'gender' => 'Gender',
		'position' => 'Position',
		'office_phone' => 'Office phone',
		'completeness' => 'Completeness',
		'last_email' => 'Last E-mail',
	],

];
