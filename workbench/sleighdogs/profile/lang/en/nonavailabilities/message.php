<?php

return [

	// General messages
	'not_found' => 'Booking [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Booking was successfully created.',
		'update' => 'Booking was successfully updated.',
		'delete' => 'Booking was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the Booking. Please try again.',
		'update' => 'There was an issue updating the Booking. Please try again.',
		'delete' => 'There was an issue deleting the Booking. Please try again.',
	],

];
