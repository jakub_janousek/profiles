<?php

return [

	'general' => [

		'id' => 'Id',
		'date_start' => 'Start Date',
		'date_end' => 'End Date',
                'profile_id' => 'Profile Id (Temp)',
                'notes' => 'Notes',
		'date_start_help' => 'Enter the Start Date here',
		'date_end_help' => 'Enter the End Date here',
                'profile_id_help' => 'Enter the Profile Id here(Temp)',
                'notes_help' => 'Enter the Notes here',
	],

];
