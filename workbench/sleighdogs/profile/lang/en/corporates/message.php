<?php

return [

	// General messages
	'not_found' => 'Corporate contact [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Corporate contact was successfully created.',
		'update' => 'Corporate contact was successfully updated.',
		'delete' => 'Corporate contact was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the corporate contact. Please try again.',
		'update' => 'There was an issue updating the corporate contact. Please try again.',
		'delete' => 'There was an issue deleting the corporate contact. Please try again.',
	],

];
