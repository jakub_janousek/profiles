<?php

use Illuminate\Foundation\Application;
use Cartalyst\Extensions\ExtensionInterface;
use Cartalyst\Settings\Repository as Settings;
use Cartalyst\Permissions\Container as Permissions;

return [

	/*
	|--------------------------------------------------------------------------
	| Name
	|--------------------------------------------------------------------------
	|
	| This is your extension name and it is only required for
	| presentational purposes.
	|
	*/

	'name' => 'Profile',

	/*
	|--------------------------------------------------------------------------
	| Slug
	|--------------------------------------------------------------------------
	|
	| This is your extension unique identifier and should not be changed as
	| it will be recognized as a new extension.
	|
	| Ideally, this should match the folder structure within the extensions
	| folder, but this is completely optional.
	|
	*/

	'slug' => 'sleighdogs/profile',

	/*
	|--------------------------------------------------------------------------
	| Author
	|--------------------------------------------------------------------------
	|
	| Because everybody deserves credit for their work, right?
	|
	*/

	'author' => 'Sleighdogs',

	/*
	|--------------------------------------------------------------------------
	| Description
	|--------------------------------------------------------------------------
	|
	| One or two sentences describing the extension for users to view when
	| they are installing the extension.
	|
	*/

	'description' => 'Extended user profile',

	/*
	|--------------------------------------------------------------------------
	| Version
	|--------------------------------------------------------------------------
	|
	| Version should be a string that can be used with version_compare().
	| This is how the extensions versions are compared.
	|
	*/

	'version' => '0.1.0',

	/*
	|--------------------------------------------------------------------------
	| Requirements
	|--------------------------------------------------------------------------
	|
	| List here all the extensions that this extension requires to work.
	| This is used in conjunction with composer, so you should put the
	| same extension dependencies on your main composer.json require
	| key, so that they get resolved using composer, however you
	| can use without composer, at which point you'll have to
	| ensure that the required extensions are available.
	|
	*/

	'require' => [
		'platform/access',
		'platform/users',
		'platform/pages',
                'sleighdogs/qualifications'
	],

	/*
	|--------------------------------------------------------------------------
	| Autoload Logic
	|--------------------------------------------------------------------------
	|
	| You can define here your extension autoloading logic, it may either
	| be 'composer', 'platform' or a 'Closure'.
	|
	| If composer is defined, your composer.json file specifies the autoloading
	| logic.
	|
	| If platform is defined, your extension receives convetion autoloading
	| based on the Platform standards.
	|
	| If a Closure is defined, it should take two parameters as defined
	| bellow:
	|
	|	object \Composer\Autoload\ClassLoader      $loader
	|	object \Illuminate\Foundation\Application  $app
	|
	| Supported: "composer", "platform", "Closure"
	|
	*/

	'autoload' => 'composer',

	/*
	|--------------------------------------------------------------------------
	| Service Providers
	|--------------------------------------------------------------------------
	|
	| Define your extension service providers here. They will be dynamically
	| registered without having to include them in app/config/app.php.
	|
	*/

	'providers' => [

		'Sleighdogs\Profile\Providers\ProfileServiceProvider',
                'Sleighdogs\Profile\Providers\NonAvailabilityServiceProvider',

	],

	/*
	|--------------------------------------------------------------------------
	| Routes
	|--------------------------------------------------------------------------
	|
	| Closure that is called when the extension is started. You can register
	| any custom routing logic here.
	|
	| The closure parameters are:
	|
	|	object \Cartalyst\Extensions\ExtensionInterface  $extension
	|	object \Illuminate\Foundation\Application        $app
	|
	*/

	'routes' => function(ExtensionInterface $extension, Application $app)
	{
		Route::group([
			'namespace' => 'Sleighdogs\Profile\Controllers\Frontend',
		], function()
		{
			Route::get('profile', ['as' => 'sleighdogs.profile.profile', 'uses' => 'ProfileController@profile']);

			Route::post('login', ['as' => 'user.login', 'uses' => 'UsersController@processLogin']);

			Route::post('register', ['as' => 'user.register', 'uses' => 'UsersController@processRegistration']);
		});

		Route::group([
			'namespace' => 'Sleighdogs\Profile\Controllers\Frontend',
		], function()
		{
			Route::get('contact/{id}', ['as' => 'sleighdogs.profile.view', 'uses' => 'PublicProfileController@profile']);

			Route::get('view/{slug}', ['as' => 'sleighdogs.profile.view.slug', 'uses' => 'PublicProfileController@profileBySlug']);

			Route::get('embed/{id}', ['as' => 'sleighdogs.profile.embed', 'uses' => 'PublicProfileController@embed']);
		});

		Route::group([
			'prefix'    => admin_uri() . '/profile/all/',
			'namespace' => 'Sleighdogs\Profile\Controllers\Admin',
		], function()
		{
			Route::get('/' , ['as' => 'admin.sleighdogs.profile.all', 'uses' => 'ProfilesController@index']);

			Route::get('grid' , ['as' => 'admin.sleighdogs.profile.all.grid', 'uses' => 'ProfilesController@grid']);

			Route::post('/' , ['as' => 'admin.sleighdogs.profile.all', 'uses' => 'ProfilesController@executeAction']);
		});

		Route::group([
			'prefix'    => admin_uri() . '/profile/corporates/',
			'namespace' => 'Sleighdogs\Profile\Controllers\Admin',
		], function()
		{
			Route::get('/' , ['as' => 'admin.sleighdogs.profile.corporates.all', 'uses' => 'CorporatesController@index']);
			Route::post('/', ['as' => 'admin.sleighdogs.profile.corporates.all', 'uses' => 'CorporatesController@executeAction']);

			Route::get('cachetest' , ['as' => 'admin.sleighdogs.profile.corporates.cachetest', 'uses' => 'CorporatesController@cachetest']);

			Route::get('grid', ['as' => 'admin.sleighdogs.profile.corporates.grid', 'uses' => 'CorporatesController@grid']);

			Route::get('create' , ['as' => 'admin.sleighdogs.profile.corporates.create', 'uses' => 'CorporatesController@create']);
			Route::post('create', ['as' => 'admin.sleighdogs.profile.corporates.create', 'uses' => 'CorporatesController@store']);

			Route::get('{id}'   , ['as' => 'admin.sleighdogs.profile.corporates.edit'  , 'uses' => 'CorporatesController@edit']);
			Route::post('{id}'  , ['as' => 'admin.sleighdogs.profile.corporates.edit'  , 'uses' => 'CorporatesController@update']);

			Route::delete('{id}', ['as' => 'admin.sleighdogs.profile.corporates.delete', 'uses' => 'CorporatesController@delete']);
		});

		Route::group([
			'prefix'    => admin_uri() . '/profile/individuals/',
			'namespace' => 'Sleighdogs\Profile\Controllers\Admin',
		], function()
		{
			Route::get('/' , ['as' => 'admin.sleighdogs.profile.individuals.all', 'uses' => 'IndividualsController@index']);
			Route::post('/', ['as' => 'admin.sleighdogs.profile.individuals.all', 'uses' => 'IndividualsController@executeAction']);

			Route::get('grid', ['as' => 'admin.sleighdogs.profile.individuals.grid', 'uses' => 'IndividualsController@grid']);

			Route::get('create' , ['as' => 'admin.sleighdogs.profile.individuals.create', 'uses' => 'IndividualsController@create']);
			Route::post('create', ['as' => 'admin.sleighdogs.profile.individuals.create', 'uses' => 'IndividualsController@store']);

			Route::get('{id}'   , ['as' => 'admin.sleighdogs.profile.individuals.edit'  , 'uses' => 'IndividualsController@edit']);
			Route::post('{id}'  , ['as' => 'admin.sleighdogs.profile.individuals.edit'  , 'uses' => 'IndividualsController@update']);

			Route::delete('{id}', ['as' => 'admin.sleighdogs.profile.individuals.delete', 'uses' => 'IndividualsController@delete']);
		});

		Route::get('/', function(){
			if ( Sentinel::check() ) {
				return redirect()->route('user.login');
			} else {
				return redirect()->route('user.login');
			}
		});

                Route::group([
                        'prefix'    => admin_uri().'/profile/profiles',
                        'namespace' => 'Sleighdogs\Profile\Controllers\Admin',
                ], function()
                {
                        Route::get('/' , ['as' => 'admin.sleighdogs.profile.profiles.all', 'uses' => 'ProfilesController@index']);
                        Route::post('/', ['as' => 'admin.sleighdogs.profile.profiles.all', 'uses' => 'ProfilesController@executeAction']);

                        Route::get('grid', ['as' => 'admin.sleighdogs.profile.profiles.grid', 'uses' => 'ProfilesController@grid']);

                        Route::get('create' , ['as' => 'admin.sleighdogs.profile.profiles.create', 'uses' => 'ProfilesController@create']);
                        Route::post('create', ['as' => 'admin.sleighdogs.profile.profiles.create', 'uses' => 'ProfilesController@store']);

                        Route::get('{id}'   , ['as' => 'admin.sleighdogs.profile.profiles.edit'  , 'uses' => 'ProfilesController@edit']);
                        Route::post('{id}'  , ['as' => 'admin.sleighdogs.profile.profiles.edit'  , 'uses' => 'ProfilesController@update']);

                        Route::delete('{id}', ['as' => 'admin.sleighdogs.profile.profiles.delete', 'uses' => 'ProfilesController@delete']);
                });

		Route::group([
			'prefix'    => 'profile/profiles',
			'namespace' => 'Sleighdogs\Profile\Controllers\Frontend',
		], function()
		{
			Route::get('/', ['as' => 'sleighdogs.profile.profiles.index', 'uses' => 'ProfilesController@index']);
		});
                
                Route::group([
                        'prefix'    => admin_uri().'/profile/nonavailabilities',
                        'namespace' => 'Sleighdogs\Profile\Controllers\Admin',
                ], function()
                {
                        Route::get('/' , ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.all', 'uses' => 'NonAvailabilitiesController@index']);
                        Route::post('/', ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.all', 'uses' => 'NonAvailabilitiesController@executeAction']);

                        Route::get('grid', ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.grid', 'uses' => 'NonAvailabilitiesController@grid']);

                        Route::get('create' , ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.create', 'uses' => 'NonAvailabilitiesController@create']);
                        Route::post('create', ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.create', 'uses' => 'NonAvailabilitiesController@store']);

                        Route::get('{id}'   , ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.edit'  , 'uses' => 'NonAvailabilitiesController@edit']);
                        Route::post('{id}'  , ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.edit'  , 'uses' => 'NonAvailabilitiesController@update']);

                        Route::delete('{id}', ['as' => 'admin.sleighdogs.nonavailabilities.nonavailabilities.delete', 'uses' => 'NonAvailabilitiesController@delete']);
                });
	},

	/*
	|--------------------------------------------------------------------------
	| Database Seeds
	|--------------------------------------------------------------------------
	|
	| Platform provides a very simple way to seed your database with test
	| data using seed classes. All seed classes should be stored on the
	| `database/seeds` directory within your extension folder.
	|
	| The order you register your seed classes on the array below
	| matters, as they will be ran in the exact same order.
	|
	| The seeds array should follow the following structure:
	|
	|	Vendor\Namespace\Database\Seeds\FooSeeder
	|	Vendor\Namespace\Database\Seeds\BarSeeder
	|
	*/

	'seeds' => [

		'Sleighdogs\Profile\Database\Seeds\SalutationsTableSeeder',

	],

	/*
	|--------------------------------------------------------------------------
	| Permissions
	|--------------------------------------------------------------------------
	|
	| Register here all the permissions that this extension has. These will
	| be shown in the user management area to build a graphical interface
	| where permissions can be selected to allow or deny user access.
	|
	| For detailed instructions on how to register the permissions, please
	| refer to the following url https://cartalyst.com/manual/permissions
	|
	*/

	'permissions' => function(Permissions $permissions)
	{
		$permissions->group('corporates', function ($g) {
            $g->name = trans('sleighdogs/profile::corporates/common.title');

            $g->permission('corporates.index', function ($p) {
                $p->label = trans('sleighdogs/profile::corporates/permissions.index');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\CorporatesController', 'index, grid');
            });

            $g->permission('corporates.executeAction', function ($p) {
                $p->label = trans('sleighdogs/profile::corporates/permissions.executeAction');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\CorporatesController', 'executeAction');
            });

            $g->permission('corporates.create', function ($p) {
                $p->label = trans('sleighdogs/profile::corporates/permissions.create');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\CorporatesController', 'create, store');
            });

            $g->permission('corporates.update', function ($p) {
                $p->label = trans('sleighdogs/profile::corporates/permissions.edit');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\CorporatesController', 'edit, update');
            });

            $g->permission('corporates.delete', function ($p) {
                $p->label = trans('sleighdogs/profile::corporates/permissions.delete');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\CorporatesController', 'delete');
            });
        });

        $permissions->group('individuals', function ($g) {
            $g->name = trans('sleighdogs/profile::individuals/common.title');

            $g->permission('individuals.index', function ($p) {
                $p->label = trans('sleighdogs/profile::individuals/permissions.index');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\IndividualsController', 'index, grid');
            });

            $g->permission('individuals.executeAction', function ($p) {
                $p->label = trans('sleighdogs/profile::individuals/permissions.executeAction');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\IndividualsController', 'executeAction');
            });

            $g->permission('individuals.create', function ($p) {
                $p->label = trans('sleighdogs/profile::individuals/permissions.create');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\IndividualsController', 'create, store');
            });

            $g->permission('individuals.update', function ($p) {
                $p->label = trans('sleighdogs/profile::individuals/permissions.edit');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\IndividualsController', 'edit, update');
            });

            $g->permission('individuals.delete', function ($p) {
                $p->label = trans('sleighdogs/profile::individuals/permissions.delete');

                $p->controller('Sleighdogs\Profile\Controllers\Admin\IndividualsController', 'delete');
            });
        });

		$permissions->group('profile', function($g)
		{
			$g->name = 'Profiles';

			$g->permission('profile.index', function($p)
			{
				$p->label = trans('sleighdogs/profile::profiles/permissions.index');

				$p->controller('Sleighdogs\Profile\Controllers\Admin\ProfilesController', 'index, grid');
			});

			$g->permission('profile.create', function($p)
			{
				$p->label = trans('sleighdogs/profile::profiles/permissions.create');

				$p->controller('Sleighdogs\Profile\Controllers\Admin\ProfilesController', 'create, store');
			});

			$g->permission('profile.edit', function($p)
			{
				$p->label = trans('sleighdogs/profile::profiles/permissions.edit');

				$p->controller('Sleighdogs\Profile\Controllers\Admin\ProfilesController', 'edit, update');
			});

			$g->permission('profile.delete', function($p)
			{
				$p->label = trans('sleighdogs/profile::profiles/permissions.delete');

				$p->controller('Sleighdogs\Profile\Controllers\Admin\ProfilesController', 'delete');
			});
		});
	},

	/*
	|--------------------------------------------------------------------------
	| Widgets
	|--------------------------------------------------------------------------
	|
	| Closure that is called when the extension is started. You can register
	| all your custom widgets here. Of course, Platform will guess the
	| widget class for you, this is just for custom widgets or if you
	| do not wish to make a new class for a very small widget.
	|
	*/

	'widgets' => function()
	{

	},

	/*
	|--------------------------------------------------------------------------
	| Settings
	|--------------------------------------------------------------------------
	|
	| Register any settings for your extension. You can also configure
	| the namespace and group that a setting belongs to.
	|
	*/

	'settings' => function(Settings $settings, Application $app)
	{

	},

	/*
	|--------------------------------------------------------------------------
	| Menus
	|--------------------------------------------------------------------------
	|
	| You may specify the default various menu hierarchy for your extension.
	| You can provide a recursive array of menu children and their children.
	| These will be created upon installation, synchronized upon upgrading
	| and removed upon uninstallation.
	|
	| Menu children are automatically put at the end of the menu for extensions
	| installed through the Operations extension.
	|
	| The default order (for extensions installed initially) can be
	| found by editing app/config/platform.php.
	|
	*/

	'menus' => [

		'admin' => [
			[
                                'slug' => 'admin-sleighdogs-profile',
				'name' => 'Profiles',
				'class' => 'fa fa-circle-o',
				'uri' => 'profile',
				'regex' => '/:admin\/profile/i',
				'children' => [
					[
						'class' => 'fa fa-circle-o',
						'name' => 'Profiles',
						'uri' => 'profile/profiles',
						'regex' => '/:admin\/profile\/profile/i',
						'slug' => 'admin-sleighdogs-profile-profile',
					],
				],
			],
		],
		'main' => [
			
		],

	],

];
