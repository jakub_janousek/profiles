<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('degrees', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('disciplines', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('salutations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('profiles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->Integer('salutation')->unsigned();
            $table->tinyInteger('gender');
            $table->string('portrait');
            $table->string('email');
            $table->string('password');
            $table->string('phone');
            $table->integer('category')->unsigned();
            $table->text('education');
            $table->integer('degree')->unsigned();
            $table->integer('discipline')->unsigned();
            $table->string('company');
            $table->timestamps();
        });
        Schema::table('profiles', function($table) {
            $table->foreign('category')
                    ->references('id')->on('categories');
            $table->foreign('degree')
                    ->references('id')->on('degrees');
            $table->foreign('discipline')
                    ->references('id')->on('disciplines');
            $table->foreign('salutation')
                    ->references('id')->on('salutations');
        });
        Schema::create('projects', function(Blueprint $table) {
            $table->increments('id');
            $table->date('year');
            $table->string('title');
            $table->text('description');
            $table->string('client');
            $table->integer('profile_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('projects', function($table) {
            $table->foreign('profile_id')
                    ->references('id')->on('profiles')
                    ->onDelete('cascade');
        });
        Schema::create('experiences', function(Blueprint $table) {
            $table->increments('id');
            $table->date('year_start');
            $table->date('year_end');
            $table->string('company');
            $table->text('description');
            $table->integer('profile_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('experiences', function($table) {
            $table->foreign('profile_id')
                    ->references('id')->on('profiles')
                    ->onDelete('cascade');
            $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');
        });
        Schema::create('files', function(Blueprint $table) {
            $table->increments('id');
            $table->string('original_name');
            $table->string('name_on_server');
            $table->integer('profile_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('files', function($table) {
            $table->foreign('profile_id')
                    ->references('id')->on('profiles')
                    ->onDelete('cascade');
        });
        Schema::create('nonavailabilites', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->date('date_start');
            $table->date('date_end');
            $table->text('notes');
            $table->timestamps();
        });
        Schema::table('nonavailabilites', function($table) {
            $table->foreign('profile_id')
                    ->references('id')->on('profiles')
                    ->onDelete('cascade');
        });
        Schema::create('profile_qualifications', function(Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('qualifications_id')->unsigned();
        });
        Schema::table('profile_qualifications', function($table) {
            $table->foreign('profile_id')
                    ->references('id')->on('profiles')
                    ->onDelete('cascade');
            $table->foreign('qualifications_id')
                    ->references('id')->on('qualifications')
                    ->onDelete('cascade');
        });
        Schema::create('project_qualifications', function(Blueprint $table) {
            $table->integer('project_id')->unsigned();
            $table->integer('qualifications_id')->unsigned();
        });
        Schema::table('project_qualifications', function($table) {
            $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');
            $table->foreign('qualifications_id')
                    ->references('id')->on('qualifications')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('project_qualifications', function($table) {
            $table->dropForeign('project_qualifications_project_id_foreign');
            $table->dropForeign('project_qualifications_qualifications_id_foreign');
        });
        
        Schema::drop('project_qualifications');
        Schema::table('profile_qualifications', function($table) {
            $table->dropForeign('profile_qualifications_profile_id_foreign');
            $table->dropForeign('profile_qualifications_qualifications_id_foreign');
        });
        Schema::drop('profile_qualifications');
        Schema::table('nonavailabilites', function($table) {
            $table->dropForeign('nonavailabilites_profile_id_foreign');
        });
        Schema::drop('nonavailabilites');
        Schema::table('files', function($table) {
            $table->dropForeign('files_profile_id_foreign');
        });
        Schema::drop('files');
        Schema::table('experiences', function($table) {
            $table->dropForeign('experiences_profile_id_foreign');
            $table->dropForeign('experiences_project_id_foreign');
        });
        Schema::drop('experiences');
        Schema::table('projects', function($table) {
            $table->dropForeign('projects_profile_id_foreign');
        });
        Schema::drop('projects');
        Schema::table('profiles', function($table) {
            $table->dropForeign('profiles_category_foreign');
            $table->dropForeign('profiles_degree_foreign');
            $table->dropForeign('profiles_discipline_foreign');
            $table->dropForeign('profiles_salutation_foreign');
        });
        Schema::drop('profiles');
        Schema::drop('salutations');
        Schema::drop('disciplines');
        Schema::drop('degrees');
        Schema::drop('categories');
    }

}
