<?php namespace Sleighdogs\Profile\Database\Seeds;

use DB;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class SalutationsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// $faker = Faker::create();

		DB::table('salutations')->truncate();
                DB::table('salutations')->insert([
			 'Herr'	,
                         'Dr',
                         'Frau'
			 ]);
	}

}
