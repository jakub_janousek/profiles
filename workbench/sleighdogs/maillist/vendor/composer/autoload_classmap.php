<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'CreateMaillistsTable' => $baseDir . '/database/migrations/2015_12_17_123149_create_maillists_table.php',
    'Sleighdogs\\Maillist\\Controllers\\Admin\\MaillistsController' => $baseDir . '/src/Controllers/Admin/MaillistsController.php',
    'Sleighdogs\\Maillist\\Controllers\\Frontend\\MaillistsController' => $baseDir . '/src/Controllers/Frontend/MaillistsController.php',
    'Sleighdogs\\Maillist\\Database\\Seeds\\MaillistsTableSeeder' => $baseDir . '/database/seeds/MaillistsTableSeeder.php',
    'Sleighdogs\\Maillist\\Handlers\\Maillist\\MaillistDataHandler' => $baseDir . '/src/Handlers/Maillist/MaillistDataHandler.php',
    'Sleighdogs\\Maillist\\Handlers\\Maillist\\MaillistDataHandlerInterface' => $baseDir . '/src/Handlers/Maillist/MaillistDataHandlerInterface.php',
    'Sleighdogs\\Maillist\\Handlers\\Maillist\\MaillistEventHandler' => $baseDir . '/src/Handlers/Maillist/MaillistEventHandler.php',
    'Sleighdogs\\Maillist\\Handlers\\Maillist\\MaillistEventHandlerInterface' => $baseDir . '/src/Handlers/Maillist/MaillistEventHandlerInterface.php',
    'Sleighdogs\\Maillist\\Models\\Maillist' => $baseDir . '/src/Models/Maillist.php',
    'Sleighdogs\\Maillist\\Providers\\MaillistServiceProvider' => $baseDir . '/src/Providers/MaillistServiceProvider.php',
    'Sleighdogs\\Maillist\\Repositories\\Maillist\\MaillistRepository' => $baseDir . '/src/Repositories/Maillist/MaillistRepository.php',
    'Sleighdogs\\Maillist\\Repositories\\Maillist\\MaillistRepositoryInterface' => $baseDir . '/src/Repositories/Maillist/MaillistRepositoryInterface.php',
    'Sleighdogs\\Maillist\\Validator\\Maillist\\MaillistValidator' => $baseDir . '/src/Validator/Maillist/MaillistValidator.php',
    'Sleighdogs\\Maillist\\Validator\\Maillist\\MaillistValidatorInterface' => $baseDir . '/src/Validator/Maillist/MaillistValidatorInterface.php',
    'Sleighdogs\\Maillist\\Widgets\\Maillist' => $baseDir . '/src/Widgets/Maillist.php',
);
