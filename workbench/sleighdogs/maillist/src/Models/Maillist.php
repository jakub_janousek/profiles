<?php namespace Sleighdogs\Maillist\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Maillist extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'maillists';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'sleighdogs/maillist.maillist';

	public function users()
	{
		return $this->belongsToMany('Platform\Users\Models\User', 'maillist_users', 'maillist_id', 'user_id')
			->withPivot('opt_out');
	}

	public function getFiltered()
	{
		$result = [];

		$filtered = false;

		$query = \Platform\Attributes\Models\Value::where(function($q){
				return $q->where('entity_type', 'Platform\Users\Models\User')
					->orWhere('entity_type', 'Sleighdogs\Profile\Models\Corporate');
			});


		if ( $this->filters ) {

			$filtered = true;

			$filters = unserialize($this->filters);

			foreach( $filters as $filter ) {
				if ( strpos($filter, 'values..') !== false ) {
					// Filtering by attributes
					$prepared = str_replace('values..', '', $filter);

					list($key, $value) = explode(':', $prepared);

					$query->where($key, $value);
				}
			}
		}

		if ( $this->custom_filters ) {

			$filtered = true;

			$custom_filters = unserialize($this->custom_filters);

			$processed_filters = [];

			foreach ( $custom_filters as $filter ) {

				list($key, $compare, $value, $attribute_id) = explode(':', $filter);

				if ( !isset($processed_filters[$key]) ) {
					$processed_filters[$key] = [];
				}

				$processed_filters[$key][] = [
					'key' => $key,
					'value' => $value,
					'compare' => $compare,
					'attribute_id' => $attribute_id
				];

			}

			foreach( $processed_filters as $filters ) {

				$query = $query->where(function($qas) use ($filters) {

					$index_filter = 0;

					foreach( $filters as $filter ) {

						$index_filter++;

						extract($filter);

						if ( $index_filter == 1 ) {
							$where_method = 'where';
						} else {
							$where_method = 'orWhere';
						}

						$qas->{$where_method}(function($q) use ($compare, $value, $attribute_id) {
							$q->where('value', $compare, $value)
								->where('attribute_id', '=', $attribute_id);
						});

					}

				});

			}

		}

		if ( $filtered ) {

			$users = app('platform.users');

			$valid_ids = $query->groupBy('entity_id')->lists('entity_id');

			return $users->whereIn('id', $valid_ids)->get();

		}

		return $result;
	}

}
