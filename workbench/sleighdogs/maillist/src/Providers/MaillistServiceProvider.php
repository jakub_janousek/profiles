<?php namespace Sleighdogs\Maillist\Providers;

use Cartalyst\Support\ServiceProvider;

class MaillistServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Sleighdogs\Maillist\Models\Maillist']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('sleighdogs.maillist.maillist.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('sleighdogs.maillist.maillist', 'Sleighdogs\Maillist\Repositories\Maillist\MaillistRepository');

		// Register the data handler
		$this->bindIf('sleighdogs.maillist.maillist.handler.data', 'Sleighdogs\Maillist\Handlers\Maillist\MaillistDataHandler');

		// Register the event handler
		$this->bindIf('sleighdogs.maillist.maillist.handler.event', 'Sleighdogs\Maillist\Handlers\Maillist\MaillistEventHandler');

		// Register the validator
		$this->bindIf('sleighdogs.maillist.maillist.validator', 'Sleighdogs\Maillist\Validator\Maillist\MaillistValidator');
	}

}
