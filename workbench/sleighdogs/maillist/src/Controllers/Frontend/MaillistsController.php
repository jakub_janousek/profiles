<?php namespace Sleighdogs\Maillist\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;

class MaillistsController extends Controller {

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sleighdogs/maillist::index');
	}

	public function unsubscribe()
	{
		$data = request()->all();

		$maillist = \Sleighdogs\Maillist\Models\Maillist::find($data['list_id']);

		$maillist->users()->sync([$data['user_id'] => ['opt_out' => ($data['opt_out'] == false ? 0 : 1)] ], false); 

		return response('Success');

	}

}
