<?php namespace Sleighdogs\Maillist\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Sleighdogs\Maillist\Repositories\Maillist\MaillistRepositoryInterface;
use Event;
use Sanatorium\Mailer\Models\Mailtransaction;
use Mail;
use Cartalyst\Themes\Laravel\Facades\Theme;


class MaillistsController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Maillist repository.
	 *
	 * @var \Sleighdogs\Maillist\Repositories\Maillist\MaillistRepositoryInterface
	 */
	protected $maillists;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Sleighdogs\Maillist\Repositories\Maillist\MaillistRepositoryInterface  $maillists
	 * @return void
	 */
	public function __construct(MaillistRepositoryInterface $maillists)
	{
		parent::__construct();

		$this->maillists = $maillists;
	}

	/**
	 * Display a listing of maillist.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sleighdogs/maillist::maillists.index');
	}

	/**
	 * Datasource for the maillist Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->maillists->grid();

		$columns = [
			'id',
			'label',
			'filters',
			'custom_filters',
			'mc_id',
			'created_at',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.sleighdogs.maillist.maillists.edit', $element->id);

			$element->type_icon = ($element->filters || $element->custom_filters ? 'fa-filter' : 'fa-envelope-o');

			$element->type_label = ($element->filters || $element->custom_filters ? 'Dynamic' : 'Static');

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new maillist.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new maillist.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating maillist.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating maillist.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified maillist.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->maillists->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("sleighdogs/maillist::maillists/message.{$type}.delete")
		);

		return redirect()->route('admin.sleighdogs.maillist.maillists.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			foreach (request()->input('rows', []) as $row)
			{
				$this->maillists->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a maillist identifier?
		if (isset($id))
		{
			if ( ! $maillist = $this->maillists->find($id))
			{
				$this->alerts->error(trans('sleighdogs/maillist::maillists/message.not_found', compact('id')));

				return redirect()->route('admin.sleighdogs.maillist.maillists.all');
			}
		}
		else
		{
			$maillist = $this->maillists->createModel();
		}

		// Show the page
		return view('sleighdogs/maillist::maillists.form', compact('mode', 'maillist'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the maillist
		list($messages) = $this->maillists->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			$this->alerts->success(trans("sleighdogs/maillist::maillists/message.success.{$mode}"));

			return redirect()->route('admin.sleighdogs.maillist.maillists.all');
		}

		$this->alerts->error($messages, 'form');

		return redirect()->back()->withInput();
	}

	public function compose($id = null)
	{
		$maillist = $this->maillists->find($id);

		$mode = 'send';

		$basic = 'Hello {{ $object->user->first_name }},

this is testing message from the system.

{{ config("platform.app.title") }}

This copy will be inserted in the default template.';

		// Maillist does not exist
		if ( !$maillist ) {
			return redirect()->back()->withErrors(['Given list was not found']);
		}

		return view('sleighdogs/maillist::maillists/compose', compact(
			'id',
			'maillist',
			'mode',
			'basic'
			));
	}

	public function send($id = null, $subject = null, $message = null)
	{
		/*
		require_once( __DIR__ . '/../../../vendor/drewm/mailchimp-api/src/MailChimp.php' );
		require_once( __DIR__ . '/../../../vendor/drewm/mailchimp-api/src/Batch.php' );

		$MailChimp = new \DrewM\MailChimp\MailChimp('1e35938ce2fcd4719c2d667b8c6d55e4-us3');

		// Maillist is not yet registered with mailchimp
		if ( !$maillist->mc_id ) {

			$maillist->mc_id = '5785322b77';
			dd($id);
		}
		*/
	
		$maillist = $this->maillists->find($id);

		// Maillist does not exist
		if ( !$maillist ) {
			return redirect()->back()->withErrors(['Given list was not found']);
		}
	
		$subject = request()->get('subject');

		$message = html_entity_decode(request()->get('message'));

		// Set frontend theme for mailing
		Theme::setActive(config("platform-themes.active.frontend"));
		Theme::setFallback(config("platform-themes.fallback.frontend"));

		foreach (request()->input('contacts', []) as $row)
		{
			$maillist->users()->attach($row);
		}

		foreach( request()->get('recipients', []) as $recipient ) 
		{

			$this->sendOne($subject, $message, null, $recipient);

		}

		foreach( $maillist->users()->get() as $user ) 
		{

			$this->sendOne($subject, $message, $user);
			
		}
	
		foreach( $maillist->getFiltered() as $user ) 
		{

			$this->sendOne($subject, $message, $user);

		}

		return redirect()->route('admin.sleighdogs.maillist.maillists.all')->withSuccess(['E-mails were succesfully sent']);
	
	}

	public function sendOne($subject = null, $message = null, $user = null, $recipient = null)
	{

		$object = new \stdClass;
		$object->user = $user;

		$template = new Mailtransaction;
		$template->template = $message;

		if ( !empty($recipient) ) {
			$receiver = $recipient;
		} else {
			$receiver = $user->email;
		}

		$content = \DbView::make($template)->field('template')->with(['object' => $object])->render();

		// @todo - use queue
		$result = Mail::send('sanatorium/mailer::blank', ['content' => $content], function ($m) use ($object, $subject, $receiver) {
            
            $m->to($receiver);
            
            $m->subject($subject);

        });

    	if( count(Mail::failures()) > 0 ) {
    		$response = 'hard_bounce';
    	} else {
    		$response = 'delivered';
    	}

    	\Sanatorium\Mailer\Models\Maillog::create([
    		'event' => $subject,
    		'receiver' => $receiver,
    		'status' => $response
    		]);


	}

}
