<?php namespace Sleighdogs\Maillist\Handlers\Maillist;

use Sleighdogs\Maillist\Models\Maillist;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface MaillistEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a maillist is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a maillist is created.
	 *
	 * @param  \Sleighdogs\Maillist\Models\Maillist  $maillist
	 * @return mixed
	 */
	public function created(Maillist $maillist);

	/**
	 * When a maillist is being updated.
	 *
	 * @param  \Sleighdogs\Maillist\Models\Maillist  $maillist
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Maillist $maillist, array $data);

	/**
	 * When a maillist is updated.
	 *
	 * @param  \Sleighdogs\Maillist\Models\Maillist  $maillist
	 * @return mixed
	 */
	public function updated(Maillist $maillist);

	/**
	 * When a maillist is deleted.
	 *
	 * @param  \Sleighdogs\Maillist\Models\Maillist  $maillist
	 * @return mixed
	 */
	public function deleted(Maillist $maillist);

}
