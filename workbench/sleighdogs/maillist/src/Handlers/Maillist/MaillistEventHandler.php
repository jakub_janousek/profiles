<?php namespace Sleighdogs\Maillist\Handlers\Maillist;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Maillist\Models\Maillist;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class MaillistEventHandler extends BaseEventHandler implements MaillistEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.maillist.maillist.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.maillist.maillist.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.maillist.maillist.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.maillist.maillist.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.maillist.maillist.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Maillist $maillist)
	{
		$this->flushCache($maillist);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Maillist $maillist, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Maillist $maillist)
	{
		$this->flushCache($maillist);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Maillist $maillist)
	{
		$this->flushCache($maillist);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Maillist\Models\Maillist  $maillist
	 * @return void
	 */
	protected function flushCache(Maillist $maillist)
	{
		$this->app['cache']->forget('sleighdogs.maillist.maillist.all');

		$this->app['cache']->forget('sleighdogs.maillist.maillist.'.$maillist->id);
	}

}
