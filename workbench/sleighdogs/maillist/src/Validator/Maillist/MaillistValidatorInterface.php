<?php namespace Sleighdogs\Maillist\Validator\Maillist;

interface MaillistValidatorInterface {

	/**
	 * Updating a maillist scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
