<?php namespace Sleighdogs\Maillist\Repositories\Maillist;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;

class MaillistRepository implements MaillistRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Sleighdogs\Maillist\Handlers\Maillist\MaillistDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent maillist model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sleighdogs.maillist.maillist.handler.data'];

		$this->setValidator($app['sleighdogs.maillist.maillist.validator']);

		$this->setModel(get_class($app['Sleighdogs\Maillist\Models\Maillist']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sleighdogs.maillist.maillist.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sleighdogs.maillist.maillist.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new maillist
		$maillist = $this->createModel();

		// Fire the 'sleighdogs.maillist.maillist.creating' event
		if ($this->fireEvent('sleighdogs.maillist.maillist.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the maillist
			$maillist->fill($data)->save();

			// Fire the 'sleighdogs.maillist.maillist.created' event
			$this->fireEvent('sleighdogs.maillist.maillist.created', [ $maillist ]);
		}

		return [ $messages, $maillist ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the maillist object
		$maillist = $this->find($id);

		// Fire the 'sleighdogs.maillist.maillist.updating' event
		if ($this->fireEvent('sleighdogs.maillist.maillist.updating', [ $maillist, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($maillist, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the maillist
			$maillist->fill($data)->save();

			// Fire the 'sleighdogs.maillist.maillist.updated' event
			$this->fireEvent('sleighdogs.maillist.maillist.updated', [ $maillist ]);
		}

		return [ $messages, $maillist ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the maillist exists
		if ($maillist = $this->find($id))
		{
			// Fire the 'sleighdogs.maillist.maillist.deleted' event
			$this->fireEvent('sleighdogs.maillist.maillist.deleted', [ $maillist ]);

			// Delete the maillist entry
			$maillist->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
