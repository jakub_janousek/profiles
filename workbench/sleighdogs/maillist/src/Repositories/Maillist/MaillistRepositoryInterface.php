<?php namespace Sleighdogs\Maillist\Repositories\Maillist;

interface MaillistRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Sleighdogs\Maillist\Models\Maillist
	 */
	public function grid();

	/**
	 * Returns all the maillist entries.
	 *
	 * @return \Sleighdogs\Maillist\Models\Maillist
	 */
	public function findAll();

	/**
	 * Returns a maillist entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Sleighdogs\Maillist\Models\Maillist
	 */
	public function find($id);

	/**
	 * Determines if the given maillist is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given maillist is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given maillist.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a maillist entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Sleighdogs\Maillist\Models\Maillist
	 */
	public function create(array $data);

	/**
	 * Updates the maillist entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Sleighdogs\Maillist\Models\Maillist
	 */
	public function update($id, array $data);

	/**
	 * Deletes the maillist entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
