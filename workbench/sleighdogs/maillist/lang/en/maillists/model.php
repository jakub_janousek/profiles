<?php

return [

	'general' => [

		'id' => 'Id',
		'label' => 'Label',
		'type' => 'Type',
		'mc_id' => 'Mc_Id',
		'created_at' => 'Created At',
		'label_help' => 'Enter the Label here',
		'mc_id_help' => 'Enter the Mc_Id here',

	],

];
