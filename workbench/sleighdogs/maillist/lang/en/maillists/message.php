<?php

return [

	// General messages
	'not_found' => 'Maillist [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Maillist was successfully created.',
		'update' => 'Maillist was successfully updated.',
		'delete' => 'Maillist was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the maillist. Please try again.',
		'update' => 'There was an issue updating the maillist. Please try again.',
		'delete' => 'There was an issue deleting the maillist. Please try again.',
	],

];
