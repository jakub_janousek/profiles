<?php

return [

	'index'  => 'List Mailing lists',
	'create' => 'Create new Mailing list',
	'edit'   => 'Edit Mailing list',
	'delete' => 'Delete Mailing list',
	'compose'=> 'Compose message',
	'send'   => 'Send message',
	'sendOne'=> 'Send single mail',
];
