<?php namespace Sleighdogs\Maillist\Database\Seeds;

use DB;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class MaillistsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();

		DB::table('maillists')->truncate();

		foreach(range(1, 20) as $index)
		{
			DB::table('maillists')->insert([
			 	'label' => $faker->company(),
				'mc_id' => $faker->randomNumber(6),
				'created_at' => $faker->dateTime(),
				'updated_at' => $faker->dateTime(),
			]);
		}
	}

}
