<script type="text/template" data-grid="maillist" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td><a href="<%= r.edit_uri %>"><%= r.id %></a></td>
			<td><a href="<%= r.edit_uri %>"><%= r.label %></a></td>
			<td>
				<i class="fa <%= r.type_icon %>" data-toggle="tooltip" data-placement="right" title="<%= r.type_label %>"></i>
			</td>
			<td><%= r.created_at %></td>
		</tr>

	<% }); %>

</script>
