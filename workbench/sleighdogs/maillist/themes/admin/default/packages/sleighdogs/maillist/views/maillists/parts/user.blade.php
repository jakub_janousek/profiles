@if ( $user->type == 'individual' || $user->type == 'internal' || $user->type == '' )
<tr>
	<td>{{ $user->first_name }} {{ $user->last_name }}</td>
	<td>{{ $user->email }}</td>
	<td>
		@if ( is_object($user->pivot) )
			@if ( $user->pivot->opt_out == 1 )
				<i class="fa fa-ban"></i>
			@endif
		@endif
	</td>
	<td class="text-right">
		<a href="#" class="btn btn-default">
			<i class="fa fa-trash-o"></i>
		</a>
	</td>
</tr>
@else
<tr>
	<?php $user = \Sleighdogs\Profile\Models\Corporate::find($user->id) ?>
	<td>{{ $user->brand_name }}</td>
	<td>{{ $user->email }}</td>
	<td>
		@if ( is_object($user->pivot) )
			@if ( $user->pivot->opt_out == 1 )
				<i class="fa fa-ban"></i>
			@endif
		@endif
	</td>
	<td class="text-right">
		<a href="#" class="btn btn-default">
			<i class="fa fa-trash-o"></i>
		</a>
	</td>
</tr>
@endif