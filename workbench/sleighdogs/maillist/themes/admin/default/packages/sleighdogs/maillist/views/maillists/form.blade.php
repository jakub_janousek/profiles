@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('sleighdogs/maillist::maillists/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="maillist-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.sleighdogs.maillist.maillists.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $maillist->exists ? $maillist->id : null }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($maillist->exists)
							<li>
								<a href="{{ route('admin.sleighdogs.maillist.maillists.compose', $maillist->id) }}" class="tip btn btn-success" data-toggle="tooltip" data-original-title="Compose e-mail">
									<i class="fa fa-share"></i> <span class="visible-xs-inline">Compose e-mail</span>
								</a>
							</li>
							<li>
								<a href="{{ route('admin.sleighdogs.maillist.maillists.delete', $maillist->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('sleighdogs/maillist::maillists/common.tabs.general') }}}</a></li>
					<!-- <li role="presentation"><a href="#attributes" aria-controls="attributes" role="tab" data-toggle="tab">{{{ trans('sleighdogs/maillist::maillists/common.tabs.attributes') }}}</a></li> -->
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<fieldset>

							<div class="row">

								<div class="form-group{{ Alert::onForm('label', ' has-error') }}">

									<label for="label" class="control-label">
										<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/maillist::maillists/model.general.label_help') }}}"></i>
										{{{ trans('sleighdogs/maillist::maillists/model.general.label') }}}
									</label>

									<input type="text" class="form-control" name="label" id="label" placeholder="{{{ trans('sleighdogs/maillist::maillists/model.general.label') }}}" value="{{{ input()->old('label', $maillist->label) }}}">

									<span class="help-block">{{{ Alert::onForm('label') }}}</span>

								</div>

								<div class="form-group{{ Alert::onForm('mc_id', ' has-error') }}">

									<label for="mc_id" class="control-label">
										<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('sleighdogs/maillist::maillists/model.general.mc_id_help') }}}"></i>
										{{{ trans('sleighdogs/maillist::maillists/model.general.mc_id') }}}
									</label>

									<input type="text" class="form-control" name="mc_id" id="mc_id" placeholder="{{{ trans('sleighdogs/maillist::maillists/model.general.mc_id') }}}" value="{{{ input()->old('mc_id', $maillist->mc_id) }}}">

									<span class="help-block">{{{ Alert::onForm('mc_id') }}}</span>

								</div>


							</div>

							<table class="table table-stripped">
								<tbody>
								@foreach( $maillist->users()->get() as $user )
									@include('sleighdogs/maillist::maillists/parts/user')
								@endforeach

								@foreach( $maillist->getFiltered() as $user )
									@include('sleighdogs/maillist::maillists/parts/user')
								@endforeach
								</tbody>
							</table>

						</fieldset>

					</div>

					{{-- Tab: Attributes --}}
					<div role="tabpanel" class="tab-pane fade" id="attributes">
						@attributes($maillist)
					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
