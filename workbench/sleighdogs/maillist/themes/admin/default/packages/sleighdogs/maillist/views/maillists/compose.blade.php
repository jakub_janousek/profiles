@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('sleighdogs/maillist::maillists/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{-- Queue assets --}}
{{ Asset::queue('selectize', 'selectize/css/selectize.css', 'styles') }}
{{ Asset::queue('redactor', 'redactor/css/redactor.css', 'styles') }}

{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}
{{ Asset::queue('redactor', 'redactor/js/redactor.min.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
<script type="text/javascript">


$(function(){
	$('[data-load-template]').click(function(event){
		event.preventDefault();

		var id = $(this).data('load-template');

		$.ajax({
			type : 'GET',
			url : '{{ route('admin.sanatorium.mailer.maillog.template') }}',
			data : {
				id: id
			}
		}).done(function(msg){
			// would be better to chain, but that's not cool with redactor
			$('#message').redactor('code.set', msg.template);
			$('#message').val(msg.template);
			$('#message').trigger('change');
		});
		
	});

	$('#recipients').selectize({
		create: true,
		sortField: 'text'
	});

	$('#contacts').selectize({
		sortField: 'text'
	});
});
</script>
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="maillist-form" action="{{ route('admin.sleighdogs.maillist.maillists.send', [$id]) }}" role="form" method="post" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.sleighdogs.maillist.maillists.edit', [$id]) }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $maillist->exists ? $maillist->id : null }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							<li>
								<button type="submit" class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.send') }}}">
									<i class="fa fa-share"></i> <span class="visible-xs-inline">{{{ trans('action.send') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('sleighdogs/maillist::maillists/common.tabs.general') }}}</a></li>
				</ul>

				<div class="tab-content">
					
					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">
						
						<div class="row">
							<div class="col-sm-8">
								<div class="form-group">
									<label for="subject" class="control-label">Subject</label>
									<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
								</div>

								<div class="form-group{{ Alert::onForm('message', ' has-error') }}">

									<label for="message" class="control-label">Body <i class="fa fa-info-circle" data-toggle="popover" data-content="..."></i></label>
									
									<textarea class="form-control redactor" name="message" id="message" rows="20">{!! request()->old('message', $basic) !!}</textarea>

									<span class="help-block">{{{ Alert::onForm('message') }}}</span>

								</div>
							</div>

							<div class="col-sm-4">
								<div class="form-group">
									<label for="" class="control-label">
										Use template
									</label>
									<div class="list-group">
										@foreach( \Sanatorium\Mailer\Models\Mailtransaction::all() as $trans )
										<a href="#" class="list-group-item" data-load-template="{{ $trans->id }}">
											<i class="fa fa-long-arrow-left"></i>
											<?php
											if ( trans('sanatorium/mailer::events.'.$trans->event) != 'sanatorium/mailer::events.'.$trans->event ) {
												$label = trans('sanatorium/mailer::events.'.$trans->event);
											} else {
												$label = $trans->event;
											}
											?>
											{{ $label }}
										</a>
										@endforeach
									</div>
								</div>

								<div class="form-group">
									<label for="recipients" class="control-label">
										Add receivers<br><small>Following recipients will not be saved with the mail list</small>
									</label>
										
									<?php $users = app('platform.users'); ?>
									
									<select name="recipients[]" id="recipients" multiple>
									@foreach( $users->all() as $user )
										<option value="{{ $user->email }}">{{ $user->email }}</option>
									@endforeach
									</select>
										
								</div>

								<div class="form-group">
									<label for="recipients" class="control-label">
										Add contacts<br><small>Following recipients will be saved with the mail list</small>
									</label>
										
									<?php $users = app('platform.users'); ?>
									
									<select name="contacts[]" id="contacts" multiple>
									@foreach( $users->all() as $user )
										<option value="{{ $user->id }}">{{ $user->email }}</option>
									@endforeach
									</select>
										
								</div>
							</div>
						</div>	
					

					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
