<?php

return [

	// General messages
	'not_found' => 'Qualifications [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Qualifications was successfully created.',
		'update' => 'Qualifications was successfully updated.',
		'delete' => 'Qualifications was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the qualifications. Please try again.',
		'update' => 'There was an issue updating the qualifications. Please try again.',
		'delete' => 'There was an issue deleting the qualifications. Please try again.',
	],

];
