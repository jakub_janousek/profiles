<?php namespace Sleighdogs\Qualifications\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;

class QualificationsController extends Controller {

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sleighdogs/qualifications::index');
	}

}
