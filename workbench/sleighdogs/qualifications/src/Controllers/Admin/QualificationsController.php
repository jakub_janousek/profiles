<?php namespace Sleighdogs\Qualifications\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Sleighdogs\Qualifications\Repositories\Qualifications\QualificationsRepositoryInterface;

class QualificationsController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Qualifications repository.
	 *
	 * @var \Sleighdogs\Qualifications\Repositories\Qualifications\QualificationsRepositoryInterface
	 */
	protected $qualifications;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Sleighdogs\Qualifications\Repositories\Qualifications\QualificationsRepositoryInterface  $qualifications
	 * @return void
	 */
	public function __construct(QualificationsRepositoryInterface $qualifications)
	{
		parent::__construct();

		$this->qualifications = $qualifications;
	}

	/**
	 * Display a listing of qualifications.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sleighdogs/qualifications::qualifications.index');
	}

	/**
	 * Datasource for the qualifications Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->qualifications->grid();

		$columns = [
			'id',
			'name',
			'created_at',
                        'enabled',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.sleighdogs.qualifications.qualifications.edit', $element->id);

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new qualifications.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new qualifications.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating qualifications.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating qualifications.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified qualifications.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->qualifications->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("sleighdogs/qualifications::qualifications/message.{$type}.delete")
		);

		return redirect()->route('admin.sleighdogs.qualifications.qualifications.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			foreach (request()->input('rows', []) as $row)
			{
				$this->qualifications->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a qualifications identifier?
		if (isset($id))
		{
			if ( ! $qualifications = $this->qualifications->find($id))
			{
				$this->alerts->error(trans('sleighdogs/qualifications::qualifications/message.not_found', compact('id')));

				return redirect()->route('admin.sleighdogs.qualifications.qualifications.all');
			}
		}
		else
		{
			$qualifications = $this->qualifications->createModel();
		}

		// Show the page
		return view('sleighdogs/qualifications::qualifications.form', compact('mode', 'qualifications'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the qualifications
		list($messages) = $this->qualifications->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			$this->alerts->success(trans("sleighdogs/qualifications::qualifications/message.success.{$mode}"));

			return redirect()->route('admin.sleighdogs.qualifications.qualifications.all');
		}

		$this->alerts->error($messages, 'form');

		return redirect()->back()->withInput();
	}

}
