<?php namespace Sleighdogs\Qualifications\Providers;

use Cartalyst\Support\ServiceProvider;

class QualificationsServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Sleighdogs\Qualifications\Models\Qualifications']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('sleighdogs.qualifications.qualifications.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('sleighdogs.qualifications.qualifications', 'Sleighdogs\Qualifications\Repositories\Qualifications\QualificationsRepository');

		// Register the data handler
		$this->bindIf('sleighdogs.qualifications.qualifications.handler.data', 'Sleighdogs\Qualifications\Handlers\Qualifications\QualificationsDataHandler');

		// Register the event handler
		$this->bindIf('sleighdogs.qualifications.qualifications.handler.event', 'Sleighdogs\Qualifications\Handlers\Qualifications\QualificationsEventHandler');

		// Register the validator
		$this->bindIf('sleighdogs.qualifications.qualifications.validator', 'Sleighdogs\Qualifications\Validator\Qualifications\QualificationsValidator');
	}

}
