<?php namespace Sleighdogs\Qualifications\Repositories\Qualifications;

interface QualificationsRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Sleighdogs\Qualifications\Models\Qualifications
	 */
	public function grid();

	/**
	 * Returns all the qualifications entries.
	 *
	 * @return \Sleighdogs\Qualifications\Models\Qualifications
	 */
	public function findAll();

	/**
	 * Returns a qualifications entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Sleighdogs\Qualifications\Models\Qualifications
	 */
	public function find($id);

	/**
	 * Determines if the given qualifications is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given qualifications is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given qualifications.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a qualifications entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Sleighdogs\Qualifications\Models\Qualifications
	 */
	public function create(array $data);

	/**
	 * Updates the qualifications entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Sleighdogs\Qualifications\Models\Qualifications
	 */
	public function update($id, array $data);

	/**
	 * Deletes the qualifications entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
