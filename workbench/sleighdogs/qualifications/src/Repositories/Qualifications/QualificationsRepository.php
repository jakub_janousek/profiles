<?php namespace Sleighdogs\Qualifications\Repositories\Qualifications;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;

class QualificationsRepository implements QualificationsRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Sleighdogs\Qualifications\Handlers\Qualifications\QualificationsDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent qualifications model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sleighdogs.qualifications.qualifications.handler.data'];

		$this->setValidator($app['sleighdogs.qualifications.qualifications.validator']);

		$this->setModel(get_class($app['Sleighdogs\Qualifications\Models\Qualifications']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sleighdogs.qualifications.qualifications.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sleighdogs.qualifications.qualifications.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new qualifications
		$qualifications = $this->createModel();

		// Fire the 'sleighdogs.qualifications.qualifications.creating' event
		if ($this->fireEvent('sleighdogs.qualifications.qualifications.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the qualifications
			$qualifications->fill($data)->save();

			// Fire the 'sleighdogs.qualifications.qualifications.created' event
			$this->fireEvent('sleighdogs.qualifications.qualifications.created', [ $qualifications ]);
		}

		return [ $messages, $qualifications ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the qualifications object
		$qualifications = $this->find($id);

		// Fire the 'sleighdogs.qualifications.qualifications.updating' event
		if ($this->fireEvent('sleighdogs.qualifications.qualifications.updating', [ $qualifications, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($qualifications, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the qualifications
			$qualifications->fill($data)->save();

			// Fire the 'sleighdogs.qualifications.qualifications.updated' event
			$this->fireEvent('sleighdogs.qualifications.qualifications.updated', [ $qualifications ]);
		}

		return [ $messages, $qualifications ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the qualifications exists
		if ($qualifications = $this->find($id))
		{
			// Fire the 'sleighdogs.qualifications.qualifications.deleting' event
			$this->fireEvent('sleighdogs.qualifications.qualifications.deleting', [ $qualifications ]);

			// Delete the qualifications entry
			$qualifications->delete();

			// Fire the 'sleighdogs.qualifications.qualifications.deleted' event
			$this->fireEvent('sleighdogs.qualifications.qualifications.deleted', [ $qualifications ]);

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
