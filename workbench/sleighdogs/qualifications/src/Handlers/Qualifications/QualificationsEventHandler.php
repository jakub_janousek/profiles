<?php namespace Sleighdogs\Qualifications\Handlers\Qualifications;

use Illuminate\Events\Dispatcher;
use Sleighdogs\Qualifications\Models\Qualifications;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class QualificationsEventHandler extends BaseEventHandler implements QualificationsEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sleighdogs.qualifications.qualifications.creating', __CLASS__.'@creating');
		$dispatcher->listen('sleighdogs.qualifications.qualifications.created', __CLASS__.'@created');

		$dispatcher->listen('sleighdogs.qualifications.qualifications.updating', __CLASS__.'@updating');
		$dispatcher->listen('sleighdogs.qualifications.qualifications.updated', __CLASS__.'@updated');

		$dispatcher->listen('sleighdogs.qualifications.qualifications.deleted', __CLASS__.'@deleting');
		$dispatcher->listen('sleighdogs.qualifications.qualifications.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Qualifications $qualifications)
	{
		$this->flushCache($qualifications);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Qualifications $qualifications, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Qualifications $qualifications)
	{
		$this->flushCache($qualifications);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleting(Qualifications $qualifications)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Qualifications $qualifications)
	{
		$this->flushCache($qualifications);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sleighdogs\Qualifications\Models\Qualifications  $qualifications
	 * @return void
	 */
	protected function flushCache(Qualifications $qualifications)
	{
		$this->app['cache']->forget('sleighdogs.qualifications.qualifications.all');

		$this->app['cache']->forget('sleighdogs.qualifications.qualifications.'.$qualifications->id);
	}

}
