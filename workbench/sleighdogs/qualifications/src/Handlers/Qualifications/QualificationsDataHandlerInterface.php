<?php namespace Sleighdogs\Qualifications\Handlers\Qualifications;

interface QualificationsDataHandlerInterface {

	/**
	 * Prepares the given data for being stored.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function prepare(array $data);

}
