<?php namespace Sleighdogs\Qualifications\Handlers\Qualifications;

use Sleighdogs\Qualifications\Models\Qualifications;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface QualificationsEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a qualifications is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a qualifications is created.
	 *
	 * @param  \Sleighdogs\Qualifications\Models\Qualifications  $qualifications
	 * @return mixed
	 */
	public function created(Qualifications $qualifications);

	/**
	 * When a qualifications is being updated.
	 *
	 * @param  \Sleighdogs\Qualifications\Models\Qualifications  $qualifications
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Qualifications $qualifications, array $data);

	/**
	 * When a qualifications is updated.
	 *
	 * @param  \Sleighdogs\Qualifications\Models\Qualifications  $qualifications
	 * @return mixed
	 */
	public function updated(Qualifications $qualifications);

	/**
	 * When a qualifications is being deleted.
	 *
	 * @param  \Sleighdogs\Qualifications\Models\Qualifications  $qualifications
	 * @return mixed
	 */
	public function deleting(Qualifications $qualifications);

	/**
	 * When a qualifications is deleted.
	 *
	 * @param  \Sleighdogs\Qualifications\Models\Qualifications  $qualifications
	 * @return mixed
	 */
	public function deleted(Qualifications $qualifications);

}
