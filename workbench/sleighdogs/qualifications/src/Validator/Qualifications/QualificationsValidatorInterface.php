<?php namespace Sleighdogs\Qualifications\Validator\Qualifications;

interface QualificationsValidatorInterface {

	/**
	 * Updating a qualifications scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
