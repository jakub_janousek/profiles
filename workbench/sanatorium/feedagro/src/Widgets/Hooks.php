<?php namespace Sanatorium\Feedagro\Widgets;

use Cache;

class Hooks {

    public function feed($custom = [], $css_class = 'col-sm-3', $number = 4)
    {
        $data = $this->getData();

        return view('sanatorium/feedagro::widgets/feed', compact('css_class', 'number', 'data'));
    }

    public function getData($cache = 86400)
    {
        return Cache::remember('sanatorium.feedagro.feed', $cache, function(){
            $raw = file_get_contents('http://agropress.cz/?feed=1');
            return json_decode($raw, true);
        });
    }

}
