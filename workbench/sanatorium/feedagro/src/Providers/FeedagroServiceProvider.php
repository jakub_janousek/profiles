<?php namespace Sanatorium\Feedagro\Providers;

use Cartalyst\Support\ServiceProvider;

class FeedagroServiceProvider extends ServiceProvider {

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerHooks();
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {

    }

    /**
     * Register all hooks.
     *
     * @return void
     */
    protected function registerHooks()
    {
        $hooks = [
            [
                'position' => 'newsfeed',
                'hook' => 'sanatorium/feedagro::hooks.feed'
            ]
        ];

        $manager = $this->app['sanatorium.hooks.manager'];

        foreach ($hooks as $item) {
            extract($item);
            $manager->registerHook($position, $hook);
        }
    }

}
