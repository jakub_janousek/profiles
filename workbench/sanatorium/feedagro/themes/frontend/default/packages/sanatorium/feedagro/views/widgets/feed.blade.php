@if ( isset($data) && isset($number) && isset($css_class) )

    <!-- Widget feedagro -->
    @for ( $i = 0; $i < $number; $i++ )

        <?php extract($data[$i]); ?>

        <div class="{{ $css_class }} sanatorium-feedagro-widgets-feed">
            <article class="article">
                <a href="{{ $url }}" target="_blank">
                    <div class="article-image" style="background-image:url({{ $image }});width:100%;height:230px;background-size:cover;background-repeat:no-repeat;">

                    </div>
                    <div class="article-content">
                        <h3>
                            {{ $title }}
                        </h3>
                        <p>
                            {{ $excerpt }}
                        </p>
                    </div>
                </a>
            </article>
        </div>

    @endfor

@endif