<?php namespace Sanatorium\Brand\Providers;

use Cartalyst\Support\ServiceProvider;

class BrandServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Sanatorium\Brand\Models\Brand']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('sanatorium.brand.brand.handler.event');

		$this->registerHooks();
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('sanatorium.brand.brand', 'Sanatorium\Brand\Repositories\Brand\BrandRepository');

		// Register the data handler
		$this->bindIf('sanatorium.brand.brand.handler.data', 'Sanatorium\Brand\Handlers\Brand\BrandDataHandler');

		// Register the event handler
		$this->bindIf('sanatorium.brand.brand.handler.event', 'Sanatorium\Brand\Handlers\Brand\BrandEventHandler');

		// Register the validator
		$this->bindIf('sanatorium.brand.brand.validator', 'Sanatorium\Brand\Validator\Brand\BrandValidator');
	}

	/**
	 * Register all hooks.
	 *
	 * @return void
	 */
	protected function registerHooks()
	{
		$hooks = [
			[
				'position' => 'logo',
				'hook' => 'sanatorium/brand::hooks.logo'
			]
		];

		$manager = $this->app['sanatorium.hooks.manager'];

		foreach ($hooks as $item) {
			extract($item);
			$manager->registerHook($position, $hook);
		}
	}

}
