<?php namespace Sanatorium\Brand\Handlers\Brand;

use Illuminate\Events\Dispatcher;
use Sanatorium\Brand\Models\Brand;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class BrandEventHandler extends BaseEventHandler implements BrandEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('sanatorium.brand.brand.creating', __CLASS__.'@creating');
		$dispatcher->listen('sanatorium.brand.brand.created', __CLASS__.'@created');

		$dispatcher->listen('sanatorium.brand.brand.updating', __CLASS__.'@updating');
		$dispatcher->listen('sanatorium.brand.brand.updated', __CLASS__.'@updated');

		$dispatcher->listen('sanatorium.brand.brand.deleted', __CLASS__.'@deleting');
		$dispatcher->listen('sanatorium.brand.brand.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Brand $brand)
	{
		$this->flushCache($brand);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Brand $brand, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Brand $brand)
	{
		$this->flushCache($brand);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleting(Brand $brand)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Brand $brand)
	{
		$this->flushCache($brand);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Sanatorium\Brand\Models\Brand  $brand
	 * @return void
	 */
	protected function flushCache(Brand $brand)
	{
		$this->app['cache']->forget('sanatorium.brand.brand.all');

		$this->app['cache']->forget('sanatorium.brand.brand.'.$brand->id);
	}

}
