<?php namespace Sanatorium\Brand\Handlers\Brand;

use Sanatorium\Brand\Models\Brand;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface BrandEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a brand is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a brand is created.
	 *
	 * @param  \Sanatorium\Brand\Models\Brand  $brand
	 * @return mixed
	 */
	public function created(Brand $brand);

	/**
	 * When a brand is being updated.
	 *
	 * @param  \Sanatorium\Brand\Models\Brand  $brand
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Brand $brand, array $data);

	/**
	 * When a brand is updated.
	 *
	 * @param  \Sanatorium\Brand\Models\Brand  $brand
	 * @return mixed
	 */
	public function updated(Brand $brand);

	/**
	 * When a brand is being deleted.
	 *
	 * @param  \Sanatorium\Brand\Models\Brand  $brand
	 * @return mixed
	 */
	public function deleting(Brand $brand);

	/**
	 * When a brand is deleted.
	 *
	 * @param  \Sanatorium\Brand\Models\Brand  $brand
	 * @return mixed
	 */
	public function deleted(Brand $brand);

}
