<?php namespace Sanatorium\Brand\Validator\Brand;

interface BrandValidatorInterface {

	/**
	 * Updating a brand scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
