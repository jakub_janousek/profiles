<?php namespace Sanatorium\Brand\Widgets;

class Hooks {

	public function logo()
	{
		$brand = app('sanatorium.brand.brand')->whereActive(1)->first();

		if ( !is_object($brand) )
			return null;

		//$medium = app('platform.media')->find($brand->brand_logo);
		$medium = \Platform\Media\Models\Media::find($brand->brand_logo);

		if ( !is_object($medium) )
			return null;

		return \StorageUrl::url($medium->path);

	}

}
