<?php namespace Sanatorium\Brand\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;

class BrandsController extends Controller {

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sanatorium/brand::index');
	}

}
