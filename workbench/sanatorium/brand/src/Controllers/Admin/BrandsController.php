<?php namespace Sanatorium\Brand\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Sanatorium\Brand\Repositories\Brand\BrandRepositoryInterface;

class BrandsController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Brand repository.
	 *
	 * @var \Sanatorium\Brand\Repositories\Brand\BrandRepositoryInterface
	 */
	protected $brands;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Sanatorium\Brand\Repositories\Brand\BrandRepositoryInterface  $brands
	 * @return void
	 */
	public function __construct(BrandRepositoryInterface $brands)
	{
		parent::__construct();

		$this->brands = $brands;
	}

	/**
	 * Display a listing of brand.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('sanatorium/brand::brands.index');
	}

	/**
	 * Datasource for the brand Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->brands->grid();

		$columns = [
			'id',
			'active',
			'created_at',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.sanatorium.brand.brands.edit', $element->id);

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new brand.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new brand.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating brand.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating brand.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified brand.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->brands->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("sanatorium/brand::brands/message.{$type}.delete")
		);

		return redirect()->route('admin.sanatorium.brand.brands.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			foreach (request()->input('rows', []) as $row)
			{
				$this->brands->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a brand identifier?
		if (isset($id))
		{
			if ( ! $brand = $this->brands->find($id))
			{
				$this->alerts->error(trans('sanatorium/brand::brands/message.not_found', compact('id')));

				return redirect()->route('admin.sanatorium.brand.brands.all');
			}
		}
		else
		{
			$brand = $this->brands->createModel();
		}

		// Show the page
		return view('sanatorium/brand::brands.form', compact('mode', 'brand'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the brand
		list($messages) = $this->brands->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			$this->alerts->success(trans("sanatorium/brand::brands/message.success.{$mode}"));

			return redirect()->route('admin.sanatorium.brand.brands.all');
		}

		$this->alerts->error($messages, 'form');

		return redirect()->back()->withInput();
	}

}
