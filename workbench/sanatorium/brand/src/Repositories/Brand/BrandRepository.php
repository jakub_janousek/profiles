<?php namespace Sanatorium\Brand\Repositories\Brand;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;

class BrandRepository implements BrandRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Sanatorium\Brand\Handlers\Brand\BrandDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent brand model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['sanatorium.brand.brand.handler.data'];

		$this->setValidator($app['sanatorium.brand.brand.validator']);

		$this->setModel(get_class($app['Sanatorium\Brand\Models\Brand']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('sanatorium.brand.brand.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('sanatorium.brand.brand.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new brand
		$brand = $this->createModel();

		// Fire the 'sanatorium.brand.brand.creating' event
		if ($this->fireEvent('sanatorium.brand.brand.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the brand
			$brand->fill($data)->save();

			// Fire the 'sanatorium.brand.brand.created' event
			$this->fireEvent('sanatorium.brand.brand.created', [ $brand ]);
		}

		return [ $messages, $brand ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the brand object
		$brand = $this->find($id);

		// Fire the 'sanatorium.brand.brand.updating' event
		if ($this->fireEvent('sanatorium.brand.brand.updating', [ $brand, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($brand, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the brand
			$brand->fill($data)->save();

			// Fire the 'sanatorium.brand.brand.updated' event
			$this->fireEvent('sanatorium.brand.brand.updated', [ $brand ]);
		}

		return [ $messages, $brand ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the brand exists
		if ($brand = $this->find($id))
		{
			// Fire the 'sanatorium.brand.brand.deleting' event
			$this->fireEvent('sanatorium.brand.brand.deleting', [ $brand ]);

			// Delete the brand entry
			$brand->delete();

			// Fire the 'sanatorium.brand.brand.deleted' event
			$this->fireEvent('sanatorium.brand.brand.deleted', [ $brand ]);

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
