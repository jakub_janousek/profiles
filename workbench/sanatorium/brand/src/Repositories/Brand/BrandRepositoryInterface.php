<?php namespace Sanatorium\Brand\Repositories\Brand;

interface BrandRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Sanatorium\Brand\Models\Brand
	 */
	public function grid();

	/**
	 * Returns all the brand entries.
	 *
	 * @return \Sanatorium\Brand\Models\Brand
	 */
	public function findAll();

	/**
	 * Returns a brand entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Sanatorium\Brand\Models\Brand
	 */
	public function find($id);

	/**
	 * Determines if the given brand is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given brand is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given brand.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a brand entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Sanatorium\Brand\Models\Brand
	 */
	public function create(array $data);

	/**
	 * Updates the brand entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Sanatorium\Brand\Models\Brand
	 */
	public function update($id, array $data);

	/**
	 * Deletes the brand entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
