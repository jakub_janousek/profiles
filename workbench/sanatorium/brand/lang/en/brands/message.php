<?php

return [

	// General messages
	'not_found' => 'Brand [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Brand was successfully created.',
		'update' => 'Brand was successfully updated.',
		'delete' => 'Brand was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the brand. Please try again.',
		'update' => 'There was an issue updating the brand. Please try again.',
		'delete' => 'There was an issue deleting the brand. Please try again.',
	],

];
