<?php

return [

	'index'  => 'List Brands',
	'create' => 'Create new Brand',
	'edit'   => 'Edit Brand',
	'delete' => 'Delete Brand',

];
